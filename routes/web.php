<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index');

Auth::routes();


Route::get('/test/{catId}', 'SearchController@getTotalAdByCity')->name('test');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ads/{locarion?}', 'AdsController@index')->name('ads');

Route::get('/all-ads/{areaId}/{area?}', 'AdsController@adsByArea')->name('ads by area');




Route::get('/ads-detail/{ad_id}', 'AdsController@adsDetail')->name('ads detail');
Route::get('/post-ad', 'AdsController@postAd')->name('post-ad');
Route::get('/ajax/location-area/{cityId}', 'SearchController@locationArea')->name('city area');





//route for item or service
Route::get('/post-ad/item', 'AdsController@categoryItem')->name('category item');
Route::get('/post-ad/item/{categoryTile}', 'AdsController@subCategoryByTitle');
Route::get('/post-ad/item/category/{categoryId}', 'AdsController@uploadAd');
Route::get('ajax/area/{id}', 'AdsController@ajaxLocationArea');
Route::post('store-ads/item/{categoryId}', 'AdsController@storeAd');
Route::get('/ad-edit/{adId}', 'AdsController@adEdit')->name('ad edit');
Route::post('/ad-edit-location/{adId}', 'AdsController@updateLocation')->name('update location');


// Route::get('/post-ad/location/category/{categoryId}', 'AdsController@location');
// Route::get('/post-ad/city/{cityId}/category/{categoryId}', 'AdsController@locationCity');

// Route::get('/post-ad/city-area/{cityAreaId}/category/{categoryId}', 'AdsController@uploadAd');




// Route::get('/post-ad/property', 'AdsController@categoryItem')->name('category item');
// Route::get('/post-ad/job', 'AdsController@categoryItem')->name('category item');




// Route::get('ajax/post-ad/location/division/{id}', 'AdsController@ajaxDivisionArea');

// Route::get('post-ad/category/item/{id}','AdsController@subCategory');

// Route::get('post-ad/location/{categoryId}','AdsController@selectLocation');



Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/membership', 'DashboardController@membership')->name('membership');
Route::get('/resume', 'DashboardController@resume')->name('resume');

Route::get('/cerate-resume', 'ResumeController@cerateResume')->name('add personal');

Route::post('/add-resume', 'ResumeController@storeResume');

Route::get('/create-resume-step-2', 'ResumeController@addPersonalStepTwo')->name('add personal step two');


Route::post('/add-resume-step-two', 'ResumeController@storeResumeStepTwo');


Route::get('/create-resume-step-3', 'ResumeController@addPersonalStepThree')->name('add personal step two');

Route::post('/add-resume-step-three', 'ResumeController@storeResumeStepThrees');

Route::get('/create-resume-step-4', 'ResumeController@addPersonalStepFour')->name('add personal step four');
Route::post('/add-resume-step-four', 'ResumeController@storeResumeStepFour');


Route::get('/resume-complete', 'ResumeController@myResume')->name('add personal step complete');
Route::get('/my-resume', 'ResumeController@myResume');

Route::post('/update-profile-photo', 'ResumeController@updateProfilePhoto')->name('update profile photo');
// Route::get('/edit-resume-basic', 'ResumeController@editResumeBasic');
Route::get('/membership-detail', 'MembershipController@detail');
Route::post('/membership-package', 'MembershipController@byePackage');
Route::get('/membership-package', 'MembershipController@package');

Route::post('/update-resume-basic', 'ResumeController@updateResumeBasic');




Route::get('/favorite-ads', 'DashboardController@favoriteAds')->name('favorite-ads');
Route::get('/settings', 'DashboardController@settings')->name('settings');
Route::post('/change-password', 'DashboardController@changePassword')->name('change password');
Route::get('/ad-delete/{adId}', 'DashboardController@adDelete')->name('ad delete');
Route::get('/ad-view/{adId}', 'DashboardController@adView')->name('ad view');

Route::post('/ad-edit-category/{adId}', 'DashboardController@updateCategory')->name('update category');

Route::get('ajax/category/{id}', 'DashboardController@ajaxSubcatByCat');
Route::get('remove-photo/{photoId}', 'DashboardController@removeAdPhoto');
Route::post('update-photo/{adId}', 'DashboardController@updateAdPhoto');
Route::post('ad-update/{adId}', 'DashboardController@updateAd');

