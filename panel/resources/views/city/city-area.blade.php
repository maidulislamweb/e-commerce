@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>{{$cityInfo->name}} City Area</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">{{$cityInfo->name}} City area</li>
    </ol>
  </section>

  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>

<section class="content">
  <div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><span>{{$cityInfo->name}}</span> City Area List</h3>

          <hr>
        </div>

        <div class="box-body table-responsive padding">
          <table id="divisionAreaTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>SL</th>
                <th>Name</th>

                <th>Status</th>
            
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($areas) && (count($areas)>0))
              @foreach($areas as $key => $area)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$area->name}}</td>

                <td>
                  @if($area->status == 1)
                  <span class="label label-success">Approved</span>
                  @else
                  <span class="label label-warning">Pending</span>
                  @endif
                </td>
             
                <td> <a href="#" class="btn btn-xs btn-info">edit</a> 
                  <a href="#" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">delete</a>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-xs-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Add {{$cityInfo->name}} City Area </h3>

          <hr>
        </div>

        <div class="box-body table-responsive padding">
          <form action="{{route('city-areas.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="area">Area address:</label>
              <input type="text" class="form-control" id="area" name="area" value="{{old('area')}}">
            </div>
            <div class="form-group">
           <input type="hidden" name="city" value="{{$cityInfo->id}}" />
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </div>


  </div>
</section>
</div>

@push('bottom-script')
<script>
  $(function () {
    $('#divisionAreaTable').DataTable()

    // $('#divisionAreaTable').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>
@endpush
@endsection
