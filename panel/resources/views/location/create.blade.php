@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Location</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Location</li>
    </ol>
  </section>

  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>

<section class="content">
  <div class="row">


    <div class="col-xs-12">
      <div class="box">
        <div class="box-body table-responsive padding">
          @if(!isset($editAreaInfo))
          <h3 class="box-title">Add Location </h3>
          <hr>
          <form action="{{route('location.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
              <label for="area">Area address:</label>
              <input type="text" class="form-control" id="location" name="location" value="{{old('location')}}">
            </div>
            <div class="form-group">
              <div class="radio">
                <label><input type="radio" name="type" value="division" class="locationParent">Division</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="type" value="city" class="locationParent">City</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="type" checked value="area" class="locationAreaTriger">Local Area</label>
              </div>
            </div>
            <div class="form-group locationArea">
              <label>Parent: </label>
              <select class="form-control" name="parent">

                <option value="">--select one--</option>
                @if(!empty($locations))
                @foreach($locations as $location)
                <option value="{{$location->id}}">{{$location->name }}- {{$location->type }}</option>
                @endforeach
                @endif
              </select>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
          </form>
          @endif


          @if(isset($editAreaInfo))
          <h3 class="box-title">Update City Area </h3>
          <hr>
          <form action="{{route('city-areas.update', $editAreaInfo->id)}}" method="post">
            <input type="hidden" name="_method" value="PATCH">
            {{csrf_field()}}
            <div class="form-group">
              <label for="area">Area address:</label>
              <input type="text" class="form-control" id="area" name="area" value="{{$editAreaInfo->name}}">
            </div>
            <div class="form-group">
              <label>City: </label>
              <select class="form-control" name="city">

                <option value="">--select one--</option>
                @if(!empty($cities))
                @foreach($cities as $city)
                <option value="{{$city->id}}"  {{ $editAreaInfo->city_id == $city->id ? 'selected' : '' }}>{{$city->name}}</option>
                @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <label>Status: </label>
              <select class="form-control" name="status">
                <option value="1" {{ $editAreaInfo->status == 1 ? 'selected' : '' }}>Active</option>
                <option value="0" {{ $editAreaInfo->status == 0 ? 'selected' : '' }}>Inactive</option>
              </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
          @endif
        </div>
      </div>
    </div>


  </div>
</section>
</div>

@push('bottom-script')
<script>
  $(function () {
    $('#divisionAreaTable').DataTable()

    // $('#divisionAreaTable').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>


<script>
 $('.locationParent').on('click', function () {
  var type = $(this).val();

  $('.locationArea').hide();
});

 $('.locationAreaTriger').on('click', function () {
   $('.locationArea').show();
 });

</script>
@endpush
@endsection
