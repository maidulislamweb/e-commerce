@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     All Ads
     <small>Version 2.0</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">ads</li>
  </ol>
</section>
<section class="content-header" >
 @if(Session::has('info_message'))
 <div class="alert alert-warning">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Warning! </strong> {{ Session::get('info_message') }}.
</div>
@endif

@if(Session::has('success_message'))

<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success! </strong> {{ Session::get('success_message') }}.
</div>
@endif
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <div class="row">
            <div class="col-md-4">
             <form>
               <div class="form-group">
                 <select class="form-control" name="status" id="productCategory">
                   <option value="all">All</option>
                   
                   @if(!empty($categories))
                   @foreach($categories as $key => $category)
                   <option value="{{ $category->id }}" @if(isset($categoryId)) {{$categoryId == $category->id ? 'selected' : ''}} @endif >{{ $category->name }}</option>
                   @endforeach
                   @endif
                 </select>
               </div>
             </form>
           </div>
           <div class="col-md-8">
            <div class="pull-right">
             <a href="javascript:void(0)" class="btn btn-info">Add new</a>
           </div>

         </div>
       </div>


     </div>

     <div class="box-body table-responsive">
      <table class="table table-hover" id="brandTable">
        <thead>
          <tr>
            <th>SL</th>
            <th>category</th>
            <th>Area</th>
            <th>Condition</th>
            <th>Title</th>
            <th>Price(tk)</th>

            <th>Status</th>
            
            <th><span  class="pull-right"> Action</span>

            </th>
          </tr>
        </thead>
        <tbody>
          @if(!empty($ads) && (count($ads)>0))
          @foreach($ads as $key => $ad)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$ad->categoryName}}</td>
            <td>
              @if(!empty($ad->city_area))
                @php $area = \App\Http\Controllers\CommonController::getCityArea($ad->city_area); 
                @endphp
                {{$area->name}}-city
              @endif

              @if(!empty($ad->division_area))
              @php $area = \App\Http\Controllers\CommonController::getDivisionArea($ad->division_area); 
                @endphp
                {{$area->name}}-Divi
              @endif
              
            </td>
            <td>{{$ad->condition}}</td>
            <td>{{$ad->title}}</td>
            <td>{{$ad->price}}</td>

            <td>
              @if($ad->status == 1)
              <span class="label label-success">Approved</span>
              @else
              <span class="label label-warning">Pending</span>
              @endif
            </td>
            
            <td><span  class="pull-right">

             <a href="{{route('ads.show', $ad->id)}}" class="btn btn-xs btn-info">view</a> 
             <a href="{{url('ads/delete/'.$ad->id)}}" onclick="return confirm('Are you sure you want to remove this item?');" class="btn btn-xs btn-danger">remove</a>
           </span>
         </td>
       </tr>
       @endforeach
       @endif
     </tbody>
   </table>
 </div>

</div>

</div>

</div>
</section>
</div>
@push('bottom-script')
<script>
  $(function () {
    $('#brandTable').DataTable()

  })
</script>
<script type="text/javascript">
  $('body').on('change', '#productCategory', function() {
    var catId = $(this).val();
    var siteUrl = $('#siteUrl').val();
    if(catId == 'all'){
      window.location.href = '{{url("/ads")}}';
    }
    else{
      window.location.href = '{{url("/ads/category")}}/'+catId;
    }


  });
</script>
@endpush
@endsection
