@extends('layouts.app')

@section('content')
<style type="text/css">
ul#image-gallery>li>img{
  width: 100%;
}
</style>
<div style="background-color: #E7EDEE;">

  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">

        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">

           @if(Session::has('info_message'))
           <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning! </strong> {{ Session::get('info_message') }}.
          </div>
          @endif

          @if(Session::has('success_message'))

          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success! </strong> {{ Session::get('success_message') }}.
          </div>
          @endif

        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
          <div>
            <div style="font-size: 12px;">
              <ul class="breadcrumb">
                <li><a href="javascript:void(0)">Home</a></li>
                <li><a href="{{route('ads.index')}}">All ads</a></li>
                @if(!empty($location))
                <li><a href="javascript:void(0)">{{$location->name}}</a></li>
                @endif
                @if(!empty($area))
                <li><a href="javascript:void(0)">{{$area->name}}</a></li>
                @endif
                @if(!empty($parentCategory))
                <li><a href="javascript:void(0)">{{$parentCategory->name}}</a></li>
                @endif
                @if(!empty($category))
                <li><a href="javascript:void(0)">{{$category->name}}</a></li>
                @endif
                @if(!empty($ad->title))
                <li>{{$ad->title}}</li>
                @endif
              </ul>
            </div>
            <h2>
              @if(!empty($ad->title))
              <span style="vertical-align: -webkit-baseline-middle;">{{$ad->title}}</span>
              @endif
              @if($ad->status == 5) <span class="label label-danger" style="font-size: 10px;">pending</span> @endif @if($ad->status == 1) <span class="label label-success" style="font-size: 10px;">published</span> @endif </h2>
              <p>For sale by <span style="text-transform: uppercase; color: #008973;">{{Auth::user()->name}}</span> &nbsp; @if(!empty($ad->created_at))<span>{{date("d M H:i a",strtotime($ad->created_at))}}</span>, @endif @if(!empty($ad->address))<span>{{$ad->address}}</span> @endif</p>
              <hr>
            </div>


            <div class="row">
              @if(!empty($photos) && (count($photos)))
              @foreach($photos as $key => $photo)
              @php 
              $year = date('Y', strtotime($photo->created_at));
              $month = date('m', strtotime($photo->created_at));
              @endphp
              <div class="col-xs-12">
                <img src="{{asset('/../../uploads/'.$year.'/'.$month.'/'.$photo->photo)}}"  class="img-responsive thumbnail" />
             </div>

             @endforeach
             @endif

           </div>
           <hr>




           <div class="row">
             <div class="col-md-6 col-sm-12">
               <div style="">
                <div>
                  @if(!empty($ad->price))
                  <p>TK {{$ad->price}}</p>
                  @endif

                  @if(!empty($ad->price_type))
                  <p>{{$ad->price_type}}</p>
                  @endif
                </div>
                @if(!empty($ad->description))
                <div>
                  <p>{{$ad->description}}</p>
                </div>
                @endif

                <div>
                  <h4>Contact</h4>
                  <hr>
                  @if(!empty($ad->phone_no))
                  <p><i class="fa fa-phone"></i> {{$ad->phone_no}}</p>
                  @if(!empty($ad->phone_no_type))
                  <p>{{$ad->phone_no_type}}</p>
                  @endif
                  <hr>
                  @endif

                  
                </div>


              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="ads-nfo">
                @if(!empty($ad->address))
                <p><strong>Address:</strong> {{$ad->address}}</p>
                @endif
                @if(!empty($ad->condition))
                <p><strong>Condition:</strong> {{$ad->condition}}</p>
                @endif
                @if(!empty($ad->type_id))
                <p><strong>Item type:</strong> {{$ad->type_id}}</p>
                @endif
                @if(!empty($ad->type))
                <p><strong>Type:</strong> {{$ad->type}}</p>
                @endif

                @if(!empty($ad->land_size))
                <p><strong>Land size:</strong> {{$ad->land_size}}</p>
                @endif

                @if(!empty($ad->size))
                <p><strong>Size:</strong> {{$ad->size}}</p>
                @endif

                @if(!empty($ad->bed))
                <p><strong>Beds:</strong>{{$ad->bed}}</p>
                @endif

                @if(!empty($ad->bath))
                <p><strong>Baths:</strong> {{$ad->bath}}</p>
                @endif

                @if(!empty($ad->brand))
                <p><strong>Brand:</strong> {{$ad->brand}}</p>
                @endif

                @if(!empty($features))

                <p><strong>Features:</strong> 
                  @foreach($features as $key => $feature)
                  <span>{{ $feature->title }}</span>
                  @if (!$loop->last) , @endif
                  @endforeach
                </p>
                @endif

                @if(!empty($ad->model))
                <p><strong>Model:</strong> {{$ad->model}}</p>
                @endif
                @if(!empty($ad->model_year))
                <p><strong>Model year:</strong> {{$ad->model_year}}</p>
                @endif
                @if(!empty($ad->regi_year))
                <p><strong>Registration year:</strong> {{$ad->regi_year}}</p>
                @endif
                @if(!empty($ad->capacity))
                <p><strong>Engine capacity:</strong> {{$ad->capacity}}</p>
                @endif

                @if(!empty($ad->fuel_type))
                <p><strong>Fuel type:</strong> {{$ad->fuel_type}}</p>
                @endif
                @if(!empty($ad->km))
                <p><strong>Kilometers run:</strong> {{$ad->km}} km</p>
                @endif
                @if(!empty($ad->authenticity))
                <p><strong>Authenticity:</strong> {{$ad->authenticity}}</p>
                @endif




              </div>
            </div>
          </div> 
          <div class="row">
            <div class="col-md-6">
              @if($ad->status == 5)
              <a href="{{url('ads/published/'.$ad->id)}}" class="btn btn-sm btn-success btn-block">click for published</a>
              @endif
              @if($ad->status == 1)
              <a href="{{url('ads/unpublished/'.$ad->id)}}" class="btn btn-sm btn-info btn-block">click for unpublished</a>
              @endif
            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
</section>

</div>
@push('footer-asset')

<script>

</script>
@endpush
@endsection

