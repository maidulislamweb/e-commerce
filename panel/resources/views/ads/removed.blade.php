@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Removed Ads
     <small>Version 2.0</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">ads</li>
  </ol>
</section>
<section class="content-header" >
 @if(Session::has('info_message'))
 <div class="alert alert-warning">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Warning! </strong> {{ Session::get('info_message') }}.
</div>
@endif

@if(Session::has('success_message'))

<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success! </strong> {{ Session::get('success_message') }}.
</div>
@endif
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <div class="row">
            <div class="col-md-4">
             <form>
               <div class="form-group">
                 <select class="form-control" name="status" id="productCategory">
                   <option value="all">All</option>
                   
                   @if(!empty($categories))
                   @foreach($categories as $key => $category)
                   <option value="{{ $category->id }}" @if(isset($categoryInfo)) {{$categoryInfo->id == $category->id ? 'selected' : ''}} @endif >{{ $category->name }}</option>
                   @endforeach
                   @endif
                 </select>
               </div>
             </form>
           </div>
           <div class="col-md-8"></div>
       </div>


     </div>

     <div class="box-body table-responsive">
      <table class="table table-hover" id="brandTable">
        <thead>
          <tr>
            <th>SL</th>
            <th>category</th>
            <th>Area</th>
            <th>Condition</th>
            <th>Title</th>
            <th>Price(tk)</th>

            <th>Status</th>
            
            <th><span  class="pull-right"> Action</span>

            </th>
          </tr>
        </thead>
        <tbody>
          @if(!empty($ads) && (count($ads)>0))
          @foreach($ads as $key => $ad)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$ad->category_id}}</td>
            <td>
              @if(!empty($ad->city_area))
                @php $area = \App\Http\Controllers\CommonController::getCityArea($ad->city_area); 
                @endphp
                {{$area->name}}-city
              @endif

              @if(!empty($ad->division_area))
              @php $area = \App\Http\Controllers\CommonController::getDivisionArea($ad->division_area); 
                @endphp
                {{$area->name}}-Divi
              @endif
              
            </td>
            <td>{{$ad->condition}}</td>
            <td>{{$ad->title}}</td>
            <td>{{$ad->price}}</td>

            <td>
              @if($ad->status == 1)
              <span class="label label-success">Approved</span>
              @else
              <span class="label label-warning">Pending</span>
              @endif
            </td>
            
            <td><span  class="pull-right">

             <a href="javascript:void(0)" class="btn btn-xs btn-info">restore</a> 
             <a href="javascript:void(0)" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">delete</a>
           </span>
         </td>
       </tr>
       @endforeach
       @endif
     </tbody>
   </table>
 </div>

</div>

</div>

</div>
</section>
</div>
@push('bottom-script')
<script>
  $(function () {
    $('#brandTable').DataTable()

  })
</script>
<script type="text/javascript">
  $('body').on('change', '#productCategory', function() {
    var catId = $(this).val();
    var siteUrl = $('#siteUrl').val();
    if(catId == 'all'){
      window.location.href = '{{url("/brand")}}';
    }
    else{
      window.location.href = '{{url("/brand")}}/'+catId;
    }


  });
</script>
@endpush
@endsection
