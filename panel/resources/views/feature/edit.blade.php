@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product feature
      <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">feature</li>
    </ol>
  </section>
  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>
<section class="content">
  <div class="row">
    
<div class="col-md-6">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Update feature</h3>

      <hr>
    </div>

    <div class="box-body table-responsive padding">
      <form action="{{route('feature.update', $feature->id)}}" method="post">
        <input type="hidden" name="_method" value="PATCH">
        {{csrf_field()}}
        <div class="form-group">
          <label for="area">Title :</label>
          <input type="text" class="form-control" name="title" value="{{ $feature->title }}">
          @if($errors->has('title'))
          <span class="">required field</span>
          @endif
        </div>
        <div class="form-group">
         <label for="category">Category :</label>
         <select class="form-control" name="category">
          <option>--select one--</option>
          @if(!empty($categories))
          @foreach($categories as $key => $category)
          <option value="{{ $category->id }}" {{ $category->id == $feature->category_id ? 'selected' : ''  }}>{{ $category->name }}</option>
          @endforeach
          @endif
        </select>
        @if($errors->has('category'))
        <span class="">required field</span>
        @endif
      </div>
      <div class="form-group">
       <label for="status">Status :</label>
       <select class="form-control" name="status">
        <option value="1" {{ $feature->status == 1 ? 'selected' : '' }}>Active</option>
        <option value="0" {{ $feature->status == 0 ? 'selected' : '' }}>Inactive</option>
       </select>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>
</div>
</div>
</section>
</div>
@push('bottom-script')


@endpush
@endsection
