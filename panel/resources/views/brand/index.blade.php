@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product Brand
      <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">brand</li>
    </ol>
  </section>
  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <div class="row">
            <div class="col-md-4">
             <form>
               <div class="form-group">
                 <select class="form-control" name="status" id="productCategory">
                   <option value="all">All</option>
                   
                   @if(!empty($categories))
                   @foreach($categories as $key => $category)
                   <option value="{{ $category->id }}" @if(isset($categoryInfo)) {{$categoryInfo->id == $category->id ? 'selected' : ''}} @endif >{{ $category->name }}</option>
                   @endforeach
                   @endif
                 </select>
               </div>
             </form>
           </div>
           <div class="col-md-8">
            <div class="pull-right">
             <a href="{{route('category.create')}}" class="btn btn-info">Add new</a>
           </div>

         </div>
       </div>


     </div>

     <div class="box-body table-responsive">
      <table class="table table-hover" id="brandTable">
        <thead>
          <tr>
            <th>SL</th>
            <th>category</th>
            <th>Title</th>

            <th>Status</th>
            
            <th><span  class="pull-right"> Action</span>

            </th>
          </tr>
        </thead>
        <tbody>
          @if(!empty($brands) && (count($brands)>0))
          @foreach($brands as $key => $brand)
          <tr>
            <td>{{$key+1}}</td>
            <td>
             <a href="{{url('/brand/'.$brand->category_id)}}">{{$brand->category_id}}</a> 
            </td>
            <td>
              <a href="javascript:void(0)">{{$brand->title}}</a> 
            </td>

            <td>
              @if($brand->status == 1)
              <span class="label label-success">Approved</span>
              @else
              <span class="label label-warning">Pending</span>
              @endif
            </td>
            
            <td><span  class="pull-right">
            
           <a href="{{route('brand.edit', $brand->id)}}" class="btn btn-xs btn-info">edit</a> 
           <a href="{{url('brand/delete/'.$brand->id)}}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">delete</a>
         </span>
       </td>
     </tr>
     @endforeach
     @endif
   </tbody>
 </table>
</div>

</div>

</div>
<div class="col-md-6">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Add new brand</h3>

      <hr>
    </div>

    <div class="box-body table-responsive padding">
      <form action="{{route('brand.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
          <label for="area">Title :</label>
          <input type="text" class="form-control" name="title" value="{{old('title')}}">
          @if($errors->has('title'))
          <span class="">required field</span>
          @endif
        </div>
        <div class="form-group">
         <label for="category">Category :</label>
         <select class="form-control" name="category">
          <option>--select one--</option>
          @if(!empty($categories))
          @foreach($categories as $key => $category)
          <option value="{{ $category->id }}">{{ $category->name }}</option>
          @endforeach
          @endif
        </select>
        @if($errors->has('category'))
        <span class="">required field</span>
        @endif
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>
</div>
</div>
</section>
</div>
@push('bottom-script')
<script>
  $(function () {
    $('#brandTable').DataTable()

})
</script>
<script type="text/javascript">
  $('body').on('change', '#productCategory', function() {
    var catId = $(this).val();
   var siteUrl = $('#siteUrl').val();
    if(catId == 'all'){
      window.location.href = '{{url("/brand")}}';
    }
    else{
      window.location.href = '{{url("/brand")}}/'+catId;
    }


  });
</script>
@endpush
@endsection
