@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product Category
      <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">create category</li>
    </ol>
  </section>
  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Category Create</h3>
        </div>

        <div class="box-body table-responsive">
         <form action="{{route('category.store')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="category name">
            @if($errors->has('name')) 
            <span class="error-re">required field</span>
            @endif
          </div>
          <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="category title">
            @if($errors->has('title'))
            <span class="error-re">required field</span>
            @endif
          </div>
          <div class="form-group">
            <label for="Parent">Parent:</label>
            <select class="form-control" name="parent">
              <option value="0">No parent</option>
              @if(!empty($parentCategories))
              @foreach($parentCategories as $category)
              <option value="{{$category->id}}" {{ old('parent') == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
              @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            <label for="type">Type:</label>
            <select class="form-control" name="type">
              <option value="service" @if(old('type') == 'service')selected @endif >Service</option>
              <option value="property" @if(old('type') == 'property')selected @endif>property</option>
              <option value="job" @if(old('type') == 'job')selected @endif >job</option>
            </select>
            
          </div>

          <button type="submit" class="btn btn-success">Submit</button>&nbsp;&nbsp;
          <a href="{{Route('category.index')}}" class="btn btn-default">Cancel</a>
        </form>
      </div>

    </div>

  </div>
</div>
</section>
</div>
@endsection
