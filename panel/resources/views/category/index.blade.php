@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
    Product Category
        <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">category</li>
    </ol>
</section>
  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="row">
                <div class="col-md-4">
                       <form>
             <div class="form-group">
               <select class="form-control" name="status" id="categoryType">
                 <option value="all">All</option>
                 <option value="service" @if(isset($type)) {{$type == 'service' ? 'selected' : ''}} @endif>Service</option>
                 <option value="property" @if(isset($type)) {{$type == 'property' ? 'selected' : ''}} @endif>Property</option>
                 <option value="job" @if(isset($type)) {{$type == 'job' ? 'selected' : ''}} @endif>Job</option>
               </select>
             </div>
           </form>
                </div>
                <div class="col-md-8">
                  <div class="pull-right">
                       <a href="{{route('category.create')}}" class="btn btn-info">Add new</a>
                  </div>
               
                </div>
              </div>
      

        </div>

    <div class="box-body table-responsive">
      <table class="table table-hover" id="CategoryTable">
        <thead>
            <tr>
              <th>SL</th>
              <th>Name</th>
              <th>Subcategory Name</th>
              <th>Status</th>
            
              <th><span  class="pull-right"> Action</span>
            
            </th>
          </tr>
      </thead>
      <tbody>
        @if(!empty($categories) && (count($categories)>0))
        @foreach($categories as $key => $category)
        <tr>
          <td>{{$key+1}}</td>
          <td><a href="{{url('/subcategory/'.$category->id)}}">{{$category->category_name}}</a> </td>
          <td><a href="javascript:void(0)">{{$category->subCategory_name}}</a> </td>

          <td>
            @if($category->status == 1)
            <span class="label label-success">Approved</span>
            @else
            <span class="label label-warning">Pending</span>
            @endif
        </td>
          
          <td><span  class="pull-right">
           <a href="{{url('/category/up/'.$category->id)}}" class="btn btn-xs btn-info">Up</a> 
           <a href="{{url('/category/down/'.$category->id)}}" class="btn btn-xs btn-info">Down</a> 
           <a href="#" class="btn btn-xs btn-info">edit</a> 
           <a href="{{url('category/delete/'.$category->id)}}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">delete</a>
</span>
         </td>
      </tr>
      @endforeach
      @endif
  </tbody>
</table>
</div>

</div>

</div>
</div>
</section>
</div>
@push('bottom-script')
<script>
  $(function () {
    $('#CategoryTable').DataTable()

    // $('#divisionAreaTable').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>
<script type="text/javascript">
  $('body').on('change', '#categoryType', function() {
var type = $(this).val();
var siteUrl = $('#siteUrl').val();
if(type == 'all'){
  window.location.href = '{{url("/category")}}';
}
else{
window.location.href = '{{url("/category-type/")}}/'+type;
}
// alert($('#siteUrl').val());



});
</script>
@endpush
@endsection
