@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Item Type
      <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">type</li>
    </ol>
  </section>
  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>
<section class="content">
  <div class="row">
 
<div class="col-md-6">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Add new item</h3>

      <hr>
    </div>

    <div class="box-body table-responsive padding">
      <form action="{{route('item-type.update', $itemInfo->id)}}" method="post">
        <input type="hidden" name="_method" value="PATCH" />
        {{csrf_field()}}
        <div class="form-group">
          <label for="area">Title :</label>
          <input type="text" class="form-control" name="title" value="{{$itemInfo->title}}">
          @if($errors->has('title'))
          <span class="text-danger">required field</span>
          @endif
        </div>
        <div class="form-group">
         <label for="category">Category :</label>
         <select class="form-control" name="category">
          <option value="">--select one--</option>
          @if(!empty($categories))
          @foreach($categories as $key => $category)
          <option value="{{ $category->id }}" {{ $category->id == $itemInfo->category_id ? 'selected' : ''  }}>{{ $category->name }}</option>
          @endforeach
          @endif
        </select>
        @if($errors->has('category'))
        <span class="text-danger">required field</span>
        @endif
      </div>
         <div class="form-group">
       <label for="status">Status :</label>
       <select class="form-control" name="status">
        <option value="1" {{ $itemInfo->status == 1 ? 'selected' : '' }}>Active</option>
        <option value="0" {{ $itemInfo->status == 0 ? 'selected' : '' }}>Inactive</option>
       </select>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>
</div>
</div>
</section>
</div>
@push('bottom-script')
<script>
  $(function () {
    $('#itemTypeTable').DataTable()

})
</script>
<script type="text/javascript">
  $('body').on('change', '#productCategory', function() {
    var catId = $(this).val();
   var siteUrl = $('#siteUrl').val();
    if(catId == 'all'){
      window.location.href = '{{url("/brand")}}';
    }
    else{
      window.location.href = '{{url("/brand")}}/'+catId;
    }


  });
</script>
@endpush
@endsection
