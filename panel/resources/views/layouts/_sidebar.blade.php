 
<section class="sidebar">



  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="active">
      <a href="{{url('/home')}}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      
    </li>
    <li class="treeview @if(isset($activeMenu)) {{$activeMenu}} @endif">
      <a href="#">
        <i class="fa fa-fw fa-asterisk"></i> <span>Settings</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu" style="@if(isset($activeMenu)) display: block; @endif" >
        <li><a href="{{url('/divisions')}}"><i class="fa fa-circle-o"></i> Division</a></li>
        <li><a href="{{url('/division-areas')}}"><i class="fa fa-circle-o"></i> Division Area</a></li>
           <li><a href="{{route('location.index')}}"><i class="fa fa-circle-o"></i> Location</a></li>
           <li><a href="{{url('/cities')}}"><i class="fa fa-circle-o"></i> City</a></li>
        <li><a href="{{url('/city-areas')}}"><i class="fa fa-circle-o"></i> City Area</a></li>

         <li><a href="{{Route('category.index')}}"><i class="fa fa-circle-o"></i> Category</a></li>
         <li><a href="{{Route('brand.index')}}"><i class="fa fa-circle-o"></i> Brand</a></li>
         <li><a href="{{Route('item-type.index')}}"><i class="fa fa-circle-o"></i> Item Type</a></li>
          <li><a href="{{Route('feature.index')}}"><i class="fa fa-circle-o"></i> Feature </a></li>
       
        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
      </ul>
    </li>
   

  
    <li class="treeview">
      <a href="#">
        <i class="fa fa-folder"></i> <span>Examples</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
        <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
        <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
        <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
        <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
        <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
        <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
        <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
        <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
      </ul>
    </li>

    <li class=" @if(isset($active)) {{$active}} @endif"><a href="{{route('ads.index')}}"><i class="fa fa-book"></i> <span>All Ads</span></a></li>
    <li class=" @if(isset($removedAd)) {{$removedAd}} @endif"><a href="{{url('/removed-ad')}}"><i class="fa fa-book"></i> <span>Removed Ads</span></a></li>
    <li class="header">LABELS</li>
    <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
  </ul>
</section>
