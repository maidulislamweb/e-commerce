@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> My Profile</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">profile</li>
    </ol>
  </section>

  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="well well-sm">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              @if(!empty(Auth::user()->photo))

              <img src="{{asset('assets/user-photo/'.Auth::user()->photo)}}" alt="" class="img-rounded img-responsive" />
              @else
              <img src="{{asset('assets/user-photo/default.png')}}" alt="" class="img-rounded img-responsive" />
              @endif

            </div>
            <div class="col-sm-6 col-md-8">
              <h4>{{Auth::user()->name}}</h4>
              <p>{{Auth::user()->email}}</p>
              <p>{{Auth::user()->phone}}</p>
              <p>
                @if(Auth::user()->role == 1)
                <span>Supper Admin</span>
                @endif
                @if(Auth::user()->role == 2)
                <span>Admin</span>
                @endif
                @if(Auth::user()->role == 3)
                <span>User</span>
                @endif
                 @if(Auth::user()->role == 4)
               <span>Member</span>
                @endif
              </p>


              <!-- Split button -->
              <div class="btn-group">
                <button type="button" class="btn btn-primary">
                Social</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span><span class="sr-only">Social</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Twitter</a></li>
                  <li><a href="#">Google +</a></li>
                  <li><a href="#">Facebook</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Github</a></li>
                </ul>
              </div>
              <a href="{{route('profile.edit',Auth::user()->id)}}" class="pull-right">edit profile</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
</div>

@push('bottom-script')

@endpush
@endsection
