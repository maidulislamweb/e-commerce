@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Profile Update </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">profile</li>
    </ol>
  </section>

  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>

<section class="content">

  <div class="row">
    <div class="col-xs-12">
      <div class="well well-sm">
        <div class="row">
          <div class="col-sm-12">
            <form enctype="multipart/form-data" method="post" action="{{url('profile-photo-update')}}">
              {{csrf_field()}}
            <table>
              <tr>
                <td>
                  @if(!empty(Auth::user()->photo))
                  <img src="{{asset('assets/user-photo/'.Auth::user()->photo)}}" alt="" class="img-rounded img-responsive" />
                  @else
                  <img src="{{asset('assets/user-photo/default.png')}}" alt="" class="img-rounded img-responsive" />
                  @endif
                </td>
                <td>
                 <div style="padding: 10px;">
                  <p>must be jpeg, jpg, png image (300X300)</p>
                  <input type="file" name="photo" />
                </div> 
               </td>
               <td>
                <button type="submit" class="btn btn-primary">update</button>
              </td>
            </tr>
          </table>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <hr>
          <form action="{{route('profile.update', Auth::user()->id)}}" method="post">
            {{ method_field('PATCH') }}
            {{csrf_field()}}
            <div class="form-group">
              <label >Name:</label>
              <input type="text" class="form-control"  value="{{Auth::user()->name}}" name="name" />
            </div>
            <div class="form-group">
              <label >Phone:</label>
              <input type="text" class="form-control"  value="{{Auth::user()->phone}}" name="phone" />
            </div>
            <div class="form-group">
              <label >Email:</label>
              <input type="email" class="form-control" value="{{Auth::user()->email}}" name="email" readonly/>
            </div>
            <button type="submit" class="btn btn-info">Update</button>
          </form>




        </div>
      </div>

    </div>
  </div>
</div>


</section>
</div>

@push('bottom-script')

@endpush
@endsection
