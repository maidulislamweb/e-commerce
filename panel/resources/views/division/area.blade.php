@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Division Area</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">division area</li>
    </ol>
  </section>

  <section class="content-header" >
   @if(Session::has('info_message'))
   <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Warning! </strong> {{ Session::get('info_message') }}.
  </div>
  @endif

  @if(Session::has('success_message'))

  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success! </strong> {{ Session::get('success_message') }}.
  </div>
  @endif
</section>

<section class="content">
  <div class="row">
    <div class="col-md-6 col-sm-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Division Area List</h3>

          <hr>
        </div>

        <div class="box-body table-responsive padding">
          <table id="divisionAreaTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>SL</th>
                <th>Name</th>

                <th>Status</th>
                <th>Division</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($areas) && (count($areas)>0))
              @foreach($areas as $key => $area)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$area->name}}</td>

                <td>
                  @if($area->status == 1)
                  <span class="label label-success">Approved</span>
                  @else
                  <span class="label label-warning">Pending</span>
                  @endif
                </td>
                <td>
                 <a href="{{url('/division-area/'.$area->divisionId)}}" title="Show all areas">{{$area->divisionName}}</a> 
               </td>
               <td> <a href="{{route('division-areas.edit',$area->id )}}" class="btn btn-xs btn-info" title="EDIT">edit</a> 
                <a href="{{url('/division-area/delete/'.$area->id)}}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger" title="REMOVE">delete</a>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-xs-6">
    <div class="box">


      <div class="box-body table-responsive padding">
       @if(!isset($editAreaInfo))
        <h3 class="box-title">Add Division Area </h3>
        <hr>
       <form action="{{route('division-areas.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
          <label for="area">Area address:</label>
          <input type="text" class="form-control" id="area" name="area" value="{{old('area')}}">
        </div>
        <div class="form-group">
          <label>Division: </label>
          <select class="form-control" name="division">
            <option value="">--select one--</option>
            @if(!empty($divisions))
            @foreach($divisions as $division)
            <option value="{{$division->id}}"  {{ old('division') == $division->id ? 'selected' : '' }}>{{$division->name}}</option>
            @endforeach
            @endif
          </select>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      @endif

      @if(isset($editAreaInfo))
      <h3 class="box-title">Update Division Area </h3>
        <hr>

      <form action="{{route('division-areas.update', $editAreaInfo->id)}}" method="post">
        <input type="hidden" name="_method" value="PATCH">
        {{csrf_field()}}
        <div class="form-group">
          <label for="area">Area address:</label>
          <input type="text" class="form-control" id="area" name="area" value="{{$editAreaInfo->name}}">
        </div>
        <div class="form-group">
         <label>Division: </label>
         <select class="form-control" name="division">
          <option value="">--select one--</option>
          @if(!empty($divisions))
          @foreach($divisions as $division)
          <option value="{{$division->id}}"  {{ $editAreaInfo->division_id == $division->id ? 'selected' : '' }}>{{$division->name}}</option>
          @endforeach
          @endif
        </select>
      </div>
      <div class="form-group">
        <label>Status: </label>
        <select class="form-control" name="status">
          <option value="1" {{ $editAreaInfo->status == 1 ? 'selected' : '' }}>Active</option>
          <option value="0" {{ $editAreaInfo->status == 0 ? 'selected' : '' }}>Inactive</option>
        </select>
      </div>

      <button type="submit" class="btn btn-info">Update</button>
      <a href="{{route('division-areas.index')}}" class="btn btn-default">Cancel</a>
    </form>
    @endif

  </div>
</div>
</div>


</div>
</section>
</div>

@push('bottom-script')
<script>
  $(function () {
    $('#divisionAreaTable').DataTable()

    // $('#divisionAreaTable').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>
@endpush
@endsection
