@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Division
        <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">division</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Division List</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <thead>
            <tr>
              <th>SL</th>
              <th>Name</th>

              <th>Status</th>
              <th>Detail</th>
              <th>Action</th>
          </tr>
      </thead>
      <tbody>
        @if(!empty($divisions) && (count($divisions)>0))
        @foreach($divisions as $key => $division)
        <tr>
          <td>{{$key+1}}</td>
          <td>
          <a href="{{url('/division-area/'.$division->id)}}" title="show all areas">{{$division->name}}</a> 
          </td>

          <td>
            @if($division->status == 1)
            <span class="label label-success">Approved</span>
            @else
            <span class="label label-warning">Pending</span>
            @endif
        </td>
          <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
          <td> <a href="#" class="btn btn-xs btn-info">edit</a> </td>
      </tr>
      @endforeach
      @endif
  </tbody>
</table>
</div>

</div>

</div>
</div>
</section>
</div>
@endsection
