<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

##profile
Route::resource('profile', 'ProfileController');
Route::post('profile-photo-update', 'ProfileController@updatePhoto');



##ALl Ads
Route::get('/ads/published/{id}', 'AdController@published');
Route::get('/ads/unpublished/{id}', 'AdController@unpublished');
Route::get('/ads/delete/{id}', 'AdController@delete');
Route::get('/ads/category/{categoryId}', 'AdController@categorywiseAds');
Route::resource('ads', 'AdController');
Route::get('removed-ad', 'AdController@removedAd');



##location
Route::get('/divisions', 'DivisionController@index')->name('divisions');
Route::resource('division-areas', 'DivisionAreaController');
Route::get('/division-area/{divisionId}','DivisionAreaController@divisionArea' );
Route::get('/division-area/delete/{divisionId}','DivisionAreaController@divisionAreaDelete' );
Route::get('/cities', 'CityController@index')->name('city');
Route::get('city-area/{cityId}', 'CityAreaController@cityArea');
Route::resource('city-areas', 'CityAreaController');

Route::resource('location', 'LocationController');

##category
Route::get('/category-type/{type}', 'CategoryController@categoryType');
Route::resource('category', 'CategoryController');
Route::get('/category/delete/{categoryId}', 'CategoryController@destroy');
Route::get('/category/down/{categoryId}', 'CategoryController@downCategory');
Route::get('/category/up/{categoryId}', 'CategoryController@upCategory');
Route::get('/subcategory/{categoryId}', 'CategoryController@subcategory');


##Brand
Route::get('brand/delete/{brandId}', 'BrandController@destroy');
Route::get('brand/{categoryId}', 'BrandController@categoryWiseBrand');
Route::resource('brand', 'BrandController');


##ItemType
Route::get('item-type/delete/{itemId}', 'ItemTypeController@destroy');
Route::get('item-type/{categoryId}', 'ItemTypeController@categoryWiseItem');
Route::resource('item-type', 'ItemTypeController');

##Feature
Route::get('feature/delete/{featureId}', 'FeatureController@destroy');
Route::get('feature/{categoryId}', 'FeatureController@categoryWisefeature');
Route::resource('feature', 'FeatureController');


