<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Redirect;
use Auth;
use DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('profile.index');
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('profile.edit');
    }
    public function updatePhoto(Request $request){

     $validator = Validator::make($request->all(), [
        'photo' => 'required | mimes:jpeg,jpg,png | max:1000',
    ]);

     if ($validator->fails()) {
        return Redirect::back()
        ->withErrors($validator)
        ->withInput();
    } else {
        if(!empty($request->file('photo'))){
            $image = $request->file('photo');
            $base_name = preg_replace('/\..+$/', '', $image->getClientOriginalName());
            $base_name = explode(' ', $base_name);
            $base_name = implode('-', $base_name);
            $image_name = $base_name."-".uniqid().".".$image->getClientOriginalExtension();
            $file_path = 'assets/user-photo/';
            $image->move($file_path, $image_name);

            $photos = array(
                'photo' => $image_name, 
                'updated_at'=> date("Y-m-d h:i:s", time()),
            ); 

            DB::table('users')->where('id', Auth::user()->id)->update($photos);
            
        }
        Redirect::back();
    }
}


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        
        
    ]);

       if ($validator->fails()) {
        return Redirect::back()
        ->withErrors($validator)
        ->withInput();
    } else {
        $arrData = array(
            'name' => $request->name, 
            'updated_at'=> date("Y-m-d h:i:s", time()),
        ); 
        if(!empty($request->phone)){
           $arrData['phone'] = $request->phone; 
       }

       DB::table('users')->where('id', Auth::user()->id)->update($arrData);

      return Redirect::back();
   }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
