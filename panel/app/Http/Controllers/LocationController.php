<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Location;

use Session;
use Redirect;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::get();
        // dd($locations);
        $data['locations'] = $locations;
         $data['activeMenu'] = 'menu-open';
        // dd($data);
        return view('location.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::where('type', '!=','area')->get();
        // dd($locations);
        $data['locations'] = $locations;
        //  $data['activeMenu'] = 'menu-open';
        // dd($data);
        return view('location.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $location = new Location;

        if($request->type == 'division'){
            $title = $request->location.'-division';

        }else{
             $title = $request->location;
        }
        

        $title = strtolower(implode("_",(explode(" ",$title))));



        $location->name = $request->location;
        $location->title = $title;

        $location->type = $request->type;


        if($request->parent){
           $location->parent = $request->parent; 
       }else{
        $location->parent = 0;
       }
        
        $location->status = 1;
        $location->order_id = Location::max('order_id') + 1;
        $location->created_at = date('Y-m-d');
        $location->updated_at = null;
        $location->save();
        Session::flash('success_message', 'A Location has been Inserted successfully');

        return Redirect::back();




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
