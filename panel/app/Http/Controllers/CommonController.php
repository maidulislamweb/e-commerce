<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CityArea;
use App\DivisionArea;

class CommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getCityArea($areaId)
    {
       return $city = CityArea::where('id', $areaId)->first();
    }
     public static function getDivisionArea($areaId)
    {
       return $city = DivisionArea::where('id', $areaId)->first();
    }
}
