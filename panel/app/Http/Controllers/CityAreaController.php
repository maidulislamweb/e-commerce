<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\City;
use App\CityArea;
use DB;

class CityAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $data['cities'] = $this->getCity();
       $data['areas'] = $this->getArea('all');
        $data['activeMenu'] = 'menu-open';
        // dd($data);
       return view('city.area' , $data);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['parentCategories'] = $this->getParentCategories();

        return view('category.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'area' => 'required',
            'city' => 'required',

        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {

           $check_area = CityArea::where('name', $request->area)->where('city_id', $request->city)->first();

           if(!empty($check_area)){

            Session::flash('info_message', 'The city area already existed');
            return Redirect::back();
        }

        try{
            $area = new CityArea;
            $area->name = $request->area;
            $area->city_id = $request->city;
            $area->status = 1;
            $area->order_id = CityArea::max('order_id') + 1;
            $area->created_at = date('Y-m-d');
            $area->updated_at = null;
            $area->save();
            Session::flash('success_message', 'A city area has been Inserted successfully');

            return Redirect::back();
        }

        catch(\Exception $e){

            Session::flash('info_message', 'The city area has not been created. Error : '.$e->getMessage());
            return Redirect::to('city-areas');
        }

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // dd($id);
        $editAreaInfo  = CityArea::where('id', $id)->first();
        if(!empty($editAreaInfo)){
            $data['editAreaInfo'] = $editAreaInfo;
            $data['areas'] = $this->getArea('all');
            $data['cities'] = $this->getCity();

             $data['activeMenu'] = 'menu-open';
            return view('city.area', $data);
        }
        else{
            return abort('404');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           'area' => 'required',
           'city' => 'required',
           'status' => 'required',

       ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {

            $area = CityArea::find($id);
            $area->name = $request->area;
            $area->city_id = $request->city;
            $area->status = $request->status;
            $area->updated_at = date('Y-m-d');
// dd($area);
            $area->update();

            Session::flash('success_message', 'A Area has been updated successfully');

            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getParentCategories()
    {
        return Category::select('id', 'display_name', 'order_id')->where('parent', 0)->orderBy('order_id', 'DESC')->get();
    }



    public function upCategory($id)
    {
        $category = Category::find($id);

        if($category->order_id > 1)
        {
            Category::where('parent', $category->parent)->where('order_id', $category->order_id - 1)->update(['order_id' => $category->order_id]);
            Category::where('id', $id)->update(['order_id' => $category->order_id - 1]);
        }

        return response()->json(true);
    }


    /**
     * Down the order of a category
     */

    public function downCategory($id)
    {
        $category = Category::find($id);
        $nextCategory = Category::where('parent', $category->parent)->where('order_id', '>', $category->order_id)->first();

        if(!empty($nextCategory))
        {
            Category::where('parent', $category->parent)->where('order_id', $category->order_id + 1)->update(['order_id' => $category->order_id]);
            Category::where('id', $id)->update(['order_id' => $category->order_id + 1]);
        }
        
        return response()->json(true);
    }





    public function getCity()
    {
        $cities = City::where('status', 1)->orderBy('order_id', 'asc')->get();
        return $cities;

    }

    public function getArea($cityId){
        if($cityId  == 'all'){
            $areas = DB::table('city_areas')->select('city_areas'.'.*', 'cities.name as cityName', 'cities.id as cityId')->leftJoin('cities', 'city_areas.city_id', '=', 'cities.id')->where('city_areas.status', '!=', 5)->get();
            return $areas;
        }
    }

    public function cityArea($cityId){
        // dd('aa');
       $cityInfo = City::where('id', $cityId)->first();

       if(!empty($cityInfo)){
        $areas = DB::table('city_areas')->where('city_areas.city_id', $cityId)->get();

        $data['cityInfo'] = $cityInfo;
        $data['areas'] = $areas;
        // dd($data);
        return view('city.city-area', $data);
    }

}


}