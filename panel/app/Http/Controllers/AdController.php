<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use Redirect;
use DB;
use Session;

class AdController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /* status = 5 pending, status = 1 approver, status = 2 removed */

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $ads = Ad::where('status', '!=', 2)->get();

     $ads = DB::table('ads')->select('ads.*', 'categories.name as categoryName')->leftJoin('categories', 'categories.id', '=', 'ads.category_id')->where('ads.status', '!=', 2)->get();

     $data['ads'] = $ads;
     $data['active'] = 'active';
     $data['categories'] = DB::table('categories')->get();
     return view('ads.index', $data);
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($adId)
    {
     $ad = DB::table('ads')->where('id', $adId)->first();

     if(!empty($ad->city_area)){
        $area = DB::table('city_areas')->where('id', $ad->city_area)->first();
        $city = DB::table('cities')->where('id', $area->city_id)->first();

        $data['area'] = $area;
        $data['location'] = $city;
    }
    if(!empty($ad->division_area)){
        $area = DB::table('division_areas')->where('id', $ad->division_area)->first();
        $division = DB::table('divisions')->where('id', $area->division_id)->first();
        $data['area'] = $area;
        $data['location'] = $division;
    }

    $category = DB::table('categories')->where('id', $ad->category_id)->first();
    if($category->parent != 0){
        $parentCategory = DB::table('categories')->where('id', $category->parent)->first();
        $data['parentCategory'] = $parentCategory;
    }
    $data['category'] = $category;


    if(!empty($ad->features)){
       $features =  $ad->features;
       $arrFeatures = explode(",",$features);
       $features = DB::table('features')->whereIn('id', $arrFeatures)->get();
       $data['features'] = $features;
   }

     // dd($features);
   $photos = DB::table('photos')->where('ads_id', $ad->id)->get();
   $data['ad'] = $ad;
   $data['photos'] = $photos;
    // dd($photos);

   return view('ads.view', $data);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($adId)
    {
      //
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id){
        $ad = DB::table('ads')->where('id', $id)->update(['status'=>'2']);
        return Redirect::back();
        // dd($id);
    }

    public function removedAd(){

        $data['removedAd'] = 'active';
        $ads = DB::table('ads')->where('status', 2)->get();
        $data['ads'] = $ads;
        
        $data['categories'] = DB::table('categories')->get();
        // dd($data);
        return view('ads.removed', $data);
    }

    public function categorywiseAds($categoryId){

     $ads = DB::table('ads')->select('ads.*', 'categories.name as categoryName')->leftJoin('categories', 'categories.id', '=', 'ads.category_id')->where('categories.id', '=', $categoryId)->where('ads.status', '!=', 2)->get();

     $data['categoryId'] = $categoryId;
     $data['ads'] = $ads;
     $data['active'] = 'active';
     $data['categories'] = DB::table('categories')->get();
     return view('ads.index', $data);
 }

 public function published($id){
   $ad = DB::table('ads')->where('id', $id)->first();
// dd($ad);
   if(!empty($ad)){
    if($ad->status == 5){
        DB::table('ads')->where('id', $id)->update(['status'=>'1']);
        Session::flash('success_message', 'Ad published successfully');
        return Redirect::back(); 
    }
    else{
        Session::flash('info_message', 'Something wrong, ad does not published...!');
        return Redirect::back();
    }

}
else{
    return abort(404);
}

}
public function unpublished($id){
   $ad = DB::table('ads')->where('id', $id)->first();
   if(!empty($ad)){
    if($ad->status == 1){
        DB::table('ads')->where('id', $id)->update(['status'=>'5']);
        Session::flash('success_message', 'Ad unpublished successfully');
        return Redirect::back(); 
    }
    else{
        Session::flash('info_message', 'Something wrong, ad does not published...!');
        return Redirect::back();
    }

}
else{
    return abort(404);
}

}



}
