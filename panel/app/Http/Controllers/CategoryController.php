<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
     $data['categories'] = $this->getCategories();
     $data['activeMenu'] = 'menu-open';
        // dd($data);
     return view('category.index', $data);
 }
 public function subcategory($categoryId){
    $categoryInfo = Category::where('id', $categoryId)->first();
    if(!empty($categoryInfo)){
        $subcategories = $this->getSubCategories($categoryId);

        $data['categoryInfo'] = $categoryInfo;
        $data['subcategories'] = $subcategories;
        $data['activeMenu'] = 'menu-open';
        return view('category.subcategory', $data);
    }
    else{
        return abort(404);
    }

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['parentCategories'] = $this->getParentCategories();
// dd($data);
        return view('category.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'parent' => 'required',
            'type' => 'required',

        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {

            $category = new Category;
            $category->name = $request->name;
            $category->title = $request->title;
            $category->parent = $request->parent;
            $category->status = 1;
            $category->type = $request->type;
            $category->order_id = Category::where('parent', $request->parent)->max('order_id') + 1;
            $category->created_at = date('Y-m-d');
            $category->updated_at = null;
// dd($category);
            $category->save();

            Session::flash('success_message', 'A category has been Inserted successfully');

            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['parentCategories'] = $this->getParentCategories();
        $data['category'] = Category::where('id', $id)->first();
        return view('category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'display_name' => 'required',
            'title' => 'required',
            'status' => 'required',

        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {

            $category = Category::find($id);
            $category->display_name = $request->display_name;
            $category->title = $request->title;
            $category->color = $request->color;
            $category->parent = $request->parent;
            $category->header_display = $request->header_display;
            $category->footer_display = $request->footer_display;
            $category->home_page_display = $request->home_page_display;
            $category->menubar_display = $request->menubar_display;
            $category->status = $request->status;
            $category->updated_by = Auth::user()->id;
            $category->updated_at = date('Y-m-d');
// dd($category);
            $category->update();

            Session::flash('success_message', 'A category has been Inserted successfully');

            return Redirect::to('category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryId)
    {
       $categoryInfo = Category::where('id', $categoryId)->first();

       if(!empty($categoryInfo)){
        $category = Category::where('id', $categoryId)->first();
        $category->status = 5;
        $category->updated_at = date('Y-m-d');
        $category->update();
        Session::flash('success_message', 'A category has been Remove successfully');
        return Redirect::back();

    }
    else{
        return abort('404');
    }

}

public static function getParentCategories()
{
    return Category::select('id', 'name', 'order_id')->where('parent', 0)->orderBy('order_id', 'DESC')->get();
}



public function upCategory($id)
{
    $category = Category::find($id);

    if($category->order_id > 1)
    {
        Category::where('parent', $category->parent)->where('order_id', $category->order_id - 1)->update(['order_id' => $category->order_id]);
        Category::where('id', $id)->update(['order_id' => $category->order_id - 1]);
    }

    return Redirect::back();
}


    /**
     * Down the order of a category
     */

    public function downCategory($id){
        $category = Category::find($id);

        $nextCategory = Category::where('parent', $category->parent)->where('order_id', '>', $category->order_id)->first();
  // dd($nextCategory);
        if(!empty($nextCategory))
        {
            Category::where('parent', $category->parent)->where('order_id', $category->order_id + 1)->update(['order_id' => $category->order_id]);
            Category::where('id', $id)->update(['order_id' => $category->order_id + 1]);
        }
        
        
        return Redirect::back();
    }



    ##get Category
    public static function getCategories(){

     $results = array();
     $categories = Category::select('id', 'name as category_name', 'title', 'status', 'order_id')->where('parent', 0)->orderBy('order_id')->get();

     foreach ($categories as $category) {

      $category->subCategory_name = "";
      $category->parent = "";
      array_push($results, $category);

      $subCategories = Category::select('id', 'name as subCategory_name', 'title', 'status', 'order_id')->where('parent', $category->id)->where('status', '!=', 5)->orderBy('order_id')->get();

      foreach ($subCategories as $subCategory)
      {
        $subCategory->category_name = "";
        $subCategory->parent = $category->category_name;
        array_push($results, $subCategory);
    }
}

return $results;
}

##get Subcategory
public static function getSubCategories($categoryId){
    $categories = Category::where('status','!=', 5)->where('parent', $categoryId)->orderBy('order_id')->get();

    return $categories;
}

#get categoryType
public function categoryType($type  = Null){
    if($type != Null){

     $results = array();
     $categories = Category::select('id', 'name as category_name', 'title', 'status', 'order_id')->where('parent', 0)->where('type', $type)->orderBy('order_id')->get();

     foreach ($categories as $category) {

      $category->subCategory_name = "";
      $category->parent = "";
      array_push($results, $category);

      $subCategories = Category::select('id', 'name as subCategory_name', 'title', 'status', 'order_id')->where('parent', $category->id)->where('status', '!=', 5)->where('type', $type)->orderBy('order_id')->get();

      foreach ($subCategories as $subCategory)
      {
        $subCategory->category_name = "";
        $subCategory->parent = $category->category_name;
        array_push($results, $subCategory);
    }
}

$data['type'] = $type;
$data['categories'] = $results;
        // dd($data);
return view('category.index', $data);

}
else{
    $this->getCategories();
}

}


// public function getCategoryByType($type){

// return Category::where('status','!=', 5)->where('type', $type)->orderBy('order_id')->get();
// }



}
