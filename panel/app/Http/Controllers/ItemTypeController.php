<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\Category;
use App\ItemType;

class ItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
        $data['itemTypes'] = ItemType::where('status', '!=' ,5)->get();
        $data['activeMenu'] = 'menu-open';
        return view('item-type.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        // dd($request->category);
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $item = new ItemType;
            $item->title = $request->title;
            $item->category_id = $request->category;
            $item->status = 1;
            $item->order_id = ItemType::where('category_id', $request->category)->max('order_id') + 1;
            $item->created_at = date('Y-m-d');
            $item->updated_at = null;
// dd($item);
            $item->save();
            Session::flash('success_message', 'A item has been Inserted successfully');
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemInfo = ItemType::where('id', $id)->where('status', '!=', 5)->first();
        if(!empty($itemInfo)){
           $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
           $data['itemInfo'] = $itemInfo;
           $data['activeMenu'] = 'menu-open';
           return view('item-type.edit', $data);
       }
       else{
        return abort('404');
    }
    
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $validator = Validator::make($request->all(), [
        'title' => 'required',
        'category' => 'required',
    ]);

     if ($validator->fails()) {
        return Redirect::back()
        ->withErrors($validator)
        ->withInput();
    } else {
        $item = ItemType::find($id);
        $item->title = $request->title;
        $item->category_id = $request->category;
        $item->status = $request->status;
        $item->updated_at = date('Y-m-d');
// dd($item);
        $item->update();
        Session::flash('success_message', 'A item has been Updated successfully');
        return Redirect::to('item-type');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $itemInfo = ItemType::where('id', $id)->first();

     if(!empty($itemInfo)){
        
        $itemInfo->status = 5;
        $itemInfo->updated_at = date('Y-m-d');
        $itemInfo->update();
        Session::flash('success_message', 'A item has been Remove successfully');
        return Redirect::back();

    }
    else{
        return abort('404');
    }
}

public function categoryWiseBrand($categoryId){
    $categoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();

    if(!empty($categoryInfo)){

      $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
      $data['brands'] = Brand::where('status', '!=' ,5)->where('category_id', $categoryId)->get();
      $data['categoryInfo'] = $categoryInfo;

      return view('brand.index', $data);  
  }
  else{
    return $this->index();
}

}
}
