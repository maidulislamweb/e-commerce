<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\Division;
use App\DivisionArea;
use DB;

class DivisionAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
       $data['divisions'] = $this->getDivision();
       $data['areas'] = $this->getArea('all');
       $data['activeMenu'] = 'menu-open';
        // dd($data);
       return view('division.area' , $data);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'area' => 'required',
            'division' => 'required',

        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {

           $check_area = DivisionArea::where('name', $request->area)->where('division_id', $request->division)->first();

           if(!empty($check_area)){

            Session::flash('info_message', 'The division area already existed');
            return Redirect::back();
        }

        try{
            $area = new DivisionArea;
            $area->name = $request->area;
            $area->division_id = $request->division;
            $area->status = 1;
            $area->order_id = DivisionArea::max('order_id') + 1;
            $area->created_at = date('Y-m-d');
            $area->updated_at = null;
            $area->save();
            Session::flash('success_message', 'A division area has been Inserted successfully');

            return Redirect::back();
        }

        catch(\Exception $e){

            Session::flash('info_message', 'The division area has not been created. Error : '.$e->getMessage());
            return Redirect::back();
        }

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        // dd($id);
        $editAreaInfo  = DivisionArea::where('id', $id)->first();
        if(!empty($editAreaInfo)){
            $data['editAreaInfo'] = $editAreaInfo;
            $data['areas'] = $this->getArea('all');
            $data['divisions'] = $this->getDivision();

             $data['activeMenu'] = 'menu-open';
            return view('division.area', $data);
        }
        else{
            return abort('404');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
           'area' => 'required',
           'division' => 'required',
           'status' => 'required',

       ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $area = DivisionArea::find($id);
            $area->name = $request->area;
            $area->division_id = $request->division;
            $area->status = $request->status;
            $area->updated_at = date('Y-m-d');
// dd($area);
            $area->update();

            Session::flash('success_message', 'A area has been Inserted successfully');

            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        dd($id);
    }



    public function upCategory($id){
        $category = Category::find($id);

        if($category->order_id > 1)
        {
            Category::where('parent', $category->parent)->where('order_id', $category->order_id - 1)->update(['order_id' => $category->order_id]);
            Category::where('id', $id)->update(['order_id' => $category->order_id - 1]);
        }

        return response()->json(true);
    }


    /**
     * Down the order of a category
     */

    public function downCategory($id)
    {
        $category = Category::find($id);
        $nextCategory = Category::where('parent', $category->parent)->where('order_id', '>', $category->order_id)->first();

        if(!empty($nextCategory))
        {
            Category::where('parent', $category->parent)->where('order_id', $category->order_id + 1)->update(['order_id' => $category->order_id]);
            Category::where('id', $id)->update(['order_id' => $category->order_id + 1]);
        }
        
        return response()->json(true);
    }





    public function getDivision(){
        $divisions = Division::orderBy('order_id', 'asc')->get();
        return $divisions;

    }

    public function getArea($divisionId){
        if($divisionId  == 'all'){
            $areas = DB::table('division_areas')->select('division_areas'.'.*', 'divisions.name as divisionName', 'divisions.id as divisionId')->leftJoin('divisions', 'division_areas.division_id', '=', 'divisions.id')->where('division_areas.status', '!=', 5)->get();
            return $areas;
        }
    }

    public function divisionArea($divisionId){

        $divisionInfo = Division::where('id', $divisionId)->first();

        if(!empty($divisionInfo)){
            $areas = DB::table('division_areas')->where('division_areas.division_id', $divisionId)->get();

            $data['divisionInfo'] = $divisionInfo;
            $data['areas'] = $areas;
        // dd($data);
            return view('division.division-area', $data);

        }
        else{
            return abort('404');
        }
    }

    public function divisionAreaDelete($divisionId){
     $divisionInfo = DivisionArea::where('id', $divisionId)->first();

     if(!empty($divisionInfo)){
        $area = DivisionArea::where('id', $divisionId)->first();
        $area->status = 5;
        $area->updated_at = date('Y-m-d');
        $area->update();
        Session::flash('success_message', 'A area has been Remove successfully');
        return Redirect::back();

    }
    else{
        return abort('404');
    }

}

}
