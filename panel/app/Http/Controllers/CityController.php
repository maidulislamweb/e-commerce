<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::where('status', 1)->orderBy('order_id', 'ASC')->get();
        $data['cities'] = $cities;
         $data['activeMenu'] = 'menu-open';
        // dd($data);
        return view('city.city', $data);
    }
}
