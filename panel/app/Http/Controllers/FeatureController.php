<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\Category;
use App\Feature;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
        $data['features'] = Feature::where('status', '!=' ,5)->get();
        // dd($data);
 $data['activeMenu'] = 'menu-open';
        return view('feature.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $feature = new Feature;
            $feature->title = $request->title;
            $feature->category_id = $request->category;
            $feature->status = 1;
            $feature->order_id = Feature::where('category_id', $request->category)->max('order_id') + 1;
            $feature->created_at = date('Y-m-d');
            $feature->updated_at = null;
// dd($feature);
            $feature->save();
            Session::flash('success_message', 'A feature has been Inserted successfully');
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featureInfo = Feature::where('id', $id)->first();

       if(!empty($featureInfo)){

        $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
      $data['activeMenu'] = 'menu-open';
        $data['feature'] = $featureInfo;

       return view('feature.edit', $data); 
   }
   else{
    return abort('404');
   }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $feature = feature::find($id);
            $feature->title = $request->title;
            $feature->category_id = $request->category;
            $feature->status = $request->status;
            $feature->order_id = feature::where('category_id', $request->category)->max('order_id') + 1;
            
            $feature->updated_at =  date('Y-m-d');
// dd($feature);
            $feature->update();
            Session::flash('success_message', 'A feature has been Updated successfully');
            return Redirect::to('feature');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $featureInfo = Feature::where('id', $id)->first();

     if(!empty($featureInfo)){
        
        $featureInfo->status = 5;
        $featureInfo->updated_at = date('Y-m-d');
        $featureInfo->update();
        Session::flash('success_message', 'A product feature has been Remove successfully');
        return Redirect::back();

    }
    else{
        return abort('404');
    }
    }

    public function categoryWisefeature($categoryId){
        $categoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();

        if(!empty($categoryInfo)){

          $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
        $data['features'] = Feature::where('status', '!=' ,5)->where('category_id', $categoryId)->get();
        $data['categoryInfo'] = $categoryInfo;

        return view('feature.index', $data);  
        }
        else{
            return $this->index();
        }
        
    }
}
