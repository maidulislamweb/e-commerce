<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use Redirect;
use App\Category;
use App\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
        $data['brands'] = Brand::where('status', '!=' ,5)->get();
        $data['activeMenu'] = 'menu-open';
        return view('brand.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $brand = new Brand;
            $brand->title = $request->title;
            $brand->category_id = $request->category;
            $brand->status = 1;
            $brand->order_id = Brand::where('category_id', $request->category)->max('order_id') + 1;
            $brand->created_at = date('Y-m-d');
            $brand->updated_at = null;
// dd($brand);
            $brand->save();
            Session::flash('success_message', 'A brand has been Inserted successfully');
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brandInfo = Brand::where('id', $id)->first();

        if(!empty($brandInfo)){

            $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();

            $data['brand'] = $brandInfo;
            $data['activeMenu'] = 'menu-open';
            return view('brand.edit', $data); 
        }
        else{
            return abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'category' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $brand = Brand::find($id);
            $brand->title = $request->title;
            $brand->category_id = $request->category;
            $brand->status = $request->status;
            $brand->order_id = Brand::where('category_id', $request->category)->max('order_id') + 1;
            
            $brand->updated_at =  date('Y-m-d');
// dd($brand);
            $brand->update();
            Session::flash('success_message', 'A brand has been Updated successfully');
            return Redirect::to('brand');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $brandInfo = Brand::where('id', $id)->first();

       if(!empty($brandInfo)){
        
        $brandInfo->status = 5;
        $brandInfo->updated_at = date('Y-m-d');
        $brandInfo->update();
        Session::flash('success_message', 'A product brand has been Remove successfully');
        return Redirect::back();

    }
    else{
        return abort('404');
    }
}

public function categoryWiseBrand($categoryId){
    $categoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();

    if(!empty($categoryInfo)){

      $data['categories'] = Category::where('parent','!=', 0)->where('status', 1)->get();
      $data['brands'] = Brand::where('status', '!=' ,5)->where('category_id', $categoryId)->get();
      $data['categoryInfo'] = $categoryInfo;

      return view('brand.index', $data);  
  }
  else{
    return $this->index();
}

}
}
