<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Division;

class DivisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisions = Division::where('status', 1)->orderBy('order_id', 'ASC')->get();
        $data['divisions'] = $divisions;
        $data['activeMenu'] = 'menu-open';
        return view('division.division', $data);
    }
}
