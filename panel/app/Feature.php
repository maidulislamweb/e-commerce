<?php

namespace App;
use App\Category;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
	protected $fillable = [
        'category_id', 'title', 'status','order_id',
    ];


    public function getCategory(){
    	return $this->belongsTo(Category::class, 'category_id');
    }

}
