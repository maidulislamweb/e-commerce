-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2018 at 06:56 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ec`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `condition` enum('new','used','recondition') COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_year` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `features` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticity` enum('original','clone') COLLATE utf8mb4_unicode_ci NOT NULL,
  `regi_year` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `price_type` enum('fixed','negotiable') COLLATE utf8mb4_unicode_ci NOT NULL,
  `fuel_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `km` int(11) DEFAULT NULL,
  `bed` int(11) DEFAULT NULL,
  `bath` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `land_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no_type` enum('hide','show') COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('men','women','unisex') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `status`, `category_id`, `location_id`, `user_id`, `condition`, `model`, `model_year`, `title`, `brand_id`, `features`, `description`, `authenticity`, `regi_year`, `type_id`, `price`, `price_type`, `fuel_type`, `capacity`, `km`, `bed`, `bath`, `size`, `land_size`, `address`, `phone_no`, `phone_no_type`, `gender`, `user_location`, `user_ip`, `created_at`, `updated_at`) VALUES
(11, 1, 13, 38, 5, 'used', 'i5', NULL, 'Apple I5', 3, NULL, '<p>\r\n\r\nIPhone 7 PLUS 32GB Factory Unlock,Fully Brand New Condition Phone . <br>Came from Abroad With Full Boxed &amp; Cash Memo.<br>3 Days Replacement Guaranty &amp; 1 Years Service Warranty. <br>&lt;&lt;&lt; আমাদের প্রোডাক্ট ১০০%অরজিনাল কপি প্রমাণ হলে মূল্য ফেরত &gt;&gt;&gt; <br>&lt;&lt;&lt; এক দাম দামাদামি করবেন না কেউ দয়া করে &gt;&gt;&gt; <br>&lt;&lt;&lt; ঢাকার বাহিরে যে কোন জেলায় কুরিয়ারের মাধ্যমে condition-এ পণ্য পাঠানো হয়।&gt;&gt;&gt;<br>&lt;&lt;&lt; WholeSale নেওয়ার জন্য যোগাযোগ করতে পারেন।&gt;&gt;&gt; <br>&lt;&lt;&lt; A.R. Telecom\' Shop=76-78 Level-5 ,SAH ALLI PLAZA, Mirpur 10 Dhaka1216 &gt;&gt;&gt;<br>&lt;&lt;&lt; Fixed Price &gt;&gt;&gt;<br>SIM	Nano-SIM<br>DISPLAY	Type	LED-backlit IPS LCD, capacitive touchscreen, 16M colors<br>Size	5.5 inches, 83.4 cm2 (~67.7% screen-to-body ratio)<br>Internal	32 GB, 3 GB RAM<br>CAMERA	Primary	Dual: 12 MP <br>Secondary	7 MP (f/2.2, 32mm), <br>FEATURES	Sensors	Fingerprint <br>BATTERY	Non-removable Li-Ion 2900 mAh battery\r\n\r\n<br></p>', 'original', NULL, NULL, 20000, 'negotiable', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mirpur 10, Dhaka', '01918993427', 'hide', NULL, '1', '1', '2018-08-14 11:46:20', NULL),
(12, 1, 15, 9, 5, 'used', '2000', 1, 'Toyota Noah X-SILVER 2005', 5, NULL, '<p>\r\n\r\n</p><p>CONDITION: 1ST HAND USER</p><p>Series : X-Noah</p><p>Model : 2005 .</p><p>Registration : 2009 .</p><p>Mileage : 72000 KM.</p><p>Engine : V V T-I.</p><p>Displacement : 2000 CC.</p><p>Transmission : Automatic .</p><p>Color : SILVER.</p><p>Fuel System : Octane &amp; Cng</p><p>Options : Fully Auto and loaded.Built in DVD Navigation Back Camera. HID Light. , Fog Lamps. EBD, 2 Air Bag, 4 ABS Disk Brake, Anti Theft Security System, Super Cool AC, Tilt Power Steering, Power Windows &amp; Mirror, Central Lock, UVS &amp; Tempered Glass,All Power &amp; Full Option. No any accidental history,<br>All Papers up-to date.</p><p>Note : If you want to check anything you can take this car to any automobile analyzing center.</p>\r\n\r\n<br><p></p>', 'original', 2001, NULL, 240000, 'negotiable', 'diesel,cng,petrol,octane', 20, 300000, NULL, NULL, NULL, NULL, 'Mogbazar', '01918993428', 'hide', NULL, '1', '1', '2018-08-14 11:49:32', NULL),
(13, 1, 30, 9, 5, 'used', NULL, NULL, 'Original Leather Bag-Malaysia Bag', NULL, NULL, '<p>\r\n\r\n</p><p>Leather Waist Carrying Bag - Malaysian Leather For Men &amp; Women.</p><p>Fanny Pack with 4-Zipper Pockets, SAVFY Waist Bag Travel Pocket with Adjustable Belt For Workout Vacation Hiking, For iPhone 6, 6S Plus, Galaxy - S4, S5, S6, S7.</p><p>Product Details :<br>1. WITH 4 SEPARATE COMPARTMENT : The mian large pocket is roomy enough to hold cellphone, passport, small purse or other valuable items. With the two small pockets in the front and the Back, &amp; a pocket in the back, you could put your - Passport, keys, coins, cashes in it to protect your cell phone from scratched.</p><p>2. A PERFECT GIFT FOR TRAVELERS : Are your Families, or your Friends planning to travel ? Do you looking for the Best Travel Gifts for them ? This fanny pack is ideal for various activities such as Jogging, Walking, Cycling, Hiking, Holidays, Festivals and so on.</p><p>3. HIGH QUALITY MATERIAL : Made from superior Malaysian Leather, not only durable, but also light and compact for your comfortable Leather. A best choice for safe keeping when workout.</p><p>4. STRONG FLEXBILE BELT : The strap of waist bag is widely adjustable for most suitable for men and women. It is much more secure &amp; relaxed than carrying a bag when traveling or workout.</p><p>5. AMPLE STORAGE SPACE : Main compartment provides independent protection for your phones from screen scratches, cash, key and watch from Water, Sand, Dust and Dirt. It is easily for outdoor sports with no worries.</p><p>Colour : As Seen On Picture.<br>Note : High Quality Product.</p><p>Origin : 100% Malaysian Leather.<br>.----------------------.<br>★ অর্ডারের নিয়ম-০১ : ☺️-Note: মোবাইলে অর্ডার দিতে ক্লিক করুন-ডান পাশে/নিচে-Bikroy-(Buy Now)-বাটন-এ-[24 ঘণ্টার মধ্যে যে কোন সময় প্রোডাক্ট অর্ডার করতে পারেন].</p><p>★ অর্ডারের সিস্টেম-০২ : ☺️-অর্ডার করবার জন্য : বিক্রয় এর ডান/নিচে-[Contact : Ask the seller : ক্লিক করে-Chat/Massage]-অপশনে গিয়ে আপনার, নাম ও ঠিকানা ও মোবাইল নাম্বার দিন-{Cash On Delivery}-মাধ্যমে প্রোডাক্ট হাতে পেয়ে মূল্য পরিশোধ করুন ।</p><p>★রাসেল ইন্টারন্যাশনাল ইনক। ব্রান্ডের বিভিন্ন-ব্রান্ড নিউ প্রোডাক্ট অত্যন্ত সুলভ মূল্যে--[Cash On DeliverY]-Order Placed-করার মাধ্যমে বাংলাদেশে যে কোন জায়গায় প্রোডাক্ট পেতে পারেন।</p><p>- ডেলিভারি প্রক্রিয়া : ★ ঢাকা সিটি এর মধ্যে ২৪ ঘণ্টা এবং বাংলাদেশের অন্যান্য জেলাতে সর্বচ্ছ ৭২ ঘণ্টার মধ্যে ডেলিভারি সম্পন্ন করা হয় । <br>- কোন কারন বশত প্রোডাক্ট হাতে পাওয়ার পর ত্রুটি / নষ্ট থাকলে, সকল প্রোডাক্ট ৪৮ ঘণ্টার রিপ্লেস-মেন্ট গ্যারান্টি ।</p><p>★ অন্যান্য আইটেম লিস্টের জন্য : ডান পাশে/নিচে-[রাসেল ইন্টারন্যাশনাল]-Logo-অপশনে ক্লিক করুন-ব্রান্ডের সকল-প্রোডাক্ট আইটেম দেখার জন্য। <br>☺️★★★ RASEL INTERNATIONAL INC. - Solution For A Small Planet ★★★☺️<br>★★ Best Quality - Reasonable Price.<br>★ এখনই, অর্ডার করুন-আর-(ক্যাশ অন ডেলিভারিতে)-বুঝে নিন, আপনার পণ্যটি - ধন্যবাদ.</p>\r\n\r\n<br><p></p>', 'original', NULL, NULL, 2000, 'negotiable', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mohammadpur', '01918993427', 'hide', NULL, '1', '1', '2018-08-14 11:54:13', '2018-08-14 11:55:45'),
(14, 1, 14, 35, 2, 'new', 'Redmi 4', NULL, 'Xiaomi Redmi 4 .. (Used)', NULL, NULL, '<p>\r\n\r\n<b>sell sell sell No exchange</b><br>Only sell Only set<br>Ram2GB Rom16GB<br>camera 13+8 full ok<br>kono problem nai<br>fingerprints ace <i>4G OTG</i> Network 2G 3G 4G দিকে নিতে পারবেন আজকে বিতর নিলে কিছু কম রাগবো আমার টাকার খুব দরকার তাই বিএি করছে\r\n\r\n<br></p>', 'original', NULL, NULL, 2000, 'negotiable', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CTG', '01918993427', 'hide', NULL, '1', '1', '2018-08-14 23:55:35', NULL),
(15, 1, 30, 42, 2, 'new', NULL, NULL, 'brand new canvas bag abd', NULL, NULL, '<p>\r\n\r\n<b>*waterproof </b><br>*পাওয়ার বেংক কানেক্ট করে mobile চার্জ করার সুবিধা। <br>*easy and comoact.&nbsp; up dated<br></p>', 'original', NULL, NULL, 2000, 'negotiable', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jani na', '0191899342', 'hide', NULL, '1', '1', '2018-08-14 23:58:04', '2018-08-15 00:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `category_id`, `title`, `status`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 13, 'Aamra', 1, 1, NULL, NULL),
(2, 13, 'Acer', 1, 2, NULL, NULL),
(3, 13, 'Apple', 1, 3, NULL, NULL),
(4, 15, 'Toyota', 1, 1, '2018-08-10 18:00:00', NULL),
(5, 15, 'BMW', 1, 2, '2018-08-10 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `title`, `type`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', 0, 'electronics', NULL, 1, 1, NULL, NULL),
(2, 'Cars & Vehicles', 0, 'vehicles', NULL, 2, 1, NULL, NULL),
(3, 'Property', 0, 'property', NULL, 3, 1, NULL, NULL),
(4, 'Services', 0, 'services', NULL, 4, 1, NULL, NULL),
(5, 'Home & Garden', 0, 'home', NULL, 5, 1, NULL, NULL),
(6, 'Clothing, Health & Beauty', 0, 'fashion', NULL, 7, 1, NULL, NULL),
(7, 'Hobby, Sport & Kids', 0, 'hobby', NULL, 8, 0, NULL, NULL),
(8, 'Business & Industry', 0, 'business', NULL, 9, 0, NULL, NULL),
(9, 'Education', 0, 'education', NULL, 10, 0, NULL, NULL),
(10, 'Pets & Animals', 0, 'animals', NULL, 11, 0, NULL, NULL),
(11, 'Food & Agriculture', 0, 'food', NULL, 12, 0, NULL, NULL),
(12, 'Other', 0, 'other', NULL, 13, 0, NULL, NULL),
(13, 'Mobile Phones', 1, 'mobile', NULL, 1, 1, NULL, NULL),
(14, 'Mobile Phone Accessories', 1, 'mobile-accessories', NULL, 2, 1, NULL, NULL),
(15, 'Car', 2, 'car', 'property', 1, 1, '2018-07-25 18:00:00', NULL),
(16, 'Motorbikes & Scooters', 2, 'Motorbikes & Scooters', 'service', 2, 1, '2018-08-08 18:00:00', NULL),
(17, 'Bicycles and Three Wheelers', 2, 'Bicycles and Three Wheelers', 'service', 3, 1, '2018-08-08 18:00:00', NULL),
(18, 'Houses', 3, 'Houses', 'service', 1, 1, '2018-08-08 18:00:00', NULL),
(19, 'Plots & Land', 3, 'Plots & Land', 'service', 2, 1, '2018-08-08 18:00:00', NULL),
(20, 'Apartments & Flats', 3, 'Apartments & Flats', 'service', 3, 1, '2018-08-08 18:00:00', NULL),
(21, 'Tickets', 4, 'Tickets', 'service', 1, 1, '2018-08-08 18:00:00', NULL),
(22, 'Events & Hospitality', 4, 'Events & Hospitality', 'service', 2, 1, '2018-08-08 18:00:00', NULL),
(23, 'Travel & Visa', 4, 'Travel & Visa', 'service', 3, 1, '2018-08-08 18:00:00', NULL),
(24, 'Furniture', 5, 'Furniture', 'service', 1, 1, '2018-08-08 18:00:00', NULL),
(25, 'Electricity, AC, Bathroom & Garden', 5, 'Electricity, AC, Bathroom & Garden', 'service', 2, 1, '2018-08-08 18:00:00', NULL),
(26, 'Clothing', 6, 'Clothing', 'service', 1, 1, '2018-08-08 18:00:00', NULL),
(27, 'Shoes & Footwear', 6, 'Shoes & Footwear', 'service', 2, 1, '2018-08-08 18:00:00', NULL),
(28, 'Jewellery', 6, 'Jewellery', 'service', 3, 1, '2018-08-08 18:00:00', NULL),
(29, 'Watches', 6, 'Watches', 'service', 4, 1, '2018-08-08 18:00:00', NULL),
(30, 'Bags', 6, 'Bags', 'service', 5, 1, '2018-08-08 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item_types`
--

CREATE TABLE `item_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `title`, `parent`, `type`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', 'dhaka-division', 0, 'division', 1, 1, NULL, NULL),
(9, 'Savar', 'savar', 1, 'area', 6, 1, '2018-08-13 18:00:00', NULL),
(12, 'Chittagong', 'chittagong-division', 0, 'division', 9, 1, '2018-08-13 18:00:00', NULL),
(13, 'Sylhet', 'sylhet-division', 0, 'division', 10, 1, '2018-08-13 18:00:00', NULL),
(14, 'Khulna', 'khulna-division', 0, 'division', 11, 1, '2018-08-13 18:00:00', NULL),
(15, 'Rajshahi', 'rajshahi-division', 0, 'division', 12, 1, '2018-08-13 18:00:00', NULL),
(16, 'Rangpur', 'rangpur-division', 0, 'division', 13, 1, '2018-08-13 18:00:00', NULL),
(17, 'Barisal', 'barisal-division', 0, 'division', 14, 1, '2018-08-13 18:00:00', NULL),
(27, 'Dhaka', 'dhaka', 0, 'city', 15, 1, '2018-08-13 18:00:00', NULL),
(28, 'Chittagong', 'chittagong', 0, 'city', 16, 1, '2018-08-13 18:00:00', NULL),
(29, 'Khulna', 'khulna', 0, 'city', 17, 1, '2018-08-13 18:00:00', NULL),
(30, 'Rangpur', 'rangpur', 0, 'area', 18, 1, '2018-08-13 18:00:00', NULL),
(31, 'Barisal', 'barisal', 0, 'city', 19, 1, '2018-08-13 18:00:00', NULL),
(32, 'Rajshahi', 'rajshahi', 0, 'city', 20, 1, '2018-08-13 18:00:00', NULL),
(33, 'Rangpur', 'rangpur', 0, 'city', 21, 1, '2018-08-13 18:00:00', NULL),
(35, 'Anderkilla', 'anderkilla', 28, 'area', 22, 1, '2018-08-13 18:00:00', NULL),
(36, 'Baizid', 'baizid', 28, 'area', 23, 1, '2018-08-13 18:00:00', NULL),
(37, 'Mirpur', 'mirpur', 27, 'area', 24, 1, '2018-08-13 18:00:00', NULL),
(38, 'Dhanmondi', 'dhanmondi', 27, 'area', 25, 1, '2018-08-13 18:00:00', NULL),
(39, 'Mogbazar', 'mogbazar', 27, 'area', 26, 1, '2018-08-13 18:00:00', NULL),
(40, 'Faridpur', 'faridpur', 1, 'area', 27, 1, '2018-08-13 18:00:00', NULL),
(41, 'Coxbazar', 'coxbazar', 12, 'area', 28, 1, '2018-08-13 18:00:00', NULL),
(42, 'Rangamati', 'rangamati', 12, 'area', 29, 1, '2018-08-13 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_09_044313_create_categories_table', 2),
(4, '2018_02_09_113014_create_cities_table', 3),
(5, '2018_02_09_113314_create_city_areas_table', 3),
(6, '2018_02_09_113354_create_divisions_table', 3),
(7, '2018_02_09_113418_create_division_areas_table', 3),
(8, '2018_02_21_052806_create_ads_table', 4),
(9, '2018_02_21_165804_create_photos_table', 4),
(10, '2018_03_05_182817_create_resumes_table', 4),
(11, '2018_02_20_064859_create_brands_table', 5),
(12, '2018_02_20_160028_create_item_types_table', 5),
(13, '2018_02_21_094919_create_features_table', 5),
(15, '2018_08_14_140700_create_locations_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ads_id` int(11) NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `ads_id`, `photo`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'unsplash-600x300-5b56baf182d16.jpg', 1, '2018-07-23 23:36:49', NULL),
(2, 2, 'unsplash-600x300-5b56baf670d96.jpg', 1, '2018-07-23 23:36:54', NULL),
(3, 3, 'unsplash-600x300-5b56bb4a36e3e.jpg', 1, '2018-07-23 23:38:18', NULL),
(4, 4, 'unsplash-600x300-5b56c28d5728a.jpg', 1, '2018-07-24 00:09:17', NULL),
(5, 4, 'biman-5a91b69a4c64d-5b56c28d57e42.jpg', 1, '2018-07-24 00:09:17', NULL),
(7, 2, 'khaleda-5a93f738177f3-5b56c99a78ffb.jpg', 1, '2018-07-24 00:39:22', '2018-07-24 00:39:22'),
(8, 5, 'mobile-5b5990e0aca30.jpg', 1, '2018-07-26 03:14:08', NULL),
(9, 5, 'mobile1-5b5990e0ad5e8.jpg', 1, '2018-07-26 03:14:08', NULL),
(10, 6, 'mobile1-5b599111e8fd0.jpg', 1, '2018-07-26 03:14:57', NULL),
(11, 7, 'mobile-5b599166f3ed1.jpg', 1, '2018-07-26 03:16:23', NULL),
(12, 7, 'mobile1-5b59916700849.jpg', 1, '2018-07-26 03:16:23', NULL),
(13, 7, 'samsung_mobile_under_15000-5b59916701019.jpg', 1, '2018-07-26 03:16:23', NULL),
(14, 8, 'download-(1)-5b5991da96aa1.jpg', 1, '2018-07-26 03:18:18', NULL),
(15, 8, 'download-5b5991da97271.jpg', 1, '2018-07-26 03:18:18', NULL),
(16, 9, 'bag-5b6c14ede31ba.jpg', 1, '2018-08-09 04:18:21', NULL),
(17, 9, 'bag1-5b6c14ede6482.jpg', 1, '2018-08-09 04:18:21', NULL),
(18, 10, 'download-5b6e9f4941492.jpg', 1, '2018-08-11 02:33:13', NULL),
(19, 10, 'images-5b6e9f4941c62.jpg', 1, '2018-08-11 02:33:13', NULL),
(20, 10, 'download-(1)-5b6e9f4942432.jpg', 1, '2018-08-11 02:33:13', NULL),
(21, 10, 'images-5b6e9f4942c02.jpg', 1, '2018-08-11 02:33:13', NULL),
(23, 10, 'download-(1)-5b6ea219dc82d.jpg', 1, '2018-08-11 02:45:13', '2018-08-11 02:45:13'),
(24, 11, 'mobile-5b73156c7a7e7.jpg', 1, '2018-08-13 23:46:20', NULL),
(25, 11, 'mobile1-5b73156c7abcf.jpg', 1, '2018-08-13 23:46:20', NULL),
(26, 11, 'Why_Laravel-5b73156c7afb7.jpg', 1, '2018-08-13 23:46:20', NULL),
(27, 12, 'download-(1)-5b73162cd0f80.jpg', 1, '2018-08-13 23:49:32', NULL),
(28, 12, 'download-5b73162cdb778.jpg', 1, '2018-08-13 23:49:32', NULL),
(29, 12, 'download-5b73162cdbf48.jpg', 1, '2018-08-13 23:49:32', NULL),
(30, 12, 'images-5b73162cdc718.jpg', 1, '2018-08-13 23:49:32', NULL),
(31, 12, 'download-(1)-5b73162cdcb00.jpg', 1, '2018-08-13 23:49:32', NULL),
(32, 13, 'bag-5b731745237b9.jpg', 1, '2018-08-13 23:54:13', NULL),
(33, 13, 'bag1-5b73174523f89.jpg', 1, '2018-08-13 23:54:13', NULL),
(34, 14, 'mobile-5b73c057ba863.jpg', 1, '2018-08-14 23:55:35', NULL),
(35, 14, 'samsung_mobile_under_15000-5b73c057bb033.jpg', 1, '2018-08-14 23:55:35', NULL),
(36, 15, 'bag1-5b73c0ec8aeac.jpg', 1, '2018-08-14 23:58:04', NULL),
(37, 15, 'bag-5b73c0ec8b67c.jpg', 1, '2018-08-14 23:58:04', NULL),
(38, 15, 'download-(1)-5b73c4cd41171.jpg', 1, '2018-08-15 00:14:37', '2018-08-15 00:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `resumes`
--

CREATE TABLE `resumes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `division_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `highest_edu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special_edu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exprience` int(11) DEFAULT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_join` date DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_period` int(11) DEFAULT NULL,
  `yourself` longtext COLLATE utf8mb4_unicode_ci,
  `current_role` longtext COLLATE utf8mb4_unicode_ci,
  `current_salary` int(11) DEFAULT NULL,
  `expected_salary` int(11) DEFAULT NULL,
  `marital_status` enum('married','unmarried') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` longtext COLLATE utf8mb4_unicode_ci,
  `present_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resumes`
--

INSERT INTO `resumes` (`id`, `user_id`, `gender`, `dob`, `city_area`, `division_area`, `linkedin`, `highest_edu`, `special_edu`, `institute`, `exprience`, `organization`, `last_join`, `designation`, `notice_period`, `yourself`, `current_role`, `current_salary`, `expected_salary`, `marital_status`, `cv`, `present_address`, `permanent_address`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 'male', '2004-01-25', '1', NULL, NULL, 'graduate', 'accounting', 'Govt Banglacollege', 2, 'sdsd', '2000-01-01', 'no', 20, NULL, NULL, 1, 2000000, 'unmarried', 'coverletter-5b6bde23627db.docx', '271/B/1, Boro Magbazar, Dhaka-1217', 'Bhola', 5, '2018-08-09 01:49:21', '2018-08-09 01:49:21'),
(2, 6, 'male', '2004-01-06', '1', NULL, 'https://mybdjobs.bdjobs.com/mybdjobs/masterview.asp', 'diploma', 'info_technology', 'IDB-BISEW', 2, 'The Data Soft', '2000-10-10', 'Developer', 30, 'To be a great php and angularJS developer (Programmer) in the world. I want to proved myself by coding and creating any dynamic web application and it it will be 100% responsive in any device. I like object oriented programming.', 'To be a great php and angularJS developer (Programmer) in the world. I want to proved myself by coding and creating any dynamic web application and it it will be 100% responsive in any device. I like object oriented programming.', 240000, 300000, 'married', 'coverletter-5b6c040989bfd.docx', '271/B/1, Uttara, Dhaka-1217', 'Barishal', 5, '2018-08-09 02:36:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `membership_type`, `photo`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md Maidul Islam', 'admin@gmail.com', NULL, 'member', '', '$2y$10$zzAJAgHTEb/Cqpp613rSV.XU5AsBr7icDH2JIkUelXUWPpx6adMOO', 'y5TVrHKHYGqxyTlNU1E86KIOWR5XejGdxaKYNpNfHvYGDuZnuohXaZmtCFv2', '2018-02-08 21:31:27', '2018-02-08 21:31:27'),
(2, 'mahi', 'mahibc1@gmail.com', NULL, NULL, '', '$2y$10$zzAJAgHTEb/Cqpp613rSV.XU5AsBr7icDH2JIkUelXUWPpx6adMOO', 'MvtZj72ndejdiFB0LVqVEcMOKQoBoKHrKj83OWjtG5jHsX9Chz3eDSxTylKh', '2018-07-23 21:34:24', '2018-07-23 21:34:24'),
(3, 'mahi', 'maidul.tech@gmail.com', NULL, '', '', '$2y$10$dL.VzeD22C9pQBCtjoOaOu.OzmySUr.zk8rkYWFpJHH2OivSvrg2G', '34zKKZPoqAqNMfMexgqR9DEC48yNn7GpNmjtjDxTLzyhL5Zk3NonGbY1n8Lj', '2018-07-23 21:44:22', '2018-07-23 21:44:22'),
(4, 'mahi', 'admin1@gmail.com', NULL, '', '', '$2y$10$KHGcfWn.h7FuG8o6evHMA.h.rDK98RK/Wf4s3Frre2DXpi0ww.YA.', 'xCrgbWtRKXQIRHzXBk4EXublieABUgArYLxoTbNHySNthbBmQ5L7yBpqiTdT', '2018-07-23 21:46:32', '2018-07-23 21:46:32'),
(5, 'Maidul Islam', 'mahi@gmail.com', '01918993427', 'member', 'Tanbira_Alam-5b6bd78f1ce8c.jpg', '$2y$10$vqUuev4MQshtUIxkkVysG.3GTMp5ydAVNfkVAT1Idkjk7VYuVbXiS', 'MQuHXnfp2orQeg2hf4PwIYEpERpxrqyWiKt1g1tVCQayzhdPqUKLiun1RIGw', '2018-08-08 23:42:00', '2018-08-08 23:56:31'),
(6, 'Arif', 'arif@gmail.com', '017189999999', 'member', 'sabiha-5b6bfd3e8886e.jpg', '$2y$10$B1I3/eklRhO0Do8jNk4HNunBNHmeoEC6ygHvBXeGjjFcobBcaRigi', 'SqJKAKU7jWwVilfBM9m0jQG1590QPgin1aldLbr2vo49tSXq02HTtrmx3lGT', '2018-08-09 02:28:48', '2018-08-09 03:53:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_types`
--
ALTER TABLE `item_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resumes`
--
ALTER TABLE `resumes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_types`
--
ALTER TABLE `item_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `resumes`
--
ALTER TABLE `resumes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
