<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->integer('category_id');
            $table->integer('location_id')->nullable();
           $table->integer('user_id');
            $table->enum('condition', ['new', 'used', 'recondition']);
            $table->string('model')->nullable();
            $table->integer('model_year')->nullable();
            $table->string('title')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('features')->nullable();
            $table->text('description');
            $table->enum('authenticity', ['original', 'clone']);
            $table->integer('regi_year')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('price');
            $table->enum('price_type', ['fixed', 'negotiable']);
            $table->string('fuel_type')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('km')->nullable();
            $table->integer('bed')->nullable();
            $table->integer('bath')->nullable();
            $table->integer('size')->nullable();
            $table->string('land_size')->nullable();
            $table->string('address');
            $table->string('phone_no');
            $table->enum('phone_no_type', ['hide', 'show']);
            $table->enum('gender', ['men', 'women', 'unisex']);
            $table->string('user_location')->nullable();
            $table->string('user_ip')->nullable();
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
