<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('gender', ['male', 'female', 'other'])->nullable();
            $table->date('dob')->nullable();
          
            $table->string('city_area')->nullable();
            $table->string('division_area')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('highest_edu')->nullable();
            $table->string('special_edu')->nullable();
            $table->string('institute')->nullable();
            $table->integer('exprience')->nullable();
            $table->string('organization')->nullable();
            $table->date('last_join')->nullable();
            $table->string('designation')->nullable();
            $table->integer('notice_period')->nullable();
            $table->longText('yourself')->nullable();
            $table->longText('current_role')->nullable();
            $table->integer('current_salary')->nullable();
            $table->integer('expected_salary')->nullable();
            $table->enum('marital_status', ['married', 'unmarried'])->nullable();
            $table->longText('cv')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
