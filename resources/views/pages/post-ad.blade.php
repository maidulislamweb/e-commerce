@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE; padding-top: 20px;">
 
  <section style="padding-bottom: 80px;">
   <div class="container">
    <div class="well bg-f" style="margin-bottom: 10px;">
     <div class="row" style="margin-bottom: 80px;">
      <div class="col-md-10 col-md-offset-1">
       <h3>Welcome <span>{{Auth::user()->name}}</span>! Let's post an ad. Choose any option below:</h3>
     </div>
   </div>
   <div class="row">
   <div class="col-md-1"></div>
    <div class="col-md-5">
      <div class="boxed-area">
        <div class="box-title">
          <img width="72" height="72" src="{{asset('assets/logo/offer-1x-c3448312.png')}}">
        </div>
        <h3 class="t-center">Sell something:</h3>
        <ul class="list-unstyled dash-menu">
          <li><a href="{{url('/post-ad/item')}}" class="@if(isset($dashboard)) {{$dashboard}} @endif">Sell an item or service <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
          <li><a href="{{url('/post-ad/property')}}" class="@if(isset($membership)) {{$membership}} @endif">Offer a property for rent <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
          <li><a href="{{url('/post-ad/job')}}" class="@if(isset($resume)) {{$resume}} @endif">Post a job vacancy <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

        </ul>

      </div>
    </div>
    <div class="col-md-5">
      <div class="boxed-area">
        <div class="box-title">
          <img width="72" height="72" src="{{asset('assets/logo/wanted-1x-0487f1af.png')}}">
        </div>
        <h3 class="t-center">Look for something:</h3>
           <ul class="list-unstyled dash-menu">
          <li><a href="javascript:void(0)" class="@if(isset($dashboard)) {{$dashboard}} @endif">Look for property to rent <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
          <li><a href="javascript:void(0)" class="@if(isset($membership)) {{$membership}} @endif">Look for something to buy<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
          
          
        </ul>
      </div>
    </div>
     <div class="col-md-1"></div>

  </div>
  <hr>
  <div class="row">
    <div class="col-md-12">
      <div class="text-center">
       
          <img src="{{asset('assets/logo/ad-limits-lg-2x-e46d4edd.png')}}" style="width: 72px; display: inline;">
        
        <div style="display: inline-block; padding-left: 20px; vertical-align: middle;">
          <h4>Your free posting allowance</h4>
          <a href="" class="h-off">Learn about posting ads for free on Bikroy</a>
        </div>
      </div>
    </div>
  </div>
</div>



   <div class="well bg-f" style="margin-bottom: 10px;">
     <div class="row" style="">
      <div class="col-md-12">
        <h3>Quick rules</h3>
        <h4>All ads posted on Bikroy.com must follow our rules:</h4>
      </div>
     
     </div>

     <div class="row" style="">
      <div class="col-md-6">
        <ul class="ul-style">
          <li>Make sure you post in the correct category.</li>
          <li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
          <li>Do not upload pictures with watermarks.</li>
        </ul>
      </div>
      <div class="col-md-6">
        <ul class="ul-style">
          <li>Do not post ads containing multiple items unless it's a package deal.</li>
          <li>Do not put your email or phone numbers in the title or description.</li>
        </ul>
      </div>
     </div>
      <div class="row" style="">
      <div class="col-md-12">
        <a href="" class="h-off pull-right" >Click here to see all of our posting rules <i class="fa fa-angle-right"></i></a>
      </div>
    </div>
   </div>

</div>
</section>

</div>
@endsection

