@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE; padding-top: 20px;">

  <section style="padding-bottom: 80px;">
   <div class="container">
    <div class="well bg-f" style="margin-bottom: 10px;">
      <h3>Sell an item or service</h3>
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb no-margin">
            <li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{url('/post-ad')}}">Post Ad</a></li>
            <li><a href="{{url('/post-ad/item')}}">Item & Service</a></li>
            <li>{{$categoryInfo->name}}</li>
            
          </ol>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4">
          <h4>Select a subcategory...</h4>
          <div style="padding-left: 20px; padding-right: 40px;">
            <ul class="list-unstyled cat-list ul-style1">
              @if(!empty($subcategories))
              @foreach($subcategories as $key => $category)
              <li><a href="{{url('post-ad/item/category/'.$category->id)}}" class="h-off">{{$category->name}} </a></li> 

              @endforeach
              @endif
            </ul>
          </div>
        </div>
 


      </div>
    </div>
  </section>

</div>
@push('footer-asset')
<script type="text/javascript">
 $(document).on("click", ".categoryItem", function () {
  var id = $(this).data('id');
  // alert(id);
  $('#ulSubcategory').empty();

  $.ajax({
    type: 'GET',
    url: '{{URL("ajax/post-ad/category/item")}}'+"/"+id,

    success: function (data) {

      if(data.category.name != null){
        $("#subcategoryTitle").text(data.category.name);

        $.each(data.subcategory, function() {
          $('#ulSubcategory').append( $("<li class='scat-list'></li>").text(this.name).val(this.id) );
        });

      }
    }
  });

})
</script>

<script type="text/javascript">
 $(document).on("click", ".scat-list", function () {
  var id = $(this).attr("value");
  var url = $('.siteUrl').val();
  alert(url);
  window.location.href = url+'/post-ad/category/item/'+id;
    // alert(id);
  })
</script>
@endpush
@endsection

