@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE; padding-top: 20px;">

	<section style="padding-bottom: 10px;">
		<div class="container">
			<div class="well bg-f" style="margin-bottom: 10px; padding: 20px;">

				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">

						<div class="row">
							<div class="col-xs-12">
								@if(Session::has('info_message'))
								<div class="alert alert-warning">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Warning! </strong> {{ Session::get('info_message') }}.
								</div>
								@endif

								@if(Session::has('success_message'))

								<div class="alert alert-success">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success! </strong> {{ Session::get('success_message') }}.
								</div>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h3 class="heading">Sell an item or service</h3>
								<hr>
								<p class="cat-title"><span style="color: #139375;"><i class="fa fa-dot-circle-o"></i> Category: </span> {{$categoryInfo->name}} <i class="fa fa-long-arrow-right" ></i> {{$subCategoryInfo->name}} <a href="{{url('post-ad/item')}}" class="pull-right h-off">Change</a> </p>
							</div>
						</div>

						<form method="POST" enctype="multipart/form-data"  action="{{url('/store-ads/item/'.$subCategoryInfo->id)}}">
							{{csrf_field()}}

							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="location">Location : </label>
												<select class="form-control" id="location" name="location">
													@if(!empty($cities))
													<optgroup label="City">
														@foreach($cities as $key => $city)
														<option value="{{$city->id}}">{{$city->name}}</option>
														@endforeach
													</optgroup>
													@endif
													@if(!empty($divisions))
													<optgroup label="Division">
														@foreach($divisions as $key => $division)
														<option value="{{$division->id}}">{{$division->name}} Division</option>
														@endforeach
													</optgroup>
													@endif
												</select>
												@if($errors->has('location'))
												<p class="text-danger">must be required</p>
												@endif
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="area"> Area : </label>
												<select class="form-control" id="locationArea" name="location">
													@if(!empty($areas))
													@foreach($areas as $key => $area)
													<option value="{{$area->id}}">{{$area->name}}</option>
													@endforeach
													@endif
												</select>
											</div>
											@if($errors->has('area'))
											<p class="text-danger">must be required</p>
											@endif
										</div>
									</div>
								</div>
							</div>
							<br>
							<div style="border: 1px solid rgb(232, 184, 184); padding: 15px;">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<h4 class="heading">Add photos (5 for free)</h4>
											<table class="table appended_tr">
												<tbody>
													<tr>
														<td>
															<div class="fileupload fileupload-new" data-provides="fileupload">
																<span class="fileupload-preview fileupload-exists thumbnail" style="max-width: 75px; max-height: 75px;"></span>

																<label class="btn btn-primary btn-file btn-sm">
																	<span class="fileupload-new"><i class="fa fa-picture-o"></i> Select Image</span>
																	<span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
																	<input type="file" name="image[]" id="image" />
																</label>
																<a href="#" class="btn fileupload-exists btn-default btn-sm" data-dismiss="fileupload">
																	<i class="fa fa-times"></i> Remove
																</a>
															</div>
														</td>
														<td></td>
													</tr>
												</tbody>
											</table>
											@if($errors->has('image'))
											<p class="text-danger">one photo must be required</p>
											@endif
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<div class="" style="margin-right: 50px">
												<button type="button" class="add_more_photo btn btn-default btn-sm btn-rounded">Add More Photo</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group" style="margin-bottom: 20px;">
										<p style="color: #139375;">Condition</p>
										<label class="radio-inline"><input type="radio" name="condition" checked="checked" value="used" />Used</label>
										<label class="radio-inline"><input type="radio" name="condition" value="new" />New</label>
										@if($errors->has('condition'))
										<p class="text-danger">must be required</p>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									@if(!empty($brands) && (count($brands)>0))
									<div class="form-group" style="margin-bottom: 20px;">
										<p style="color: #139375;">Brand</p>
										<select class="form-control" name="brand">
											<option value="">Brand  </option>
											@foreach($brands as $brand)
											<option value="{{$brand->id}}">{{$brand->title}}</option>
											@endforeach
										</select>

									</div>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									@if(!empty($types) && (count($types)>0))
									<div class="form-group" style="margin-bottom: 20px;">
										<p style="color: #139375;">Type</p>
										<select class="form-control" name="type">
											<option value="">type  </option>
											@foreach($types as $type)
											<option value="{{$type->id}}}">{{$type->title}}</option>
											@endforeach

										</select>
									</div>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									@if(!empty($features) && (count($features)>0))
									<div class="form-group" style="margin-bottom: 30px;">
										<p style="color: #139375;">Features (optional)</p>
										@foreach($features as $feature)
										<label class="checkbox-inline"><input type="checkbox" value="{{$feature->id}}" name="feature[]">{{$feature->title}}</label>
										@endforeach

									</div>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<!-- <div class="form-group" style="margin-bottom: 20px;">
										<p style="color: #139375;">Description</p>
										<textarea class="form-control" id="textarea" rows="8" cols="30" maxlength="2000" name="description">{{old('description')}}</textarea>
										<div id="textarea_feedback"></div>
										@if($errors->has('description'))
										<p class="text-danger">must be required</p>
										@endif
									</div> -->
									<div class="box">
										<div class="box-header">
											<h3 class="box-title">Description</h3>
											<!-- tools box -->
											<div class="pull-right box-tools">
												<button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
												title="Collapse">
												<i class="fa fa-minus"></i></button>
												<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip"
												title="Remove">
												<i class="fa fa-times"></i></button>
											</div>
											<!-- /. tools -->
										</div>
										<!-- /.box-header -->
										<div class="box-body pad">
											<!-- <form> -->
												<textarea class="wysihtml5" placeholder="Place some text here"
												style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="description">{{old('description')}}</textarea>
												@if($errors->has('description'))
										<p class="text-danger">must be required</p>
										@endif
											<!-- </form> -->
										</div>
									</div>
								</div>
							</div>

							@if($categoryInfo->id == 1 || $categoryInfo->id == 2)
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Model</p>
										<input type="text" name="model" class="form-control" value="{{old('model')}}" />
									</div>
								</div>
							</div>
							@endif

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Title</p>
										<input type="text" name="title" class="form-control" value="{{old('title')}}" />
									</div>
								</div>
							</div>

							@if($categoryInfo->id == 2)
							<!--2=car&vehicles,-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Model Year</p>
										<input type="number" name="model_year" class="form-control" value="{{old('model_year')}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Registration Year</p>
										<input type="number" name="regi_year" class="form-control" value="{{old('regi_year')}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group" style="margin-bottom: 30px;">
										<p style="color: #139375;">Fuel Type</p>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="diesel">Diesel</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="cng">CNG</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="petrol">Petrol</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="octane">Octane</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="other">Other fuel type</label>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Engine Capacity(cc)</p>
										<input type="number" name="capacity" class="form-control" value="{{old('capacity')}}" />
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Kilometers Run(km)</p>
										<input type="number" name="km" class="form-control" value="{{old('km')}}" />
									</div>
								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 32 || $subCategoryInfo->id == 34)
							<!--Apartments & Flats = 32, Houses =34-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Beds</p>
										<input type="number" name="bed" class="form-control" value="{{old('bed')}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Baths</p>
										<input type="number" name="bath" class="form-control" value="{{old('bath')}}" />
									</div>
								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 34)
							<!--Houses =34-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="border: 1px solid #e8b8b8; margin-left: 0px; padding-top: 15px; margin-right: 0px; padding-bottom: 15px;">
											<div class="col-md-6">
												<p style="color: #139375;">Land Size</p>
												<input type="number" name="land_size" class="form-control" />
											</div>
											<div class="col-md-6">
												<p style="color: #139375;">Unit</p>
												<select name="size_unit" class="form-control">
													<option value="">unit</option>
													<option value="katha">katha</option>
													<option value="bigha">Bigha</option>
													<option value="acre">Acre</option>
													<option value="decimal">Decimal</option>
													<option value="shotok">Shotok</option>

												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 32)
							<!--Apartments & Flats = 32, -->
							<div class="row">
								<div class="col-md-12">

									<div class="form-group">
										<p style="color: #139375;">Size(sqft)</p>
										<input type="number" name="size" class="form-control" value="{{old('size')}}" />
									</div>
								</div>
							</div>
							@endif
							@if($subCategoryInfo->id == 48 || $subCategoryInfo->id == 49)
							<!--clothing = 48, shoes & footwear =49-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Gender</p>
										<label class="radio-inline"><input type="radio" name="gender" checked="checked" value="men">Men</label>
										<label class="radio-inline"><input type="radio" name="gender" value="women">Women</label>
										<label class="radio-inline"><input type="radio" name="gender" value="unisex">Unisex</label>
									</div>
								</div>
							</div>
							@endif


							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Authenticity</p>
										<label class="radio-inline"><input type="radio" name="authenticity" checked="checked" value="original">Original</label>
										<label class="radio-inline"><input type="radio" name="authenticity" value="clone">Replica / Clone / Other</label>
										@if($errors->has('authenticity'))
										<p class="text-danger">must be required</p>
										@endif
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Price (Tk)</p>
										<input type="number" name="price" class="form-control" value="{{old('price')}}" />
										<label class="checkbox-inline"><input type="checkbox" value="negotiable" name="price_type">Negotiable</label>
										@if($errors->has('price'))
										<p class="text-danger">must be required</p>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Address</p>
										<input type="text" name="address" class="form-control" value="{{old('address')}}" />
										@if($errors->has('address'))
										<p class="text-danger">must be required</p>
										@endif
									</div>
								</div>
							</div>

							<div class="row" style="border: 1px solid rgb(232, 184, 184); margin-left: 0px; padding-top: 15px; margin-right: 0px; padding-bottom: 15px;">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Contact details</p>
										<hr style="margin-top: 0px;">
										<p>Name: <span>{{Auth::user()->name}}</span></p>

										<p>Phone number</p>
										<input type="text" name="phone_no" class="form-control" placeholder="Your phone number" value="{{old('phone_no')}}" />
										<label class="checkbox-inline"><input type="checkbox" value="hide" name="phone_no_type">Hide phone number</label>
										@if($errors->has('phone_no'))
										<p class="text-danger">must be required</p>
										@endif
										<br>
										<br>
										<p>Email: <span>{{Auth::user()->email}}</span></p>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<button type="submit" class="btn btn1">Post ad</button>
									</div>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<section style="background-color: #E7EDEE; ">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>By posting this ad, you agree to the Terms & Conditions of this site.</p>
			</div>
		</div>

		<div class="well bg-f" style="margin-bottom: 10px;">
			<div class="row" style="">
				<div class="col-md-12">
					<h3>Quick rules</h3>
					<h4>All ads posted on Bikroy.com must follow our rules:</h4>
				</div>
			</div>
			<div class="row" style="">
				<div class="col-md-6">
					<ul class="ul-style">
						<li>Make sure you post in the correct category.</li>
						<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
						<li>Do not upload pictures with watermarks.</li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="ul-style">
						<li>Do not post ads containing multiple items unless it's a package deal.</li>
						<li>Do not put your email or phone numbers in the title or description.</li>
					</ul>
				</div>
			</div>
			<div class="row" style="">
				<div class="col-md-12">
					<a href="" class="h-off pull-right" >Click here to see all of our posting rules <i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>

@push('footer-asset')
<script>
	$(document).ready(function() {
		var max_fields      = 5; 
		var wrapper         = $(".appended_tr"); 
		var add_button      = $(".add_more_photo"); 
		var x = 1; 
		$(add_button).click(function(e){ 
			e.preventDefault();
			if(x < max_fields){ 
				x++; 
				$(wrapper).append('<tr><td><div class="fileupload fileupload-new" data-provides="fileupload"><span class="fileupload-preview fileupload-exists thumbnail" style="max-width: 75px; max-height: 75px;"></span> <label class="btn btn-primary btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span><input type="file" name="image[]"></label> <a href="#" class="btn fileupload-exists btn-default btn-sm" data-dismiss="fileupload"> <i class="fa fa-times"></i> Remove</a></span></div></td></td><td style="width:50px"><i class="fa fa-trash-o fa-2x text-dark remove_field" title="remove the field"></i></td></tr>');
			}
			else{
				alert('You can not upload more then 5 photos !');
			}
		});

		$(wrapper).on("click",".remove_field", function(e){ 
			e.preventDefault(); $(this).closest('tr').remove(); x--;
		})
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var text_max = 2000;
		$('#textarea_feedback').html(text_max + ' characters remaining');

		$('#textarea').keyup(function() {
			var text_length = $('#textarea').val().length;
			var text_remaining = text_max - text_length;

			$('#textarea_feedback').html(text_remaining + ' characters remaining');
		});
	});
</script>
<script type="text/javascript">
	$('body').on('change', '#location', function() {
		var locationId = $(this).val();

		// alert(locationId);
		$('#locationArea').empty();
		$.ajax({
			type: 'GET',
			url: '{{URL("ajax/area/")}}'+"/"+locationId,
			success: function (data) {
				if(data.areas != null){
					$.each(data.areas, function() {
						$('#locationArea').append( $("<option ></option>").text(this.name).val(this.id) );
					});
				}
			}
		});

	});
</script>

@endpush

@endsection

