@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="adv text-center">

            <a href="javascript:void(0)" target="_blank">
              <img src="{{asset('assets/adv/ad.jpg')}}" class="img-responsive">
            </a>

          </div>
        </div>
      </div>
      <div class="row">
      	<div class="col-xs-12">
      		<p><a href="{{url('/')}}">Home</a> -> <a href="{{url('/ads')}}"> All ads</a>  -> <a href="{{url('/ads/'.$location->title)}}">{{$location->name}}</a>  -> <a href="{{url('/ads/'.$area->title)}}">{{$area->name}} </a> -> <span><a href="{{url('/ads/')}}?catId={{$category->id}}">{{$category->name}}</a></span> -> <span><a href="{{url('/ads/')}}?catId={{$subCategory->id}}">{{$subCategory->name}}</a> </span></p>
      	</div>
      </div>
      <div class="row">
      	<div class="col-xs-12">
      		<div class="panel well">
      			<div class="panel panel-header">
      				<p>non woven color bag</p>
      				<p>For sale by SILME BAG INDUSTRIES LTD <small>9 Aug 1:48 pm Savar, Dhaka</small></p>
      			</div>
      		</div>
      	</div>
      </div>
      <div class="well">
        <div class="row">
         <div class="col-md-7">
          <div class="ad-photo-slide" style=" background-color: #e7edee;">
           <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:700px;height:580px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
              <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{asset('img/spin.svg')}}" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:700px;height:500px;overflow:hidden;">

              @if(!empty($photos))
              @foreach($photos as $key => $photo)
              <div>
                <img data-u="image" src="{{asset('../uploads/'.$photo->photo)}}" />
                <img data-u="thumb" src="{{asset('../uploads/'.$photo->photo)}}" />
              </div>
              @endforeach
              @endif



            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
              <div data-u="slides">
                <div data-u="prototype" class="p" style="width:190px;height:90px;">
                  <div data-u="thumbnailtemplate" class="t"></div>
                  <svg viewbox="0 0 16000 16000" class="cv">
                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                  </svg>
                </div>
              </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
              <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
              </svg>
            </div>
            <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
              <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
              </svg>
            </div>
          </div>
          <!-- #endregion Jssor Slider End -->
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <div class="ui-price">
              <div class="ui-price-tag"><span class="l1"></span><span class="l2"></span>Tk <span class="amount">{{$adInfo->price}}</span><span class="r1"></span><span class="r2"></span><span class="r3"></span></div>
            </div>
            <div>
              <p>
                @php echo $adInfo->description @endphp
              </p>
            </div>
            <hr>
            <div>
             <p>Contact</p>
           </div>
           <div>
            <p><span><i class="fa fa-phone" aria-hidden="true"></i></span>  {{$adInfo->phone_no}}</p>
          </div>
          <div>
            <p><span><i class="fa fa-comments-o" aria-hidden="true"></i></span>  Chat</p>
          </div>
          <br>
          <div>
            <button class="btn btn-default btn-block">Promote This Ad</button>
          </div>
        </div>
        <div class="col-md-4">
          <div>
            @if($adInfo->condition)
            <p> <strong>Condition: </strong>{{$adInfo->condition}}</p>
            @endif

            @if($adInfo->brand_title)
            <p><strong>Brand: </strong> {{$adInfo->brand_title}}</p>
            @endif

            @if($adInfo->model)
            <p><strong>Model: </strong> {{$adInfo->model}}</p>
            @endif

            @if($adInfo->features)
            <p><strong>Features: </strong>{{$adInfo->features}}</p>
            @endif


            @if($adInfo->authenticity)
            <p><strong>Authenticity: </strong>{{$adInfo->authenticity}}</p>
            @endif
            @if($adInfo->regi_year)
            <p><strong>regi_year: </strong>{{$adInfo->regi_year}}</p>
            @endif
            @if($adInfo->fuel_type)
            <p><strong>fuel_type: </strong>{{$adInfo->fuel_type}}</p>
            @endif
            @if($adInfo->capacity)
            <p><strong>Engine capacity: </strong>{{$adInfo->capacity}}</p>
            @endif
            @if($adInfo->km)
            <p><strong> Kilometers run: </strong>{{$adInfo->km}}</p>
            @endif
            @if($adInfo->bed)
            <p><strong>bed: </strong>{{$adInfo->bed}}</p>
            @endif
            @if($adInfo->bath)
            <p><strong>bath: </strong>{{$adInfo->bath}}</p>
            @endif
            @if($adInfo->size)
            <p><strong>size: </strong>{{$adInfo->size}}</p>
            @endif
            @if($adInfo->land_size)
            <p><strong>land_size: </strong>{{$adInfo->land_size}}</p>
            @endif
            @if($adInfo->gender)
            <p><strong>gender: </strong>{{$adInfo->gender}}</p>
            @endif
            @if($adInfo->address)
            <p><strong>address: </strong>{{$adInfo->address}}</p>
            @endif







          </div>
          <hr>
          <div>
            <p><strong><i class="fa fa-star-o"></i> Save ad as Favorite</strong></p>
            <p><strong><i class="fa fa-adjust"></i> Report this ad</strong> </p>
          </div>
        </div>
      </div>



    </div>
    <div class="col-md-5">
     <h4>Share this ad</h4>
     <hr/> 
     <button type="button" class="btn btn-fb"><i class="fa fa-facebook pr-1"></i> Facebook</button>
     <button type="button" class="btn btn-tw"><i class="fa fa-twitter pr-1"></i> Twitter</button>
     <button type="button" class="btn btn-gplus"><i class="fa fa-google-plus pr-1"></i> Google +</button>
     <button type="button" class="btn btn-li"><i class="fa fa-linkedin pr-1"></i> Linkedin</button>
   </div>
 </div>
</div>
<div class="well">
  <p>More ads from Gadget & Electronics Products</p>
  <div class="row">
    <div>
            <ul class="list-unstyled user-ad-list">
              @if(!empty($relAds))
              @foreach($relAds as $key => $relAd)

              @php 
             $thumb = \App\Http\Controllers\AdsController::getThumbPhoto($relAd->id);
              @endphp
              <li class="col-md-6">
                <div class="media well">
                  <div class="media-left">
                    <img src="{{asset('/../uploads/'.$photo->photo)}}" class="media-object" style="width:160px">
                 
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <a href="{{url('ads/'.$relAd->id)}}" class="h-off"> {{$relAd->title}}</a>

                    </h4>
                    <p>{{$relAd->address}}</p>
                    <p>TK: {{$relAd->price}}</p>
                  </div>
                  <p class="update-time">{{$relAd->created_at}}</p>
                </div>
              </li>
              @endforeach
              @endif
            </ul>
          </div>
  </div>

</div>
</div>
</section>

@push('footer-asset')

<link rel="stylesheet" href="{{asset('css/jssor.slider-healper.css')}}">
<script src="{{asset('assets/js/jssor.slider-27.4.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/jssor.slider-healper.js')}}" type="text/javascript"></script>
@endpush

@endsection