@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE; padding-top: 20px;">

  <section style="padding-bottom: 80px;">
   <div class="container">
    <div class="well bg-f" style="margin-bottom: 10px;">
      <h3>Where is your item or service located?</h3>
      <hr>
      <p class="cat-title"><i class="fa fa-dot-circle-o"></i> Category: {{$categoryInfo->name}} <i class="fa fa-long-arrow-right" ></i> {{$subCategoryInfo->name}} <a href="{{url('/post-ad/item')}}" class="pull-right h-off">Change</a> </p>
      <div class="row">
        <div class="col-md-4">
          <h4>Select City or Division</h4>

          <div style="padding-left: 20px; padding-right: 40px; margin-bottom: 40px;">
            <h5>Cities</h5>
            <ul class="list-unstyled ul-style1">
              @if(!empty($cities))
              @foreach($cities as $key => $city)
              <li><a href="{{url('post-ad/city/'.$city->id.'/category/'.$categoryInfo->id)}}" class="h-off cityId" data-id="{{$city->id}}">{{$city->name}} </a></li> 

              @endforeach
              @endif
            </ul>
          </div>
          <div style="padding-left: 20px; padding-right: 40px; margin-bottom: 40px;">
            <h5>Divisions</h5>
            <ul class="list-unstyled cat-list ul-style1">
              @if(!empty($divisions))
              @foreach($divisions as $key => $division)
              <li><a href="{{url('post-ad/division/'.$division->id.'/category/'.$categoryInfo->id)}}" class="h-off divisionId" data-id="{{$division->id}}">Division {{$division->name}} </a></li> 

              @endforeach
              @endif
            </ul>
          </div>

          
        </div>
        <div class="col-md-8"></div>


      </div>
      </div>
    </div>
  </section>

</div>
@push('footer-asset')

<!--area by city-->
<script type="text/javascript">
 $(document).on("click", ".cityId", function () {
  var id = $(this).data('id');
  $('.dinamic-location-division').hide();
  $('.dinamic-location-area').show();
  $('#ulCityArea').empty();

  $.ajax({
    type: 'GET',
    url: '{{URL("ajax/post-ad/location/city/")}}'+"/"+id,

    success: function (data) {

      if(data.city.name != null){
        $("#cityTitle").text(data.city.name);

        $.each(data.areas, function() {
          $('#ulCityArea').append( $("<option ></option>").text(this.name).val(this.id) );
        });

      }
    }
  });

})
</script>


<!--area by division-->
<!-- <script type="text/javascript">
 $(document).on("click", ".divisionId", function () {
  var id = $(this).data('id');
   $('.dinamic-location-area').hide();
  $('.dinamic-location-division').show();
 
  $('#divisionArea').empty();

  $.ajax({
    type: 'GET',
    url: '{{URL("ajax/post-ad/location/division/")}}'+"/"+id,

    success: function (data) {

      if(data.division.name != null){
        $("#divisionTitle").text(data.division.name);

        $.each(data.areas, function() {
          $('#divisionArea').append( $("<option ></option>").text(this.name).val(this.id) );
        });

      }
    }
  });

})
</script> -->

<!-- <script type="text/javascript">
 $(document).on("click", ".scat-list", function () {
  var id = $(this).attr("value");
  var url = $('.siteUrl').val();
  alert(url);
  window.location.href = url+'/post-ad/category/item/'+id;
 
  })
</script> -->
@endpush
@endsection

