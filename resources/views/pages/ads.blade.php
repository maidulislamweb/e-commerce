@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="adv text-center">

            <a href="javascript:void(0)" target="_blank">
              <img src="{{asset('assets/adv/ad.jpg')}}" class="img-responsive">
            </a>

          </div>
        </div>
      </div>
    </div>
  </section>

  
  <section >
   <div class="container">
    <div class="well bg-f" style="margin-bottom: 10px;">
     <div class="row">
      <div class="col-md-6">
        <div>
          <button class="btn btn-success" id="location" style="width: 45%; background-color: #139275;" data-toggle="modal" data-target="#locationModal" data-locationId=" @isset($location) {{$location->id}}  @endisset"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp; @if(!empty($location)){{$location->name}} {{$location->type}}  @else Select Location @endif</button>
          <button class="btn btn-success" style="width: 45%; background-color: #139275;"><i class="fa fa-tags" aria-hidden="true"></i>&nbsp;&nbsp;Select Category</button>
        </div>
      </div>
      <div class="col-md-6">
        <div class="master-search">
          <div class="input-group" id="adv-search">
            <input type="text" class="form-control" placeholder="Search for snippets" />
            <div class="input-group-btn">
              <div class="btn-group" role="group">
                <div class="dropdown dropdown-lg">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label for="filter">Filter by</label>
                        <select class="form-control">
                          <option value="0" selected>All Snippets</option>
                          <option value="1">Featured</option>
                          <option value="2">Most popular</option>
                          <option value="3">Top rated</option>
                          <option value="4">Most commented</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="contain">Author</label>
                        <input class="form-control" type="text" />
                      </div>
                      <div class="form-group">
                        <label for="contain">Contains the words</label>
                        <input class="form-control" type="text" />
                      </div>
                      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </form>
                  </div>
                </div>
                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>


</div>
</section>


<section>
  <div class="container">
    <div class="well bg-f">
      <div class="row">
        <div class="col-md-3" style="border-right: 3px solid #E7EDEE;">
          <div class="search-br">

            <div class="search-part">
              <p class="b-b">Sort results by:</p>
              <form action="" method="post">

                <div class="form-group">

                  <select  class="form-control">
                    <option>Date: Newest on top</option>
                    <option>Date: Oldest on top</option>
                    <option>Price: High to Low</option>
                    <option>Price: Low to High</option>
                  </select>

                </div>

              </form>
            </div>

            <div class="search-part">
              <p class="b-b">Type of poster</p>
              <form action="" method="post">

                <div class="form-group">
                  <div class="radio">
                    <label><input type="radio" name="optradio" {{ $by_paying_member == '0' ? 'checked' : ''}}  value="0" class="adType">All posters</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="radio">
                    <label><input type="radio" {{ $by_paying_member == '1' ? 'checked' : ''}} name="optradio" value="1" class="adType">Only members</label>
                  </div>
                </div>

              </form>
            </div>
            
            <div class="search-part">
              <p class="b-b">Category:</p>
              <strong><a href="{{Request::url()}}?by_paying_member={{$by_paying_member}}">All Categories</a> </strong>
              <div>
                <ul class="list-unstyled cat-list">
                  @foreach($categories as $category)
                  @php
                  $totalAds = \App\Http\Controllers\SearchController::getTotalAdByCategory($category->id);
                  @endphp
                  <li><a href="{{Request::url()}}?by_paying_member={{$by_paying_member}}&catId={{$category->id}}"  class="{{ $category->id == $catId ? 'location-active' : ''}}">{{$category->name}}<span> ({{$totalAds}})</span></a></li>

                  @endforeach
                </ul>
              </div>
            </div>
            <div class="search-part">
              <p class="b-b">Location:</p>
              <strong><a href="{{url('/ads')}}">All of Bangladesh</a> </strong>
              <div>
                <ul class="list-unstyled cat-list">
                  @if(!empty($cities))
                  @foreach($cities as $city)

                  @php
                  $totalAds = \App\Http\Controllers\SearchController::getTotalAdByCity($city->id);
                  @endphp
                  <li><a href="{{url('ads/'.$city->title.'?by_paying_member='.$by_paying_member.'&catId='.$catId)}}" class="@if($location != null) {{ $city->id == $location->id ? 'location-active' : ''}} @endif">{{$city->name}} {{$city->type}} ({{$totalAds}})</a></li>
                  @endforeach
                  @endif


                  @if(!empty($divisions))
                  @foreach($divisions as $division)

                  @php
                  $totalAds = \App\Http\Controllers\SearchController::getTotalAdByCity($division->id);
                  @endphp
                  <li><a href="{{url('ads/'.$division->title.'?by_paying_member='.$by_paying_member.'&catId='.$catId)}}" class="@if($location != null) {{ $division->id == $location->id ? 'location-active' : ''}} @endif">{{$division->name}} Division ({{$totalAds}})</a></li>
                  @endforeach
                  @endif
                  


                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div>
            <h4><span><a href="{{url('/')}}">Home</a> </span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span> <a href="{{url('/ads')}}">All ads</a>  in  @if(!empty($location)){{$location->name}}  @else Bangladesh @endif</span></h4>
          </div>
          <div><p>Showing 1-25 of 176,049 ads
          Ads in Banglade</p></div>
          <hr>
          <div>
            <ul class="list-unstyled user-ad-list">
              @if(!empty($ads) && (count($ads) > 0))
              @foreach($ads as $key => $ad)

              @php 
              $thumb = \App\Http\Controllers\AdsController::getThumbPhoto($ad->id);
              @endphp
              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="{{url('/../uploads/'.$thumb->photo)}}" class="media-object" style="width:160px">

                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <a href="{{url('ads-detail/'.$ad->id)}}" class="h-off"> {{$ad->title}}</a>

                    </h4>
                    <p>@if($ad->membership_type != '') <span class="badge">{{$ad->membership_type}}</span> @endif{{$ad->city_area_name}} <span>,{{$ad->categoryName}}</span> </p>
                    <p>TK: {{$ad->price}}</p>
                  </div>
                  <p class="update-time">{{$ad->created_at}}</p>
                </div>
              </li>
              @endforeach
              @else
              <p class="text-center text-danger well">no ads</p>
              @endif
            </ul>
          </div>
        </div>
        <div class="col-md-2">
          <div class="adv">
            <a href="">
              <img src="{{ asset('assets/adv/ad.gif') }}" class="img-responsive">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div>


<!-- location Modal -->
<div id="locationModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-6">

            <ul class="list-group">
              <li class="list-group-item disabled">Select a City </li>
              <!-- <li class="list-group-item disabled">Cities</li> -->
              @if($cities)
              @foreach($cities as $city)
              @php 
              if(!empty($location)){
              $locationId  = $location->id;
            }else{
            $locationId  = '-1';
          }


          @endphp

          <li class="list-group-item"><a class="locationId {{ $city->id == $locationId ? 'location-active' : ''}}" href="javascript:void(0)" locationId={{$city->id}}>{{$city->name}} City</a></li>
          @endforeach
          @endif

              @if($divisions)
              <li class="list-group-item disabled">Division</li>
              @foreach($divisions as $division)
              <li class="list-group-item"><a href="javascript:void(0)" class="locationId {{ $city->id == $locationId ? 'location-active' : ''}}" locationId={{$division->id}}>{{$division->name}} Division</a></li>
              
              @endforeach
              @endif

            </ul>
          </div>
          <div class="col-md-6">

            <ul class="list-group" id="cityArea">
              <li class="list-group-item disabled">Select a local area within Dhaka</li>
              @if(!empty($areas))
              @foreach($areas as $area)
              <li class="list-group-item"><a href="{{url('ads/'.$area->title)}}">{{$area->name}}</a></li>
              
              @endforeach 
              @endif

            </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<input type="hidden" name="currentUrl" class="currentUrl" value="{{Request::url()}}">

@push('footer-asset')

<script type="text/javascript">
  $('.locationId').on('click', function () {

    var locationId = $(this).attr('locationId');
    var city = $(this).text();
    $(this).addClass('location-active');

    var siteUrl = $('.siteUrl').val();

    $('#cityArea').html("");
    $.ajax({
      type: 'GET',
      url: '{{URL("ajax/location-area")}}'+"/"+locationId,

      success: function (data) {
        $('#cityArea').append('<li class="list-group-item disabled">Select a local area within <span> '+ city +'</span></li>')
        $.each(data, function (index, value) {
          $('#cityArea').append('<li class="list-group-item"><a href="'+siteUrl+'/ads/'+value.locationTitle+'">' +value.locationName+ '</a></li>');

        });

      }
    });
  });
</script>

<script type="text/javascript">
 var siteUrl = $('.currentUrl').val();
 $('.adType').on('click', function () {
  var type = $(this).val();

  var url = siteUrl+'?by_paying_member='+type;
  window.location.href = url;
    // alert(url);
  });


</script>

@endpush
@endsection

