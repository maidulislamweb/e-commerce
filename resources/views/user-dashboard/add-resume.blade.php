@extends('layouts.app')
@push('header-asset')

<link href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

@endpush

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
        <div class="row">
          <div class="col-md-3">
           @include('user-dashboard._dashboard_left')
         </div>
         <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              @if(Session::has('info_message'))
              <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning! </strong> {{ Session::get('info_message') }}.
              </div>
              @endif

              @if(Session::has('success_message'))

              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success! </strong> {{ Session::get('success_message') }}.
              </div>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">

              <div class="dash-n-h">
               <h3>My resume</h3>
             </div>
             <div>
               <p style="padding-top: 15px;">Let's start with adding some details about yourself.</p>

               <ul class="ui-steps has-4 profile-steps is-short is-mt-12"><li class=" is-current"><div class="steps-title"><span class="number"><a href="{{url('/cerate-resume')}}">1</a></span><span class="label"></span></div><div class="steps-content"></div></li><li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-2')}}">2</a></span><span class="label"></span></div><div class="steps-content"></div></li><li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-3')}}">3</a> </span><span class="label"></span></div><div class="steps-content"></div></li><li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-4')}}">4</a></span><span class="label"></span></div><div class="steps-content"></div></li></ul>

               <h4>Personal details</h4>
               <hr>
             </div>
             <div>

              <div>
                <div class="media">
                  <div class="media-left">
                    @if(Auth::user()->photo)
                    <img src="{{asset('profile-photo/'.Auth::user()->photo)}}" class="media-object" style="width:60px; border-radius: 50%;">
                    @else
                    <img src="{{asset('profile-photo/default.png')}}" class="media-object" style="width:60px; border-radius: 50%;">
                    @endif
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#myModal">Change Photo</button>
                    </h4>
                    <p>Supported file formats jpg, jpeg, png (standard size 300X300)</p>

                  </div>
                </div>
                <hr>
                <form action="{{ url('/add-resume') }}" autocomplete="off" method="post">
                  {{csrf_field()}}

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="name">Name :</label>
                        <input type="text" class="form-control" id="name" name="name" autofocus=""  value="{{Auth::user()->name}}" />
                        @if($errors->has('name'))
                        <p class="text-danger">must be required</p>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6">
                     <div class="form-group">
                      <label for="phone">Phone:</label>
                      <input type="text" class="form-control" id="phone"  name="phone"  value="{{Auth::user()->phone}}" />
                      @if($errors->has('phone'))
                      <p class="text-danger">must be required</p>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="gender">Gender (optional)</label>
                      <select class="form-control" name="gender">
                        <option value="male" @if(!empty($info->gender)) {{ $info->gender == 'male' ? 'selected' : '' }} @else {{ old('gender') == 'male' ? 'selected' : '' }}  @endif>Male</option>

                        <option value="female" @if(!empty($info->gender)) {{ $info->gender == 'female' ? 'selected' : '' }} @else {{ old('gender') == 'female' ? 'selected' : '' }} @endif>Female</option>

                        <option value="other" @if(!empty($info->gender)) {{ $info->gender == 'other' ? 'selected' : '' }} @else {{ old('gender') == 'other' ? 'selected' : '' }} @endif>Other</option>
                      </select>

                    </div>
                  </div>
                  <div class="col-md-6">
                   <div class="form-group">
                    <label for="dob">Date of birth:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker" name="dob" placeholder="yyyy-mm-dd" @if(!empty($info->dob)) value="{{$info->dob}}" @else value="{{old('dob')}}" @endif >

                    </div>
                    @if($errors->has('dob'))
                    <p class="text-danger">must be required</p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="location">Location:</label>
                    <select class="form-control" id="location" name="location">
                      @if(!empty($cities))
                      <optgroup label="City">
                        @foreach($cities as $key => $city)
                        <option value="city-{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                      </optgroup>
                      @endif
                      @if(!empty($divisions))
                      <optgroup label="Division">
                        @foreach($divisions as $key => $division)
                        <option value="division-{{$division->id}}">{{$division->name}} Division</option>
                        @endforeach
                      </optgroup>
                      @endif
                    </select>
                    @if($errors->has('location'))
                    <p class="text-danger">must be required</p>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                   <label for="area">Area:</label>
                   <select class="form-control" id="locationArea" name="area">

                    @if(!empty($cityAreas))
                    @foreach($cityAreas as $cityArea)
                    <option value="{{$cityArea->id}}">{{$cityArea->name}}</option>
                    @endforeach
                    @endif
                   
                  </select>
                  @if($errors->has('area'))
                  <p class="text-danger">must be required</p>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="LinkedIn">LinkedIn Profile (optional):</label>
                  <input type="text" class="form-control" id="linkedin" placeholder="" name="linkedin" @if(!empty($info->linkedin)) value="{{$info->linkedin}}" @else value="{{old('linkedin')}}" @endif>
                </div>
              </div>
              <div class="col-md-6">

              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
               {{-- <a href="{{url('/add/personal/step-2')}}" class="btn btn-success btn1 btn-lg btn-block" style="margin-top: 20px;">Continue</a> --}}
               <button type="submit" class="btn btn-success btn1 btn-lg btn-block" style="margin-top: 20px;">Continue</button> 
             </div>
             <div class="col-md-6">

             </div>
           </div>


         </form>

       </div>

     </div>


   </div>
 </div>
</div>
</div>
</div>
</div>
</section>
</div>

<!-- Modal for updating photo -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose a photo for resume</h4>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data"  action="{{url('/update-profile-photo')}}">
          {{csrf_field()}}
          <div class="form-group">
            <input type="file" class="form-control" id="profile-photo" name="profile_photo" />
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@push('footer-asset')

<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
   //Date picker
   $('#datepicker').datepicker({
    autoclose: true,
    startDate:'1918-03-05',
    endDate:'2004-03-05',
    format:'yyyy-mm-dd'
  });
 });
</script>

<script type="text/javascript">
  $('body').on('change', '#location', function() {
    var locationId = $(this).val();
    $('#locationArea').empty();
    $.ajax({
      type: 'GET',
      url: '{{URL("ajax/area/")}}'+"/"+locationId,
      success: function (data) {
        if(data.areas != null){
          $.each(data.areas, function() {
            $('#locationArea').append( $("<option ></option>").text(this.name).val(this.id) );
          });
        }
      }
    });

  });
</script>

@endpush
@endsection

