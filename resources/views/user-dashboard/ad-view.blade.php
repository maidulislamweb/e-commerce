@extends('layouts.app')

@section('content')
<style type="text/css">
ul#image-gallery>li>img{
  width: 100%;
}
</style>
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
        <a href="{{url('/dashboard')}}"><i class="fa fa-arrow-left" style="font-size: 24px;"></i></a>

        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
            <div>
              <div style="font-size: 12px;">
                <ul class="breadcrumb">
                  <li><a href="javascript:void(0)">Home</a></li>
                  <li><a href="javascript:void(0)">All ads</a></li>
                  @if(!empty($location))
                  <li><a href="javascript:void(0)">{{$location->name}}</a></li>
                  @endif
                  @if(!empty($area))
                  <li><a href="javascript:void(0)">{{$area->name}}</a></li>
                  @endif
                  @if(!empty($parentCategory))
                  <li><a href="javascript:void(0)">{{$parentCategory->name}}</a></li>
                  @endif
                  @if(!empty($category))
                  <li><a href="javascript:void(0)">{{$category->name}}</a></li>
                  @endif
                  @if(!empty($ad->title))
                  <li>{{$ad->title}}</li>
                  @endif
                </ul>
              </div>
              <h2>
                @if(!empty($ad->title))
                <span style="vertical-align: -webkit-baseline-middle;">{{$ad->title}}</span>
                @endif
                @if($ad->status == 5) <span class="label label-danger" style="font-size: 10px;">pending</span> @endif @if($ad->status == 1) <span class="label label-success" style="font-size: 10px;">published</span> @endif </h2>
                <p>For sale by <span style="text-transform: uppercase; color: #008973;">{{Auth::user()->name}}</span> &nbsp; @if(!empty($ad->created_at))<span>{{date("d M H:i a",strtotime($ad->created_at))}}</span>, @endif @if(!empty($ad->address))<span>{{$ad->address}}</span> @endif</p>
                <hr>
              </div>
              @if(!empty($photos) && (count($photos)))


              <div class="demo" style="background-color: #E6EDEE; padding-top: 20px; padding-bottom: 20px;">
                <div class="item">            
                  <div class="clearfix" style="width: 100%;">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                     @foreach($photos as $key => $photo)
                     
                     <li data-thumb="{{asset('../uploads/'.$photo->photo)}}"> 
                      <img src="{{asset('../uploads/'.$photo->photo)}}" class="" />
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
            <hr>
            @endif

            <div class="row">
             <div class="col-md-6 col-sm-12">
               <div style="">
                <div>
                  <div class="well">
                    @if(!empty($ad->price))
                    <p>TK {{$ad->price}}</p>
                    @endif

                    @if(!empty($ad->price_type))
                    <p>Type: {{$ad->price_type}}</p>
                    @endif
                  </div>
                  

                </div>
                @if(!empty($ad->description))
                <div>
                  <p>@php echo $ad->description @endphp</p>
                </div>
                @endif

                <div>
                  <h4>Contact</h4>
                  <hr>
                  @if(!empty($ad->phone_no))
                  <p><i class="fa fa-phone"></i> {{$ad->phone_no}}</p>
                  @if(!empty($ad->phone_no_type))
                  <p>{{$ad->phone_no_type}}</p>
                  @endif
                  <hr>
                  @endif

                  <p><i class="fa fa-comments"></i> Chat</p>
                  <hr>
                  <p><i class="fa fa-envelope"></i> Reply by email</p>
                </div>


              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="ads-nfo">
                @if(!empty($ad->address))
                <p><strong>Address:</strong> {{$ad->address}}</p>
                @endif
                @if(!empty($ad->condition))
                <p><strong>Condition:</strong> {{$ad->condition}}</p>
                @endif
                @if(!empty($ad->type_id))
                <p><strong>Item type:</strong> {{$ad->type_id}}</p>
                @endif
                @if(!empty($ad->type))
                <p><strong>Type:</strong> {{$ad->type}}</p>
                @endif

                @if(!empty($ad->land_size))
                <p><strong>Land size:</strong> {{$ad->land_size}}</p>
                @endif

                @if(!empty($ad->size))
                <p><strong>Size:</strong> {{$ad->size}}</p>
                @endif

                @if(!empty($ad->bed))
                <p><strong>Beds:</strong>{{$ad->bed}}</p>
                @endif

                @if(!empty($ad->bath))
                <p><strong>Baths:</strong> {{$ad->bath}}</p>
                @endif

                @if(!empty($ad->brand))
                <p><strong>Brand:</strong> {{$ad->brand}}</p>
                @endif

                @if(!empty($features))

                <p><strong>Features:</strong> 
                  @foreach($features as $key => $feature)
                  <span>{{ $feature->title }}</span>
                  @if (!$loop->last) , @endif
                  @endforeach
                </p>
                @endif

                @if(!empty($ad->model))
                <p><strong>Model:</strong> {{$ad->model}}</p>
                @endif
                @if(!empty($ad->model_year))
                <p><strong>Model year:</strong> {{$ad->model_year}}</p>
                @endif
                @if(!empty($ad->regi_year))
                <p><strong>Registration year:</strong> {{$ad->regi_year}}</p>
                @endif
                @if(!empty($ad->capacity))
                <p><strong>Engine capacity:</strong> {{$ad->capacity}}</p>
                @endif

                @if(!empty($ad->fuel_type))
                <p><strong>Fuel type:</strong> {{$ad->fuel_type}}</p>
                @endif
                @if(!empty($ad->km))
                <p><strong>Kilometers run:</strong> {{$ad->km}} km</p>
                @endif
                @if(!empty($ad->authenticity))
                <p><strong>Authenticity:</strong> {{$ad->authenticity}}</p>
                @endif




              </div>
            </div>
          </div> 

        </div>

      </div>

    </div>
  </div>
</section>

</div>
@push('footer-asset')

<script>
 $(function(){
  $("#content-slider").lightSlider({
    loop:true,
    keyPress:true
  });
  $('#image-gallery').lightSlider({
    gallery:true,
    item:1,
    thumbItem:9,
    slideMargin: 0,
    speed:500,
    auto:false,
    loop:true,
    onSliderLoad: function() {
      $('#image-gallery').removeClass('cS-hidden');
    }  
  });
});
</script>
@endpush
@endsection

