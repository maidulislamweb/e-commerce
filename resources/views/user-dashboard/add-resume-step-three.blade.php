@extends('layouts.app')
@push('header-asset')

<link href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

@endpush

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
        <div class="row">
          <div class="col-md-3">
           @include('user-dashboard._dashboard_left')
         </div>
         <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              @if(Session::has('info_message'))
              <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning! </strong> {{ Session::get('info_message') }}.
              </div>
              @endif

              @if(Session::has('success_message'))

              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success! </strong> {{ Session::get('success_message') }}.
              </div>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">

              <div class="dash-n-h">
               <h3>My resume</h3>
             </div>
             <div>
               <p style="padding-top: 15px;">Let's start with adding some details about yourself.</p>

               <ul class="ui-steps has-4 profile-steps is-short is-mt-12">
                <li class="is-past"><div class="steps-title"><span class="number"><a href="{{url('/cerate-resume')}}">1</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li class="is-past"><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-2')}}">2</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li class="is-current"><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-3')}}">3</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-4')}}">4</a></span><span class="label"></span></div><div class="steps-content"></div></li>
              </ul>

              <h4>Professional details</h4>
              <hr>
            </div>
            <div>

              <div>

                <form action="{{url('/add-resume-step-three')}}" autocomplete="off" method="post">
                  {{csrf_field()}}

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Total year of exprience :</label>
                        <input type="number" class="form-control"  name="exprience" autofocus="" value="{{old('exprience')}}" />
                        @if($errors->has('exprience'))
                        <p class="text-danger">must be required</p>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6">
                     <div class="form-group">
                      <label >Current organization:</label>
                      <input type="text" name="organization"  class="form-control" value="{{old('organization')}}" />
                      @if($errors->has('organization'))
                      <p class="text-danger">must be required</p>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label >Role / Designation</label>
                      <input type="text" name="designation" class="form-control" value="{{old('designation')}}" />
                      @if($errors->has('designation'))
                      <p class="text-danger">must be required</p>
                      @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                   <div class="form-group">
                    <label for="notice_period">Notice period (in days):</label>
                    <input type="number" name="notice_period" class="form-control" value="{{old('notice_period')}}" />
                    @if($errors->has('notice_period'))
                    <p class="text-danger">must be required</p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Current Salary (per month)(TK):</label>
                    <input type="number" class="form-control"  placeholder="" name="current_salary" value="{{old('current_salary')}}">
                    @if($errors->has('current_salary'))
                    <p class="text-danger">must be required</p>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="notice_period">Joining Date in Cureent Organization :</label>
                  <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right datepicker" name="last_join" placeholder="yyyy-mm-dd" @if(!empty($info->last_join)) value="{{$info->last_join}}" @else value="{{old('last_join')}}" @endif >

                    </div>
                    @if($errors->has('last_join'))
                    <p class="text-danger">must be required</p>
                    @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label >About Yourself (optional) :</label>
                  <textarea class="form-control" id="about_yourself" rows="5" maxlength="2000" name="yourself">{{old('yourself')}}</textarea>
                  <div id="textarea_about_yourself"></div>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="location">About The Current Role (optional):</label>

                  <textarea class="form-control" id="about_role" rows="5" maxlength="2000" name=" current_role">{{old('current_role')}}</textarea>
                  <div id="textarea_about_role"></div>
                </div>
              </div>

            </div>
            
            <div class="row">
              <div class="col-md-6">
               <button type="submit" class="btn btn-success btn1 btn-lg btn-block" style="margin-top: 20px;">Continue</button> 
             </div>
             <div class="col-md-6">
              <a href="{{url('/create-resume-step-4')}}" class="btn btn-success btn1 btn-lg btn-block" style="margin-top: 20px;">Skip</a> 
            </div>
          </div>


        </form>

      </div>

    </div>


  </div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>




@push('footer-asset')

<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
   //Date picker
   $('#datepicker').datepicker({
    autoclose: true,
    startDate:'05/03/1918',
    endDate:'05/03/2004',
    format:'dd/mm/yyyy'
  });
 });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var text_max = 2000;
    $('#textarea_about_yourself').html(text_max + ' characters remaining');

    $('#about_yourself').keyup(function() {
      var text_length = $('#about_yourself').val().length;
      var text_remaining = text_max - text_length;

      $('#textarea_about_yourself').html(text_remaining + ' characters remaining');
    });
  });

</script>
<script type="text/javascript">
  $(document).ready(function() {
    var text_max = 2000;
    $('#textarea_about_role').html(text_max + ' characters remaining');

    $('#about_role').keyup(function() {
      var text_length = $('#about_role').val().length;
      var text_remaining = text_max - text_length;

      $('#textarea_about_role').html(text_remaining + ' characters remaining');
    });
  });

</script>

@endpush
@endsection

