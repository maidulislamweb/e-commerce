@extends('layouts.app')
@push('header-asset')

<link href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

@endpush

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well" style="margin-top: 25px; padding-bottom: 50px;">
        <div class="row">
          <div class="col-md-3">
           @include('user-dashboard._dashboard_left')
         </div>
         <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              @if(Session::has('info_message'))
              <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning! </strong> {{ Session::get('info_message') }}.
              </div>
              @endif

              @if(Session::has('success_message'))

              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success! </strong> {{ Session::get('success_message') }}.
              </div>
              @endif
            </div>
          </div>




          <div class="panel">
            <div class="panel-heading panel-heading-01"><i class="glyphicon glyphicon-eye-open icon-padding"></i> View Resume</div>
            <div class="panel-body panel-body-02">


              <div class="row">
                <div class="col-xs-12">
                 <h4 style="font-weight:bold; color: #333;">Download Resume <a href=""><img src="{{asset('assets/img/Word.gif')}}" title="save cv in word" width="18" height="15" hspace="4" vspace="10" align="absmiddle"></a></h4>
                 <hr>
                 

               </div>
             </div>
             <div class="row">
              <div class="col-md-6">
                <h4 class="applicantsName "> {{Auth::user()->name}}</h4>
                <p>Address: <span>{{$myResume->present_address}}</span> </p>
                 <p> Mobile No <span>{{ Auth::user()->phone }}</span> </p>
                 
               <p> e-mail : <span>{{ Auth::user()->email }}</span></p>
              </div>
              <div class="col-md-6">
               @if(Auth::user()->photo)
               <img src="{{asset('profile-photo/'.Auth::user()->photo)}}" class="pull-right" style="width:150px; height: 150px;">

               @endif
             </div>
      <!--      <div class="media">
            <div class="media-left">
              @if(Auth::user()->photo)
              <img src="{{asset('profile-photo/'.Auth::user()->photo)}}" class="media-object" style="width:60px; border-radius: 50%;">

              @endif
            </div>
            <div class="media-body">
              <h4 class="media-heading"> {{Auth::user()->name}}</h4>
              <p><span><i class="fa fa-phone"></i>&nbsp;</span>{{Auth::user()->phone}}</p>

            </div>
          </div> -->



        </div>
        <br>
        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td>{{Auth::user()->email}}</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>:</td>
                  <td>{{$myResume->gender}}</td>
                </tr>
                <tr>
                  <td>Marital Status</td>
                  <td>:</td>
                  <td>{{$myResume->marital_status}}</td>
                </tr>

                <tr>
                  <td>Date of birth</td>
                  <td>:</td>
                  <td>{{date('M d, Y', strtotime($myResume->dob))}}</td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td>:</td>
                  <td>{{$location->name}}</td>
                </tr>
                <tr>
                  <td>Area</td>
                  <td>:</td>
                  <td>{{$area->name}}</td>
                </tr>

                <tr>
                  <td>LinkedIn Profile</td>
                  <td>:</td>
                  <td>{{$myResume->linkedin}}</td>
                </tr>

              </table>
            </div>
          </div>
        </div>

        <br>
        <div class="row">
          <div class="col-xs-12">
            <div class="clearfix" style="margin-bottom: 10px;">
              <h4 class="crear-fix">Education Information <a href="javascript:void(0)" class="pull-right btn btn-sm btn-default" style="padding-left: 20px; padding-right: 20px;">Edit</a> </h4>
            </div>

            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Highest education level</td>
                  <td>:</td>
                  <td>{{$myResume->highest_edu}}</td>
                </tr>
                <tr>
                  <td>Educational specialization</td>
                  <td>:</td>
                  <td>{{$myResume->special_edu}}</td>
                </tr>
                <tr>
                  <td>Institute / University</td>
                  <td>:</td>
                  <td>{{$myResume->institute}}</td>
                </tr>

              </table>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-xs-12">
            <div class="clearfix" style="margin-bottom: 10px;">
              <h4>Professional Information <a href="javascript:void(0)" class="pull-right btn btn-sm btn-default" style="padding-left: 20px; padding-right: 20px;">Edit</a> </h4>
            </div>
            <div class="table-responsive">
             <table class="table">
              <tr>
                <td>Total years of experience</td>
                <td>:</td>
                <td>{{$myResume->exprience}}</td>
              </tr>
              <tr>
                <td>About yourself</td>
                <td>:</td>
                <td>{{$myResume->yourself}}</td>
              </tr>


            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="clearfix" style="margin-bottom: 10px;">
            <h4>Current employments <a href="javascript:void(0)" class="pull-right btn btn-sm btn-default" style="padding-left: 20px; padding-right: 20px;">Edit</a> </h4>
          </div>
          <div class="table-responsive">
           <table class="table">
            <tr>
              <td>Current Organization</td>
              <td>:</td>
              <td>{{$myResume->organization}}</td>
            </tr>

            <tr>
              <td>Role / Designation</td>
              <td>:</td>
              <td>{{$myResume->designation}}</td>
            </tr>
            <tr>
              <td>Started in</td>
              <td>:</td>
              <td>{{date('M d, Y', strtotime($myResume->last_join))}}</td>
            </tr>
            <tr>
              <td>Notice period (in days)</td>
              <td>:</td>
              <td>{{$myResume->notice_period}}</td>
            </tr>
            <tr>
              <td>About the current role</td>
              <td>:</td>
              <td>{{$myResume->current_role}}</td>
            </tr>
            <tr>
              <td> Current salary (per month) (Tk)</td>
              <td>:</td>
              <td>{{$myResume->current_salary}}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-xs-12">
        <div class="clearfix" style="margin-bottom: 10px;">
          <h4>Attach Resume <a href="javascript:void(0)" class="pull-right btn btn-sm btn-default" style="padding-left: 20px; padding-right: 20px;">Edit</a> </h4>
        </div>
        @if(!empty($myResume->cv))
        <a href="{{asset('user-cv/'.$myResume->cv)}}" target="_blank">Download cv</a> 
        @else
        <p>Not filled</p>
        @endif
      </div>
    </div>
  </div>
</div>




</div>
</div>
</div>
</div>
</section>
</div>




@push('footer-asset')

<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
   //Date picker
   $('#datepicker').datepicker({
    autoclose: true,
    startDate:'05/03/1918',
    endDate:'05/03/2004',
    format:'dd/mm/yyyy'
  });
 });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var text_max = 2000;
    $('#textarea_about_yourself').html(text_max + ' characters remaining');

    $('#about_yourself').keyup(function() {
      var text_length = $('#about_yourself').val().length;
      var text_remaining = text_max - text_length;

      $('#textarea_about_yourself').html(text_remaining + ' characters remaining');
    });
  });

</script>
<script type="text/javascript">
  $(document).ready(function() {
    var text_max = 2000;
    $('#textarea_about_role').html(text_max + ' characters remaining');

    $('#about_role').keyup(function() {
      var text_length = $('#about_role').val().length;
      var text_remaining = text_max - text_length;

      $('#textarea_about_role').html(text_remaining + ' characters remaining');
    });
  });

</script>

@endpush
@endsection

