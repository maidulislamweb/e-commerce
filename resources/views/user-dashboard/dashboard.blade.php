@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
    <section>
        <div class="container">
            <div class="row" style="margin-top: 25px;">
                <div class="col-xs-12">
                    @if(Session::has('info_message'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Warning! </strong> {{ Session::get('info_message') }}.
                    </div>
                    @endif

                    @if(Session::has('success_message'))

                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success! </strong> {{ Session::get('success_message') }}.
                    </div>
                    @endif
                </div>
            </div>
            <div class="well bg-f" style="padding-bottom: 50px;">
                
                <div class="row">
                    <div class="col-md-3">
                     @include('user-dashboard._dashboard_left') 
                 </div>
                 <div class="col-md-9">
                    <div class="dash-n-h">
                       <h3>{{Auth::user()->name}}</h3>
                   </div>

                   @if(!empty($ads) && (count($ads)>0))
                   <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>SL</th>
                            <th>Category</th>
                            <th>Title</th>
                            <th>Price (Tk)</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($ads as $key => $ad)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$ad->categoryName}}</td>
                            <td>{{$ad->title}}</td>
                            <td>{{$ad->price}}</td>
                            <td>
                                @if($ad->status == 5)
                                <span class="text-danger">Pending</span>
                                @endif
                                @if($ad->status == 1)
                                <span class="text-primary">Published</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{url('/ad-view/'.$ad->id)}}" title="View"><i class="fa fa-eye"></i></a>
                                <a href="{{url('/ad-edit/'.$ad->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('/ad-delete/'.$ad->id)}}" title="Delete" class="text-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach 


                    </tbody>
                </table>

            </div>
            @else
            <div class="dash-n-b">
                <p>You don't have any ads yet.</p>
                <a href="{{url('/post-ad')}}" class="btn btn-success">Post your ad now!</a>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
</section>

</div>
@endsection

