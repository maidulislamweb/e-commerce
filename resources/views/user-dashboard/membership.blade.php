@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
    <section>
        <div class="container">
        <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
            <div class="row">
                <div class="col-md-3">

                @include('user-dashboard._dashboard_left') 

                </div>
                <div class="col-md-9">
                <div class="dash-n-h">
                     <h3>Membership</h3>
                </div>
                    <div>
                  <img src="{{asset('assets/img/home.png')}}" class="img-responsive"  style="display: inline;" />
                  <span style="font-size: 20px; color: #000;">Become a bikroy member</span>
                  <p>Memberships give your business a voice and presence on bikroy, to reach more customers, increase your sales and expand your business! Memberships unlock powerful tools like sales analytics, a dedicated business page and discounted ad promotions.</p>
                  <a href="{{url('/membership-detail')}}" class="btn btn-success btn1">Learn more</a>
              </div>
                   

                </div>
            </div>
            </div>
        </div>
    </section>
   
</div>
@endsection

