@extends('layouts.app')
@push('header-asset')

<link href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

@endpush

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
        <div class="row">
          <div class="col-md-3">
           @include('user-dashboard._dashboard_left')
         </div>
         <div class="col-md-9">
          <div class="row">
            <div class="col-xs-12">
              @if(Session::has('info_message'))
              <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning! </strong> {{ Session::get('info_message') }}.
              </div>
              @endif

              @if(Session::has('success_message'))

              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success! </strong> {{ Session::get('success_message') }}.
              </div>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">

              <div class="dash-n-h">
               <h3>My resume</h3>
             </div>
             <div>
               <p style="padding-top: 15px;">Let's start with adding some details about yourself.</p>

               <ul class="ui-steps has-4 profile-steps is-short is-mt-12">
                <li class="is-past"><div class="steps-title"><span class="number"><a href="{{url('/cerate-resume')}}">1</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li class="is-current"><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-2')}}">2</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-3')}}">3</a></span><span class="label"></span></div><div class="steps-content"></div></li>
                <li><div class="steps-title"><span class="number"><a href="{{url('/create-resume-step-4')}}">4</a></span><span class="label"></span></div><div class="steps-content"></div></li>
              </ul>

              <h4>Education</h4>
              <hr>
            </div>
            <div>
             <div>
              <form action="{{url('/add-resume-step-two')}}" autocomplete="off" method="post" >
                {{csrf_field()}}


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Highest education level :</label>
                      <select  name="highest_edu" class="form-control">
                        <option value="">Highest education level  </option>
                        <option value="primary_school" {{ $info->highest_edu == 'primary_school' ? 'selected' : '' }}>Primary School</option>
                        <option value="high_school"  {{ $info->highest_edu == 'high_school' ? 'selected' : '' }}>High School</option>
                        <option value="ssc" {{ $info->highest_edu == 'ssc' ? 'selected' : '' }}>SSC / O Level</option>
                        <option value="hsc" {{ $info->highest_edu == 'hsc' ? 'selected' : '' }}>HSC / A Level</option>
                        <option value="diploma" {{ $info->highest_edu == 'diploma' ? 'selected' : '' }}>Diploma</option>
                        <option value="graduate" {{ $info->highest_edu == 'graduate' ? 'selected' : '' }}>Bachelor / Honors</option>
                        <option value="post_graduate" {{ $info->highest_edu == 'post_graduate' ? 'selected' : '' }}>Masters</option>
                        <option value="doctorate" {{ $info->highest_edu == 'doctorate' ? 'selected' : '' }}>PhD / Doctorate</option>
                      </select>
                       @if($errors->has('highest_edu'))
                  <p class="text-danger">must be required</p>
                  @endif

                    </div>
                  </div>
                  <div class="col-md-6">
                   <div class="form-group">
                     <label for="">Educational specialization (optional):</label>
                     <select name="special_edu" class="form-control">
                      <option value="">Educational specialization (optional) </option>
                      <option value="art_humanities"{{ $info->special_edu == 'art_humanities' ? 'selected' : '' }}>Art Humanities</option>
                      <option value="business_management"{{ $info->special_edu == 'business_management' ? 'selected' : '' }}>Business Management</option>
                      <option value="accounting"{{ $info->special_edu == 'accounting' ? 'selected' : '' }}>Accounting</option>
                      <option value="design_fashion"{{ $info->special_edu == 'design_fashion' ? 'selected' : '' }}>Design Fashion</option>
                      <option value="engineering"{{ $info->special_edu == 'engineering' ? 'selected' : '' }}>Engineering</option>
                      <option value="events_hospitality"{{ $info->special_edu == 'events_hospitality' ? 'selected' : '' }}>Events Hospitality</option>
                      <option value="finance_commerce"{{ $info->special_edu == 'finance_commerce' ? 'selected' : '' }}>Finance Commerce</option>
                      <option value="human_resources"{{ $info->special_edu == 'human_resources' ? 'selected' : '' }}>Human Resources</option>
                      <option value="info_technology"{{ $info->special_edu == 'info_technology' ? 'selected' : '' }}>Information Technology</option>
                      <option value="law"{{ $info->special_edu == 'law' ? 'selected' : '' }}>Law</option>
                      <option value="marketing_sales"{{ $info->special_edu == 'marketing_sales' ? 'selected' : '' }}>Marketing Sales</option>
                      <option value="mass_comm"{{ $info->special_edu == 'mass_comm' ? 'selected' : '' }}>Mass Communication</option>
                      <option value="medicine"{{ $info->special_edu == 'medicine' ? 'selected' : '' }}>Medicine</option>
                      <option value="sciences"{{ $info->special_edu == 'sciences' ? 'selected' : '' }}>Sciences</option>
                      <option value="vocational"{{ $info->special_edu == 'vocational' ? 'selected' : '' }}>Vocational Technical</option>
                      <option value="others"{{ $info->special_edu == 'others' ? 'selected' : '' }}>Others</option>
                    </select>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="institute">Institute / University :</label>
                    <input type="text" class="form-control" id="institute" value="{{$info->institute}}" name="institute">
                     @if($errors->has('institute'))
                  <p class="text-danger">must be required</p>
                  @endif
                  </div>
                </div>
                <div class="col-md-6">

                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                 <button type="submit" class="btn btn-success btn1 btn-lg btn-block" style="margin-top: 20px;">Continue</button>
               </div>
               <div class="col-md-6">

               </div>
             </div>


           </form>

         </div>

       </div>


     </div>
   </div>
 </div>
</div>
</div>
</div>
</section>
</div>

<!-- Modal for updating photo -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose a photo for resume</h4>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data"  action="{{url('/update-profile-photo')}}">
          {{csrf_field()}}
          <div class="form-group">
            <input type="file" class="form-control" id="profile-photo" name="profile_photo" />
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@push('footer-asset')

<script src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
   //Date picker
   $('#datepicker').datepicker({
    autoclose: true,
    startDate:'05/03/1918',
    endDate:'05/03/2004',
    format:'dd/mm/yyyy'
  });
 });
</script>

@endpush
@endsection

