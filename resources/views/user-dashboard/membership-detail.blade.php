@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
	<section>
		<div class="container">
			<div class="row" style="margin-top: 25px;">
				<div class="col-xs-12">
					<div class="membership-bg ui-panel-content ui-panel-block" style="padding: 5% 15%;">
						<div class="row">
							<div class="col-md-6">
								<h2 style="line-height: 40px;">Increase your sales with a Bikroy.com Membership!</h2>
								<br/>
								<p style="line-height: 30px;">Membership allows your business to have a bigger presence on Bikroy.com, so that you can reach even more customers. Our Membership packages are specifically designed to give you the tools you need to expand your business and increase your sales through Bikroy.com.</p>
								<a href="{{url('/membership-package')}}" target="_blank">Set up your Membership today!</a>
								
							</div>
							<div class="col-md-6 col-sm-hidden col-xs-hidden">
								<img src="{{asset('assets/img/badge-lg-85f38cd9.png')}}" class="img-responsive" />
							</div>
						</div>

					</div>
					

				</div>
			</div>
		</div>
	</section>
</div>
@endsection