@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE; padding-top: 20px;">

	<section style="padding-bottom: 10px;">
		<div class="container">
			<div class="well bg-f" style="margin-bottom: 10px; padding: 20px;">
				<a href="{{url('/dashboard')}}"><i class="fa fa-arrow-left" style="font-size: 24px;"></i></a>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">

						<div class="row">
							<div class="col-xs-12">
								@if(Session::has('info_message'))
								<div class="alert alert-warning">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Warning! </strong> {{ Session::get('info_message') }}.
								</div>
								@endif

								@if(Session::has('success_message'))

								<div class="alert alert-success">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success! </strong> {{ Session::get('success_message') }}.
								</div>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h3 class="heading">Sell an item or service</h3>
								<hr>
								<div class="row">
									<div class="col-md-12">
										<div class="well">
											<form action="{{url('ad-edit-category/'.$ad->id)}}" method="post" class="form-inline" />
												{{csrf_field()}}

												<div class="form-group">
													<label for="">Category :</label>
													<select class="form-control" id="category" name="category" style="min-width: 220px;">
														@if(!empty($categories))
														@foreach($categories as $key => $category)
														<option value="{{$category->id}}" {{ $category->id == $categoryInfo->id ? "selected":""}}>{{$category->name}}</option>
														@endforeach
														@endif
													</select>
												</div>

												<div class="form-group">
													<select class="form-control" id="subCategory" name="subcategory" style="min-width: 220px;">
														@if(!empty($subCategoryInfo))

														<option value="{{$subCategoryInfo->id}}">{{$subCategoryInfo->name}}</option>

														@endif
													</select>
												</div>
												<button type="submit" class="btn btn-primary">update</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>

     

						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">
										<div class="well">
											<form action="{{url('ad-edit-location/'.$ad->id)}}" method="post" class="form-inline" />
												{{csrf_field()}}
												<div class="form-group">
													<label for="location">Location : </label>
													<select class="form-control" id="location" name="location" style="min-width: 220px;">
														@if(!empty($cities))
														<optgroup label="City">
															@foreach($cities as $key => $city)
															<option value="{{$city->id}}" @if(isset($location)) {{ $city->id == $location->id ? "selected":""}} @endif >{{$city->name}}</option>
															@endforeach
														</optgroup>
														@endif
														@if(!empty($divisions))
														<optgroup label="Division">
															@foreach($divisions as $key => $division)
															<option value="{{$division->id}}"  @if(isset($location)) {{ $division->id == $location->id ? "selected":""}} @endif >{{$division->name}} Division</option>
															@endforeach
														</optgroup>
														@endif
													</select>
													@if($errors->has('location'))
													<p class="text-danger">must be required</p>
													@endif
												</div>
												<div class="form-group">
													<label for="area"> Area : </label>
													<select class="form-control" id="locationArea" name="area" style="min-width: 220px;">
														@if(!empty($areas))
														@foreach($areas as $area)
														<option value="{{$area->id}}" {{ $ad->location_id == $area->id ? 'selected' : ''}}>{{$area->name}}</option>
														@endforeach
														@endif
													</select>
													@if($errors->has('area'))
													<p class="text-danger">must be required</p>
													@endif
												</div>
												<button type="submit" class="btn btn-primary">update</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>

						<form method="POST" enctype="multipart/form-data"  action="{{url('update-photo/'.$ad->id)}}">
							{{csrf_field()}}

							<div class="well">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<h4 class="heading">Add photos (5 for free)</h4>
											<table class="table appended_tr">
												<tbody>
													@if(!empty($photos[0]))
													
													@foreach($photos as $photo)
													<tr>

														<td>
															<div class="fileupload fileupload-exists" data-provides="fileupload" >
																<span class="fileupload-preview fileupload-exists thumbnail" style="max-width: 120px; max-height: 80px;">

																	@if(!empty($photo->photo))

																	<img src="{{asset('../uploads/'.$photo->photo)}}" alt="Article Photo" class="img-responsive img_counter" width="75px" height="50px"/>
																	@endif
																</span>

															</div>
														</td>

														<td>
															<div class="pull-right">
																<a href="{{url('/remove-photo/'.$photo->id)}}" onclick="return confirm('Are you sure you want to delete this item?');" title="Remove photo">
																	<i class="fa fa-trash-o fa-2x"></i>
																</a>
															</div>

														</td>
													</tr>

													@endforeach
													@else
													<tr>

														<td>
															<div class="fileupload fileupload-new" data-provides="fileupload" >
																<span class="fileupload-preview fileupload-exists thumbnail" style="max-width: 75px; max-height: 50px"></span>
																<span>
																	<span class="btn btn-primary btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select Image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
																	<input type="file" id="image" name="image[]">
																</span>
																<a href="#" class="btn fileupload-exists btn-default btn-sm" data-dismiss="fileupload">
																	<i class="fa fa-times"></i> Remove
																</a>
															</span>
														</div>

													</td>
													
													<td style="visibility: hidden"><i class="fa fa-trash-o fa-2x fileupload-exists" data-dismiss="fileupload"></i></td>
												</tr>
												@endif
											</tbody>
										</table>
										<button type="button" class="add_more_photo btn btn-default btn-sm btn-rounded pull-right">Add More</button>
										<button type="submit" class="btn btn-primary">update</button>


										@if($errors->has('image'))
										<p class="text-danger">one photo must be required</p>
										@endif
									</div>
								</div>
							</div>
						</div>

					</form>

					<form method="POST" enctype="multipart/form-data"  action="{{url('/ad-update/'.$ad->id)}}">
						{{csrf_field()}}


						<div class="row">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 20px;">
									<p style="color: #139375;">Condition</p>
									<label class="radio-inline"><input type="radio" name="condition" @if($ad->condition == 'used') checked="checked" @endif value="used" />Used</label>
									<label class="radio-inline"><input type="radio" name="condition" @if($ad->condition == 'new') checked="checked" @endif value="new" />New</label>
									@if($errors->has('condition'))
									<p class="text-danger">must be required</p>
									@endif
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<p>{{$ad->brand_id}}</p>
								@if(!empty($brands) && (count($brands)>0))
								<div class="form-group" style="margin-bottom: 20px;">
									<p style="color: #139375;">Brand</p>
									<select class="form-control" name="brand">
										<option value="">Brand  </option>
										@foreach($brands as $brand)
										<option value="{{$brand->id}}" {{ $brand->id == $ad->brand_id ? 'selected' : ''}}>{{$brand->title}}</option>
										@endforeach
									</select>

								</div>
								@endif
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								@if(!empty($types) && (count($types)>0))
								<div class="form-group" style="margin-bottom: 20px;">
									<p style="color: #139375;">Type</p>
									<select class="form-control" name="type">
										<option value="">type  </option>
										@foreach($types as $type)
										<option value="{{$type->id}}}">{{$type->title}}</option>
										@endforeach

									</select>
								</div>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								@if(!empty($features) && (count($features)>0))
								<div class="form-group" style="margin-bottom: 30px;">
									<p style="color: #139375;">Features (optional)</p>
									@foreach($features as $feature)
									<label class="checkbox-inline"><input type="checkbox" value="{{$feature->id}}" name="feature[]">{{$feature->title}}</label>
									@endforeach

								</div>
								@endif
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="box-header">
										<h3 class="box-title">Description</h3>
										<!-- tools box -->
										<div class="pull-right box-tools">
											<button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
											title="Collapse">
											<i class="fa fa-minus"></i></button>
											<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip"
											title="Remove">
											<i class="fa fa-times"></i></button>
										</div>
										<!-- /. tools -->
									</div>
									<!-- /.box-header -->
									<div class="box-body pad">
										<!-- <form> -->
											<textarea class="wysihtml5" placeholder="Place some text here"
											style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="description">{{$ad->description}}</textarea>
											@if($errors->has('description'))
											<p class="text-danger">must be required</p>
											@endif
											<!-- </form> -->
										</div>
									</div>

								</div>
							</div>

							@if($categoryInfo->id == 1 || $categoryInfo->id == 2)
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Model</p>
										<input type="text" name="model" class="form-control" value="{{$ad->model}}" />
									</div>

								</div>
							</div>
							@endif
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Title</p>
										<input type="text" name="title" class="form-control" value="{{$ad->title}}" />
									</div>

								</div>
							</div>
							@if($categoryInfo->id == 2)
							<!--2=car&vehicles,-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Model Year</p>
										<input type="number" name="model_year" class="form-control" value="{{$ad->model_year}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Registration Year</p>
										<input type="number" name="regi_year" class="form-control" value="{{$ad->regi_year}}" />
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group" style="margin-bottom: 30px;">
										<p style="color: #139375;">Fuel Type</p>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="diesel">Diesel</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="cng">CNG</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="petrol">Petrol</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="octane">Octane</label>
										<label class="checkbox-inline"><input type="checkbox" name="fuel_type[]" value="other">Other fuel type</label>

									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Engine Capacity(cc)</p>
										<input type="number" name="capacity" class="form-control" value="{{$ad->capacity}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Kilometers Run(km)</p>
										<input type="number" name="km" class="form-control" value="{{$ad->km}}" />
									</div>

								</div>
							</div>

							@endif
							@if($subCategoryInfo->id == 32 || $subCategoryInfo->id == 34)
							<!--Apartments & Flats = 32, Houses =34-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Beds</p>
										<input type="number" name="bed" class="form-control" value="{{$ad->bed}}" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Baths</p>
										<input type="number" name="bath" class="form-control" value="{{$ad->bath}}" />
									</div>
								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 34)
							<!--Houses =34-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="border: 1px solid #e8b8b8; margin-left: 0px; padding-top: 15px; margin-right: 0px; padding-bottom: 15px;">
											<div class="col-md-6">
												<p style="color: #139375;">Land Size</p>
												<input type="number" name="land_size" class="form-control" />
											</div>
											<div class="col-md-6">
												<p style="color: #139375;">Unit</p>
												<select name="size_unit" class="form-control">
													<option value="">unit</option>
													<option value="katha">katha</option>
													<option value="bigha">Bigha</option>
													<option value="acre">Acre</option>
													<option value="decimal">Decimal</option>
													<option value="shotok">Shotok</option>

												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 32)
							<!--Apartments & Flats = 32, -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Size(sqft)</p>
										<input type="number" name="size" class="form-control" value="{{$ad->size}}" />
									</div>

								</div>
							</div>
							@endif

							@if($subCategoryInfo->id == 48 || $subCategoryInfo->id == 49)
							<!--clothing = 48, shoes & footwear =49-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Gender</p>
										<label class="radio-inline"><input type="radio" name="gender" checked="checked" value="men">Men</label>
										<label class="radio-inline"><input type="radio" name="gender" value="women">Women</label>
										<label class="radio-inline"><input type="radio" name="gender" value="unisex">Unisex</label>
									</div>
								</div>
							</div>
							@endif

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Authenticity</p>
										<label class="radio-inline"><input type="radio" name="authenticity" @if($ad->authenticity == 'original') checked="checked" @endif value="original">Original</label>
										<label class="radio-inline"><input type="radio" name="authenticity" @if($ad->authenticity == 'clone') checked="checked" @endif value="clone">Replica / Clone / Other</label>
										@if($errors->has('authenticity'))
										<p class="text-danger">must be required</p>
										@endif
									</div>

								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Price (Tk)</p>
										<input type="number" name="price" class="form-control" value="{{$ad->price}}" />
										<label class="checkbox-inline"><input type="checkbox" value="negotiable" name="price_type" @if($ad->price_type == 'negotiable') checked="checked" @endif>Negotiable</label>
										@if($errors->has('price'))
										<p class="text-danger">must be required</p>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Address</p>
										<input type="text" name="address" class="form-control" value="{{$ad->address}}" />
										@if($errors->has('address'))
										<p class="text-danger">must be required</p>
										@endif
									</div>

								</div>
							</div>
							<div class="row" style="border: 1px solid rgb(232, 184, 184); margin-left: 0px; padding-top: 15px; margin-right: 0px; padding-bottom: 15px;">
								<div class="col-md-12">
									<div class="form-group">
										<p style="color: #139375;">Contact details</p>
										<hr style="margin-top: 0px;">
										<p>Name: <span>{{Auth::user()->name}}</span></p>

										<p>Phone number</p>
										<input type="text" name="phone_no" class="form-control" placeholder="Your phone number" value="{{$ad->phone_no}}" />
										<label class="checkbox-inline"><input type="checkbox" value="hide" name="phone_no_type" @if($ad->phone_no_type == 'hide') checked="checked" @endif>Hide phone number</label>
										@if($errors->has('phone_no'))
										<p class="text-danger">must be required</p>
										@endif
										<br>
										<br>
										<p>Email: <span>{{Auth::user()->email}}</span></p>
									</div>

								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<button type="submit" class="btn btn1">Post ad</button>
										<a href="{{url('/dashboard')}}" class="btn btn-default">Cencel</a>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<section style="background-color: #E7EDEE; ">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>By posting this ad, you agree to the Terms & Conditions of this site.</p>
			</div>
		</div>

		<div class="well bg-f" style="margin-bottom: 10px;">
			<div class="row" style="">
				<div class="col-md-12">
					<h3>Quick rules</h3>
					<h4>All ads posted on Bikroy.com must follow our rules:</h4>
				</div>
			</div>
			<div class="row" style="">
				<div class="col-md-6">
					<ul class="ul-style">
						<li>Make sure you post in the correct category.</li>
						<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
						<li>Do not upload pictures with watermarks.</li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="ul-style">
						<li>Do not post ads containing multiple items unless it's a package deal.</li>
						<li>Do not put your email or phone numbers in the title or description.</li>
					</ul>
				</div>
			</div>
			<div class="row" style="">
				<div class="col-md-12">
					<a href="" class="h-off pull-right" >Click here to see all of our posting rules <i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>

@push('footer-asset')
<script>
	$(document).ready(function() {
		var imgCounter = $('.img_counter').size()
		var restImg = (6-imgCounter);
		var max_fields      = restImg; 
		var wrapper         = $(".appended_tr"); 
		var add_button      = $(".add_more_photo"); 
		var x = 1; 
		$(add_button).click(function(e){ 
			e.preventDefault();
			if(x < max_fields){ 
				x++; 
				$(wrapper).append('<tr><td><div class="fileupload fileupload-new" data-provides="fileupload"><span class="fileupload-preview fileupload-exists thumbnail" style="max-width: 75px; max-height: 75px;"></span> <label class="btn btn-primary btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span><input type="file" name="image[]" required="required"></label> <a href="#" class="btn fileupload-exists btn-default btn-sm" data-dismiss="fileupload"> <i class="fa fa-times"></i> Remove</a></span></div></td></td><td style="width:50px"><i class="fa fa-trash-o fa-2x text-dark remove_field" title="remove the field"></i></td></tr>');

				$('.more_photo_update').show();
			}
			else{
				alert('You can not upload more then 5 photos !');
			}
		});

		$(wrapper).on("click",".remove_field", function(e){ 
			e.preventDefault(); $(this).closest('tr').remove(); x--;
		})
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var text_max = 2000;
		$('#textarea_feedback').html(text_max + ' characters remaining');

		$('#textarea').keyup(function() {
			var text_length = $('#textarea').val().length;
			var text_remaining = text_max - text_length;

			$('#textarea_feedback').html(text_remaining + ' characters remaining');
		});
	});
</script>


<script type="text/javascript">
	$('body').on('change', '#location', function() {
		var locationId = $(this).val();
		// alert(locationId);
		$('#locationArea').empty();
		$.ajax({
			type: 'GET',
			url: '{{URL("ajax/area/")}}'+"/"+locationId,
			success: function (data) {
				if(data.areas != null){
					$.each(data.areas, function() {
						$('#locationArea').append( $("<option ></option>").text(this.name).val(this.id) );
					});
				}
			}
		});

	});
</script>

<script type="text/javascript">
	$('body').on('change', '#category', function() {
		var categoryId = $(this).val();
		$('#subCategory').empty();
		$.ajax({
			type: 'GET',
			url: '{{URL("ajax/category/")}}'+"/"+categoryId,
			success: function (data) {
				// console.log(data);
				if(data.subcategories != null){
					$.each(data.subcategories, function() {
						$('#subCategory').append( $("<option ></option>").text(this.name).val(this.id) );
					});
				}
			}
		});

	});
</script>



@endpush

@endsection

