@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
  <section>
    <div class="container">
      <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
        <div class="row">
          <div class="col-md-3">
           @include('user-dashboard._dashboard_left')
         </div>
         <div class="col-md-9">
          <div class="dash-n-h">
           <h3>Settings</h3>
         </div>
         <div class="row">
          <div class="col-xs-12">
            @if(Session::has('info_message'))
            <div class="alert alert-warning">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Warning! </strong> {{ Session::get('info_message') }}.
            </div>
            @endif

            @if(Session::has('success_message'))

            <div class="alert alert-success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Success! </strong> {{ Session::get('success_message') }}.
            </div>
            @endif
            @if($errors->any())
            @foreach($errors->all() as $error)
            <p style='padding:15px;' class='bg-danger'>{{ $error }}</p>
            @endforeach
            @endif
            @if(Request::get('errorMessage') !== null)
            <p style='padding:15px;' class='bg-danger'>{{ Request::get('errorMessage') }}</p>
            @endif
          </div>
        </div>
        <div class="dash-n-b" style="text-align: left;">

          <div>
            <div>
             <h3 style="color: #000; margin-bottom: 20px;">{{Auth::user()->name}}</h3>
             <p style="color: #008F75;">Email: <span style="color: #707676;">{{Auth::user()->email}}</span></p>
             <p style="color: #008F75;">Phone No: <span style="color: #707676;">01681944126</span></p>




           </div>
           <br>
           <div>
            <h3 style="color: #000; margin-bottom: 20px;">Change password</h3>

            <form class="form" action="{{url('/change-password')}}" method="post">
              {{csrf_field()}}
              <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="c_pass">Current Password</label>
                  <input type="password" class="form-control" id="c_pass" name="c_pass">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
               <div class="form-group">
                <label for="new_pass">New Password</label>
                <input type="password" class="form-control" id="new_pass" name="new_pass">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
             <div class="form-group">
              <label for="confirm_pass">Confirm Password</label>
              <input type="password" class="form-control" id="confirm_pass" name="confirm_pass">
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-default btn-lg btn-profile">Change Password</button>
      </form>
    </div>
    <hr>
    <div>
     

      <a href="{{ route('logout') }}"
      onclick="event.preventDefault();
      document.getElementById('logout-form').submit();" class="btn btn-smart btn-lg">
      Logout
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
    </form>
    
  </div>

</div>
</div>


</div>
</div>
</div>
</div>
</section>

</div>
@endsection

