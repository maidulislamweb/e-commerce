<ul class="list-unstyled dash-menu">
	<li><a href="{{url('/dashboard')}}" class="@if(isset($dashboard)) {{$dashboard}} @endif">My Account <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
	<li><a href="{{url('/membership')}}" class="@if(isset($membership)) {{$membership}} @endif">My membership <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
	<li><a href="{{url('/resume')}}" class="@if(isset($resume)) {{$resume}} @endif">My Resume <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
	<li><a href="{{url('/favorite-ads')}}" class="@if(isset($favoriteAds)) {{$favoriteAds}} @endif">Favorites <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
	<li><a href="{{url('/settings')}}"  class="@if(isset($settings)) {{$settings}} @endif">Settings <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
</ul>