@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
    <section>
        <div class="container">
            <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
                <div class="row">
                    <div class="col-md-3">
                       @include('user-dashboard._dashboard_left')
                   </div>
                   <div class="col-md-9">
                    <div class="dash-n-h">
                       <h3>Favorites</h3>
                   </div>
                   <div class="dash-n-b">
                     <div class="row">
                     <div class="col-md-3">
                         
                     </div>
                         <div class="col-md-9" style="text-align: left;">
                             <p>You haven't marked any ads as favorite yet.</p>
                             <p>Click on the star symbol on any ad to save it as a favorite.</p>
                             <p>Start to <a href="">browse ads</a> to find ads you would like to favorite.</p>
                         </div>
                     </div>


                 </div>


             </div>
         </div>
     </div>
 </div>
</section>

</div>
@endsection

