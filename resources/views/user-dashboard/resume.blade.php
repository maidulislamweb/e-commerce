@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
    <section>
        <div class="container">
            <div class="well bg-f" style="margin-top: 25px; padding-bottom: 50px;">
                <div class="row">
                    <div class="col-md-3">
                       @include('user-dashboard._dashboard_left')
                   </div>
                   <div class="col-md-9">
                    <div class="dash-n-h">
                     <h3>Resume</h3>
                 </div>
                 <div>
                     <h4><strong>Welcome</strong> </h4>

                     <p>Create a free resume on bikroy to showcase your skills and get your dream job. Make sure to complete your resume in order to stand out from the crowd; employers like to see details!</p>
                 </div>
                 <div>
                    <h4><strong>Personal details</strong> </h4>
                    <div>
                        <div class="media">
                          <div class="media-left">
                            @if(Auth::user()->photo)
                            <img src="{{asset('profile-photo/'.Auth::user()->photo)}}" class="media-object" style="width:60px; border-radius: 50%;">
                            @else
                            <img src="{{asset('profile-photo/default.png')}}" class="media-object" style="width:60px; border-radius: 50%;">
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{Auth::user()->name}}</h4>
                            <p><i class="fa fa-envelope-open" aria-hidden="true"></i> {{Auth::user()->email}}</p>
                        </div>
                    </div>
                    <a href="{{url('/cerate-resume')}}" class="btn btn-success btn1 btn-lg" style="margin-left: 8%; padding-left: 50px; padding-right: 50px; margin-top: 30px;">Update Resume</a>
                    <a href="{{url('/my-resume')}}" class="btn btn-success btn1 btn-lg" style="margin-left: 8%; padding-left: 50px; padding-right: 50px; margin-top: 30px;">My Resume</a>
                </div>

            </div>


        </div>
    </div>
</div>
</div>
</section>

</div>
@endsection

