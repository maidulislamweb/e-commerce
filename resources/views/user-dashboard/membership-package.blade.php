@extends('layouts.app')

@section('content')
<div style="background-color: #E7EDEE;">
	<section>
		<div class="container">
			<div class="row" style="margin-top: 25px;">
				<div class="col-xs-12">
					<div class="membership-bg ui-panel-content ui-panel-block" style="padding: 5% 15%;">
						<div class="row">
							<div class="col-md-12">
								<h2 style="line-height: 40px;">Promote your ad and sell fast on Bikroy.com - the largest marketplace in Bangladesh</h2>
								<br/>
								<p style="line-height: 30px;">Membership allows your business to have a bigger presence on Bikroy.com, so that you can reach even more customers. Our Membership packages are specifically designed to give you the tools you need to expand your business and increase your sales through Bikroy.com.</p>

								
							</div>

						</div>
						<form method="post" action="{{url('/membership-package')}}">
							{{csrf_field()}}
							<div class="row">
								<div class="col-md-12">

									<label class="checkbox-inline"><input type="checkbox" value="supper-gold" name="supperGold" id="membershipSupperGold">Top Ad for 7 days</label>

									<p>
									At the top of every ad listing page, there are up to 2 spots reserved for Top Ads. By adding a Top Ad promotion to your ad, you earn the chance for your ad to be displayed in one of these Top Ad spots - which can get you up to 10 times or more views!</p>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12">

									<label class="checkbox-inline"><input type="checkbox" value="gold" name="gold" id="membershipGold">Golden Member for 1 year</label>

									<p>
									At the top of every ad listing page, there are up to 2 spots reserved for Top Ads. By adding a Top Ad promotion to your ad, you earn the chance for your ad to be displayed in one of these Top Ad spots - which can get you up to 10 times or more views!</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-info">submit</button>
								</div>
							</div>
						</form>
					</div>
					

				</div>
			</div>
		</div>
	</section>
</div>

@push('footer-asset')
<!--script>
	$(document).ready(function() {
		$("#membershipSupperGold").change(function(event){
    if (this.checked){
     
        alert(this.value);

    } else {

        alert(this.value+' unchecked');

    }
});
	})
</script-->
@endpush
@endsection