@extends('layouts.app')

@section('content')
<section style="background-color: #E7EDEE; margin-top: -21px; padding-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default" style="padding: 20px;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                             <div class="l-l">
                                 <h2>Log in to Bikroy</h2>
                                 <p>To view your ads and account details, please login to your Bikroy account.</p>

                                 <div class="media">
                                  <div class="media-left">
                                      <img src="{{asset('assets/logo/1-1x-a2fc1800.png')}}" class="media-object" style="width:60px">
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">Start posting your own ads.</h4>
                                    
                                </div>
                            </div>
                            <div class="media">
                              <div class="media-left">
                                  <img src="{{asset('assets/logo/2-1x-3efcbe32.png')}}" class="media-object" style="width:60px">
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Mark ads as favorite and view them later.</h4>
                                
                            </div>
                        </div>
                        <div class="media">
                          <div class="media-left">
                              <img src="{{asset('assets/logo/3-1x-b04d6b82.png')}}" class="media-object" style="width:60px">
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">View and manage your ads at your convenience.</h4>
                            
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6" style="border-left: 1px solid #d4ded9;">

             <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <div class="col-md-8 col-md-offset-2">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Your Email">

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                    <div class="col-md-8 col-md-offset-2">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <button type="submit" class="btn btn-primary btn-block">
                            Login
                        </button>
                    </div>
                </div>
            </form>


            <div class="col-md-12 text-center">


                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </div>

            <div class="col-md-12 text-center">
               <hr style="padding: 10px;">
               <p style="padding-bottom: 10px;"> Don't have an account yet?</p>
               <a href="" class="btn btn-lg btn-default">Sign Up</a>
           </div>
       </div>
   </div>





</div>
</div>
</div>
</div>
</div>
</section>
@endsection
