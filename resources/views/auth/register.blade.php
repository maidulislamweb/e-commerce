@extends('layouts.app')

@section('content')
<section style="background-color: #E7EDEE; margin-top: -21px; padding-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="panel panel-default" style="width: 70%; margin-left: 15%;">
               <div class="panel-body">
                <div class="col-md-6">
                   <div class="res-form" style="width: 80%; margin-left: 10%;" >
                       <h2>Sign up on Bikroy</h2>
                       <p>The largest marketplace in Bangladesh.</p>
                   </div>
               </div>
               <div class="col-md-6">

                 <div class="res-form" style="width: 80%; margin-left: 10%;" >
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block" style="background-color: #139375; border-color: #139375; text-shadow: 0 1px 0 #007168; font-size: 18px;">
                                    Sign up
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="text-center">
                        <p>Already have an account?</p>
                        <a href="{{ route('login') }}" class="btn btn-default btn-lg" style="padding: 8px 30px;">Log in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection
