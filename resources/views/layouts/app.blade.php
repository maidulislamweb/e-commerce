<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="{{ asset('assets/js/jquery.min.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/lightslider/js/lightslider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/custom.js') }}" type="text/javascript"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/lightslider/css/lightslider.css') }}" rel="stylesheet">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <link href="{{ asset('assets/lightslider/css/lightslider.css') }}" rel="stylesheet">
    
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
   
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script>
  <script>
      $(function () {

        $('.wysihtml5').wysihtml5()
    })
</script>




@stack('header-asset')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #139375; margin-bottom: 0px;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{asset('assets/logo/header-logo.png')}}">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/ads') }}" class="t-f"> All ads</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="javascript:void(0)" class="t-f "><i class="fa fa-weixin" aria-hidden="true"></i></a></li>
                        <!-- Authentication Links -->
                        @guest
                        <li><a href="{{ route('login') }}" class="t-f btn-shadow"><i class="fa fa-user" aria-hidden="true"></i> Login</a></li>
                        <li><a href="{{ route('register') }}" class="btn btn-important1" style="margin-top: 5px; margin-bottom: 5px;">Post your ad</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn-shadow" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                My Account<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li><a href="{{url('/dashboard')}}">dashboard</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{url('/post-ad')}}" class="btn btn-important1" style="margin-top: 5px; margin-bottom: 5px;">Post your ad</a></li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>


    @yield('content')


    <section style="border-top: 4px solid #009877;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h4>Connect with us</h4>
                        <p><a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i> Like us on Facebook</a></p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="">
                        <h4>Learn More</h4>
                        <ul class="list-unstyled">
                            <li><a href="">How to sell fast</a></li>
                            <li><a href="">Buy Now on </a></li>
                            <li><a href="">Bikroy.com</a></li>
                            <li><a href="">Membership</a></li>
                            <li><a href="">Banner Advertising</a></li>
                            <li><a href="">Promote your ad</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <h4>Help & Support</h4>
                        <ul class="list-unstyled">
                            <li><a href="">FAQ</a></li>
                            <li><a href="">Stay safe on </a></li>
                            <li><a href="">Bikroy.com</a></li>
                            <li><a href="">Contact us</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                  <div>
                    <h4>Social</h4>
                    <ul class="list-unstyled">
                        <li><a href="">Blog</a></li>
                        <li><a href="">Facebook</a></li>
                        <li><a href="">Twitter</a></li>
                        <li><a href="">YouTube</a></li>
                        <li><a href="">Google+</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-2">
              <div>
                <h4>About us</h4>
                <ul class="list-unstyled">
                    <li><a href="">About us</a></li>
                    <li><a href="">Career</a></li>
                    <li><a href="">Terms & Conditions</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Sitemap</a></li>

                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="pull-left"><p>Copyright © Saltside Technologies</p></div>
        </div>
        <div class="col-md-6">
            <div class="pull-right"><img src="{{asset('assets/logo/footer-logo.png') }}"></div>
        </div>
    </div>
</div>
</section>
</div>

<input type="hidden" class="siteUrl" value="{{url('/')}}">
<!-- Scripts -->
<!-- <script src="{{ asset('js/app.js') }}"></script> -->


@stack('footer-asset')
</body>
</html>
