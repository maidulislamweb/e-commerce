@extends('layouts.app')

@section('content')

<section style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="adv text-center">

                    <a href="javascript:void(0)" target="_blank">
                        <img src="{{asset('assets/adv/ad.jpg')}}" class="img-responsive" width="100%">
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-top">
 <div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="site-info">
                <h2 class="c-b" style="font-size:25px; line-height: 1.5;">Welcome to Bikroy.com - the largest marketplace in Bangladesh!</h2>
                <p>Buy and sell everything from used cars to mobile phones and computers, or search for property, jobs and more in Bangladesh - for free!</p>

                <div style="margin-top: 40px;">
                    <h4><strong>Browse our top categories:</strong></h4>
                    <div class="panel panel-default" style="background-color: #FFF;">

                        <div class="panel-body">
                         <div class="row">
                             <div class="col-md-3">
                                 <div class="n-card">
                                     <img src="{{asset('assets/img/mobile.png')}}">
                                     <p><a href="{{url('/ads')}}" class="h-off">Electronics</a></p>
                                     <a href="{{url('/ads')}}" class="link-overlay"></a>
                                 </div>
                             </div>
                             <div class="col-md-3">
                                   <div class="n-card">
                                     <img src="{{asset('assets/img/home.png')}}" style="width: 45px;">
                                     <p><a href="{{url('/ads')}}" class="h-off">Property</a></p>
                                     <a href="{{url('/ads')}}" class="link-overlay"></a>

                                 </div>
                             </div>
                             <div class="col-md-3">
                                      <div class="n-card">
                                 <img src="{{asset('assets/img/home.png')}}" style="width: 45px;">
                                 <p><a href="{{url('/ads')}}" class="h-off">Cars & Vehicles</a></p>
                                 <a href="{{url('/ads')}}" class="link-overlay"></a>
                             </div>
                         </div>
                         <div class="col-md-3">
                                     <div class="n-card">
                             <img src="{{asset('assets/img/home.png')}}" style="width: 45px;">
                             <p><a href="{{url('/ads')}}" class="h-off">Jobs in Bangladesh</a></p>
                             <a href="{{url('/ads')}}" class="link-overlay"></a>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="panel-footer" style="background-color: #FFF; border-left: 1px solid #ccc;">
              <span><a href="">Bikroy Deals <span class="label label-danger">new!</span> </a></span>
              <span>Great deals delivered straight to your doorstep</span>
          </div>
      </div>
  </div>
</div>
</div>
<div class="col-md-4"></div>
<div class="col-md-2">
    <div class="pull-right">
        <div class="city-list">
            <h4>Cities</h4>
            <ul class="list-unstyled ul-style">
                @if(!empty($cities))
                @foreach($cities as $city)
                <li><a href="{{url('/ads/'.$city->title)}}">{{ $city->name }}</a></li>
                @endforeach
                @endif
                
            </ul>
        </div>
        <div class="division-list">
            <h4>Divisions</h4>
            <ul class="list-unstyled ul-style">
                
                @if(!empty($divisions))
                @foreach($divisions as $division)
                <li><a href="{{url('/ads/'.$division->title)}}">{{$division->name}} Division</a></li>
                @endforeach
                @endif

                
            </ul>
        </div>
    </div>
</div>
</div>
</div>
</section>

<section style="background-color: #E7EDEE; padding-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="well bg-f" style="padding: 10px;">
                   <div class="media">
                      <div class="media-left">
                        <img src="{{asset('assets/img/logo1.png')}}" class="media-object" style="width:40px" class="img-responsive" >
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Stay safe while trading!</h4>
                        <p>All ads are manually reviewed by our team for your safety. Read our tips on how to make safe deals</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well bg-f">
                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i> Like us on Facebook</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well bg-f">
                <div class="row">
                    @for($i=0; $i<12; $i++)

                    <div class="col-md-3">

                        <div class="text-center">
                            <span class=""><img src="{{asset('assets/img/mobile.png')}}"></span>
                            <h3><a href="" class="h-off">Electronics</a> </h3>
                            <p>(1020)</p>
                            <p><span>
                                Find great deals for used electronics in Bangladesh including mobile phones, computers, laptops, TVs, cameras and much more.
                            </span></p>
                        </div>

                    </div>
                    @endfor

                </div>
            </div>
        </div>
    </div>
</div>
</section>

@endsection

