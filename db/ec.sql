-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2018 at 07:42 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ec`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `title`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', 0, 'electronics', 1, 1, NULL, NULL),
(2, 'Cars & Vehicles', 0, 'vehicles', 2, 1, NULL, NULL),
(3, 'Property', 0, 'property', 3, 1, NULL, NULL),
(4, 'Services', 0, 'services', 4, 1, NULL, NULL),
(5, 'Home & Garden', 0, 'home', 5, 1, NULL, NULL),
(6, 'Clothing, Health & Beauty', 0, 'fashion', 7, 1, NULL, NULL),
(7, 'Hobby, Sport & Kids', 0, 'hobby', 8, 0, NULL, NULL),
(8, 'Business & Industry', 0, 'business', 9, 0, NULL, NULL),
(9, 'Education', 0, 'education', 10, 0, NULL, NULL),
(10, 'Pets & Animals', 0, 'animals', 11, 0, NULL, NULL),
(11, 'Food & Agriculture', 0, 'food', 12, 0, NULL, NULL),
(12, 'Other', 0, 'other', 13, 0, NULL, NULL),
(13, 'Mobile Phones', 1, 'mobile', 1, 1, NULL, NULL),
(14, 'Mobile Phone Accessories', 1, 'mobile-accessories', 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', 1, 1, NULL, NULL),
(2, 'Chittagong', 2, 1, NULL, NULL),
(3, 'Sylhet', 3, 1, NULL, NULL),
(4, 'Khulna', 4, 1, NULL, NULL),
(5, 'Barisal', 5, 1, NULL, NULL),
(6, 'Rajshahi', 6, 1, NULL, NULL),
(7, 'Rangpur', 7, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city_areas`
--

CREATE TABLE `city_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city_areas`
--

INSERT INTO `city_areas` (`id`, `name`, `city_id`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Badda', 1, 1, 1, NULL, NULL),
(2, 'Banani', 1, 2, 1, NULL, NULL),
(3, 'Banani DOHS', 1, 3, 1, NULL, NULL),
(4, 'Anderkilla', 2, 1, 1, NULL, NULL),
(5, 'Anwara', 2, 2, 1, NULL, NULL),
(6, 'Baizid', 2, 3, 1, NULL, NULL),
(7, 'Khalispur', 4, 4, 1, '2018-02-11 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', 1, 1, NULL, NULL),
(2, 'Chittagong', 2, 1, NULL, NULL),
(3, 'Sylhet', 3, 1, NULL, NULL),
(4, 'Khulna', 4, 1, NULL, NULL),
(5, 'Rajshahi ', 5, 1, NULL, NULL),
(6, 'Rangpur', 6, 1, NULL, NULL),
(7, 'Barisal', 7, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `division_areas`
--

CREATE TABLE `division_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `division_areas`
--

INSERT INTO `division_areas` (`id`, `name`, `division_id`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Faridpur', 1, 1, 0, NULL, '2018-02-11 18:00:00'),
(2, 'Gopalganj', 1, 2, 0, NULL, '2018-02-11 18:00:00'),
(3, 'Jamalpur', 1, 3, 1, NULL, NULL),
(4, 'Bandarban', 2, 1, 1, NULL, NULL),
(5, 'Cox\'s Bazar', 2, 2, 1, NULL, NULL),
(6, 'Khagrachhari', 2, 3, 1, NULL, NULL),
(8, 'Savar', 2, 5, 1, '2018-02-10 18:00:00', NULL),
(15, 'Savar', 1, 6, 1, '2018-02-10 18:00:00', NULL),
(16, 'Savar', 3, 7, 1, '2018-02-10 18:00:00', NULL),
(17, 'Madaripur', 1, 8, 1, '2018-02-11 18:00:00', NULL),
(18, 'Narangong', 1, 9, 1, '2018-02-11 18:00:00', NULL),
(19, 'Dohar', 1, 10, 1, '2018-02-11 18:00:00', NULL),
(20, 'Jamalpur', 2, 11, 1, '2018-02-11 18:00:00', NULL),
(21, 'Jamalpur', 3, 12, 1, '2018-02-11 18:00:00', NULL),
(22, 'Khalispur', 1, 13, 1, '2018-02-11 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_09_044313_create_categories_table', 2),
(4, '2018_02_09_113014_create_cities_table', 3),
(5, '2018_02_09_113314_create_city_areas_table', 3),
(6, '2018_02_09_113354_create_divisions_table', 3),
(7, '2018_02_09_113418_create_division_areas_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md Maidul Islam', 'admin@gmail.com', '$2y$10$V3rSWGUZXrlnRlrtLrNUWOCvif8B3TrypC4gz7Z9yNAcpXEhmOnnq', 'y5TVrHKHYGqxyTlNU1E86KIOWR5XejGdxaKYNpNfHvYGDuZnuohXaZmtCFv2', '2018-02-08 21:31:27', '2018-02-08 21:31:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_areas`
--
ALTER TABLE `city_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division_areas`
--
ALTER TABLE `division_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `city_areas`
--
ALTER TABLE `city_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `division_areas`
--
ALTER TABLE `division_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
