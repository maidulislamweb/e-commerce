<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use App\Category;
use App\City;
use App\Division;
use Session;
use Validator;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['dashboard'] = 'active-menu';

      $ads = DB::table('ads')->select('ads'.'.*', 'categories.name as categoryName')->leftJoin('categories', 'ads.category_id', '=', 'categories.id')->where('ads.status', '!=', '2')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
      $data['ads'] = $ads;
      // dd($ads);
      return view('user-dashboard.dashboard', $data);
    }
    public function membership(){
      $data['membership'] = 'active-menu';
      return view('user-dashboard.membership', $data);
    }


    public function resume(){
     $data['resume'] = 'active-menu';
     return view('user-dashboard.resume', $data); 
      // $resumeStatus = DB::table('resumes')->where('user_id', Auth::user()->id)->first();
      // if(!empty($resumeStatus)){
      //   if($resumeStatus->status == 5){
      //     return redirect()->to('resume-complete');
      //   }

      //   if($resumeStatus->status == 2){
      //     return redirect()->to('create-resume-step-2');
      //   }

      //   if($resumeStatus->status == 3){
      //     return redirect()->to('create-resume-step-3');
      //   }
      //   if($resumeStatus->status == 4){
      //     return redirect()->to('create-resume-step-4');
      //   }

      // }else{
      //   $data['resume'] = 'active-menu';
      //   return view('user-dashboard.resume', $data); 
      // }
   }







   public function favoriteAds(){
    $data['favoriteAds'] = 'active-menu';
    return view('user-dashboard.favorite-ads', $data);
  }
  public function settings(){
    $data['settings'] = 'active-menu';
    return view('user-dashboard.settings', $data);
  }
  public function changePassword(Request $request){

    $c_pass = $request->c_pass;
    $new_pass = $request->new_pass;
    $confirm_pass = $request->confirm_pass;
    $oldPass = Auth::user()->password;


    $validatedData = $request->validate([
      'c_pass' => 'required|min:6',
      'new_pass' => 'required|string|min:6',
      'confirm_pass' => 'required|same:new_pass',
    ],[
      'c_pass.required' => 'Old password is required',
      'c_pass.min' => 'Old password needs to have at least 6 characters',
      'new_pass.required' => 'Password is required',
      'new_pass.min' => 'Password needs to have at least 6 characters',
      'confirm_pass.required' => 'Passwords do not match'
    ]);

    $current_password = \Auth::User()->password;           
    if(\Hash::check($request->input('c_pass'), $current_password))
    {          
      $user_id = \Auth::User()->id;                       
      $obj_user = User::find($user_id);
      $obj_user->password = \Hash::make($request->input('new_pass'));
      $obj_user->save(); 
      Session::flash('success_message', 'Your Password updated successfully');
      return redirect()->back();
    }
    else
    { 
      $data['errorMessage'] = 'Please enter correct current password';
      return redirect()->route('settings', $data);
      // return redirect('/settings');
    }  

 
  }

  public function adDelete($adId){

    $ad = DB::table('ads')->where('id', $adId)->update(['status'=>'2']);
    return Redirect::back();
      // dd($adId);

  }

  public function adView($adId){
    $ad = DB::table('ads')->where('id', $adId)->first();

    if(!empty($ad->city_area)){
      $area = DB::table('city_areas')->where('id', $ad->city_area)->first();
      $city = DB::table('cities')->where('id', $area->city_id)->first();

      $data['area'] = $area;
      $data['location'] = $city;
    }
    if(!empty($ad->division_area)){
      $area = DB::table('division_areas')->where('id', $ad->division_area)->first();
      $division = DB::table('divisions')->where('id', $area->division_id)->first();
      $data['area'] = $area;
      $data['location'] = $division;
    }

    $category = DB::table('categories')->where('id', $ad->category_id)->first();
    if($category->parent != 0){
      $parentCategory = DB::table('categories')->where('id', $category->parent)->first();
      $data['parentCategory'] = $parentCategory;
    }
    $data['category'] = $category;


    if(!empty($ad->features)){
     $features =  $ad->features;
     $arrFeatures = explode(",",$features);
     $features = DB::table('features')->whereIn('id', $arrFeatures)->get();
     $data['features'] = $features;
   }

     // dd($features);
   $photos = DB::table('photos')->where('ads_id', $ad->id)->get();
   $data['ad'] = $ad;
   $data['photos'] = $photos;
    // dd($photos);

   return view('user-dashboard.ad-view', $data);
 }



public function ajaxSubcatByCat($catId){

  $subcategories = Category::where('parent', $catId)->get();
  return response()->json(['subcategories'=>$subcategories]);
}

public function removeAdPhoto($photoId){

  DB::table('photos')->where('id', $photoId)->delete();
  Session::flash('success_message', 'Ad photo deleted successfully');
  return Redirect::back();

  
}

public function updateCategory(Request $request, $adId){

  $ad = DB::table('ads')->where('id', $adId)->first();
// dd($ad);
  if(!empty($ad)){
    if(!empty($request->subcategory)){
      DB::table('ads')->where('id', $adId)->update(['category_id' => $request->subcategory]);
      Session::flash('success_message', 'Ad category updated successfully');
      return Redirect::back(); 
    }else{
     Session::flash('info_message', 'Something wrong, ad category updated does not updated...!');
     return Redirect::back();
   }
 }
 else{
  return abort(404);
}
}




public function updateAdPhoto(Request $request, $adId){

// dd($request->file('image')[0]); 
 if(!empty($request->file('image')[0])){

  $count = count($request->file('image'));
  $photos = array();

  for($i=0; $i<$count; $i++)
  {
    if(!empty($request->file('image')[$i])){
      $image = $request->file('image')[$i];
      $base_name = preg_replace('/\..+$/', '', $image->getClientOriginalName());
      $base_name = explode(' ', $base_name);
      $base_name = implode('-', $base_name);
      $image_name = $base_name."-".uniqid().".".$image->getClientOriginalExtension();
      $file_path = '../uploads';
      $image->move($file_path, $image_name);

      array_push($photos, [
        'photo'=> $image_name, 
        'ads_id'=> $adId, 
        'status'=> 1,
        'created_at'=> date("Y-m-d h:i:s", time()), 
        'updated_at'=> date("Y-m-d h:i:s", time()), 
      ]);
    }
  }

  DB::table('photos')->insert($photos);  
  Session::flash('success_message', 'Update new photo');
  return Redirect::back();              
}
else{
  Session::flash('info_message', 'Please do not submit without any new photo');
  return Redirect::back();
}

}


public function updateAd(Request $request, $adId){

  $validator = Validator::make($request->all(), [
    'condition' => 'required',
    'description' => 'required',
    'address' => 'required',
    'authenticity' => 'required',
    'price' => 'required',
    'phone_no' => 'required',
  ]);

  if ($validator->fails()) {
    return Redirect::back()
    ->withErrors($validator)
    ->withInput();
  } else {

    $arrData = array( 
      'condition' => $request->condition, 
      'description' => $request->description,
      'address' => $request->address,                      
      'authenticity' => $request->authenticity,                      
      'price' => $request->price,                      
      'phone_no' => $request->phone_no,                      

    );
    if(!empty($request->model)){
      $arrData['model'] =$request->model;
    }

    if(!empty($request->model_year)){
      $arrData['model_year'] = 1;
    }
    if(!empty($request->title)){
      $arrData['title'] = $request->title;
    }
    if(!empty($request->brand)){
      $arrData['brand_id'] = $request->brand;
    }
    if(!empty($request->feature)){

      $arrFeature = $request->feature;
      $stringFeatures = implode(",", $arrFeature);

      $arrData['features'] = $stringFeatures;
    }
    if(!empty($request->fuel_type)){

      $arrFuel = $request->fuel_type;
      $stringFuel = implode(",", $arrFuel);

      $arrData['fuel_type'] = $stringFuel;
    }

    if(!empty($request->regi_year)){
      $arrData['regi_year'] = $request->regi_year;
    }
    if(!empty($request->type_id)){
      $arrData['type_id'] = $request->type_id;
    }
    if(!empty($request->price_type)){
      $arrData['price_type'] = $request->price_type;

    }else{
      $arrData['price_type'] = 'fixed';
    }

    if(!empty($request->capacity)){
      $arrData['capacity'] = $request->capacity;
    }
    if(!empty($request->km)){
      $arrData['km'] = $request->km;
    }
    if(!empty($request->bed)){
      $arrData['bed'] = $request->bed;
    }
    if(!empty($request->bath)){
      $arrData['bath'] = $request->bath;
    }
    if(!empty($request->size)){
      $arrData['size'] = $request->size;
    }
    if(!empty($request->size)){
      $arrData['size'] = $request->size;
    }
    if(!empty($request->land_size)){
      if(!empty($request->size_unit)){
        $arrData['land_size'] = $request->land_size.'_'.$request->size_unit;
      }
      else{
        $arrData['land_size'] = $request->land_size;
      }

    }


    if(!empty($request->phone_no_type)){
      $arrData['phone_no_type'] = $request->phone_no_type;
    }else{
      $arrData['phone_no_type'] = 'show';
    }
    if(!empty($request->gender)){
      $arrData['gender'] = $request->gender;
    }

    $arrData['user_location'] = 1;
    $arrData['user_ip'] = 1;
    $arrData['updated_at'] = date('Y-m-d H:i:s');
    $arrData['status'] = 5;

    DB::table('ads')->where('id', $adId)->update($arrData);

    Session::flash('success_message', 'Ad updated successfully and pending for approved..!');
    return redirect('/dashboard');

    // dd($arrData);

  }

}





}
