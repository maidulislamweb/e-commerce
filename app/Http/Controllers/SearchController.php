<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\City;
use App\CityArea;
use App\Division;
use App\DivisionArea;
use App\Brand;
use Validator;
use Session;
use Auth;
use Redirect;
use DB;

class SearchController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function locationArea($locationId) {
		// dd($locationId);
		return response()->json(DB::table('locations')->select('locations.id as locationId', 'locations.name as locationName', 'locations.title as locationTitle')->where('parent', $locationId)->get());

	}



	public static function getTotalAdByCategory($categoryId){
		$categoryId = DB::table('categories')->select('categories.id as catId')->where('id', $categoryId)->first();
		$cat = $categoryId->catId;
		$subCategoryIds = DB::table('categories')->select('categories.id as scatId')->where('parent', $categoryId->catId)->get();

		if(count($subCategoryIds) > 0){
			foreach ($subCategoryIds as $key => $subCategoryId) {
				$scatIdArr[] = $subCategoryId->scatId;
			}
		}else{
			$scatIdArr = [];
		}

		array_push($scatIdArr, $cat);
		$totalAds =  DB::table('ads')->whereIn('ads.category_id', $scatIdArr)->where('ads.status', '=', 1)->get()->count();

// dd($totalAds);
		return $totalAds;
	}
	public static function getTotalAdByCity($cityId){

		$city = DB::table('locations')->where('id', $cityId)->first();
		if($city != null){
			$cityAreas = DB::table('locations')->where('parent', $city->id)->get();

			if(count($cityAreas) > 0){
				foreach ($cityAreas as $key => $cityArea) {
					$cityAreaArr[] = $cityArea->id;
				}
			}else{
				$cityAreaArr = [];
			}

			$totalAds =  DB::table('ads')->where('ads.status', '=', 1)->whereIn('ads.location_id', $cityAreaArr)->get()->count();
		}
		else{
			$totalAds = 0;
		}	
		


		return $totalAds;
	}





}
