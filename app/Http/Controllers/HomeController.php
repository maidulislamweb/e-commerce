<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(1);
        $cities = \DB::table('locations')->where('type', 'city')->orderBy('order_id', 'asc')->get();
        $divisions = \DB::table('locations')->where('type', 'division')->orderBy('order_id', 'asc')->get();
       
        $data['cities'] = $cities;
        $data['divisions'] = $divisions;
        // dd($data);
        return view('home', $data);
    }
}
