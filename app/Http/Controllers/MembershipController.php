<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MembershipController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function detail(){
		return view('user-dashboard.membership-detail');
	}

	public function package(){
		return view('user-dashboard.membership-package');
	}
	public function byePackage(Request $request){
		$arrData = array();
		if(!empty($request->supperGold)){
			$arrData['supperGold'] = $request->supperGold;
		}

		if(!empty($request->gold)){
			$arrData['gold'] = $request->gold;
		}
		dd($arrData);
		// return view('user-dashboard.membership-package');
	}
	
}
