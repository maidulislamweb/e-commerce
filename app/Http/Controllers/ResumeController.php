<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Redirect;
use App\Category;
use App\City;
use App\Division;
use App\Resume;
use Session;
use Validator;

use App\CityArea;


class ResumeController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cerateResume(){


      $data['resume'] = 'active-menu';

      $info = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

      $data['info'] = $info;

      $data['cities'] = City::where('status', 1)->get();
      $data['divisions'] = Division::where('status', 1)->get();
      $data['cityAreas'] = CityArea::where('status', 1)->get();

      return view('user-dashboard.add-resume', $data); 
    }


    public function storeResume(Request $request){
      $validator = Validator::make($request->all(), [
        'name' => 'required',
        'phone' => 'required',
        'location' => 'required',
        'area' => 'required',
        'dob' => 'required',
      ]);

      if ($validator->fails()) {
        return Redirect::back()
        ->withErrors($validator)
        ->withInput();
      } else {

       $arrData = array( 
        'dob' => $request->dob,
        'status' => 2,
        'created_at' => date('Y-m-d H:i:s'),
      );

       $userArr = array(
        'name' => $request->name,
        'phone' => $request->phone,
      );

       if(!empty($request->gender)){
        $arrData['gender'] = $request->gender;
      }
      if(!empty($request->linkedin)){
        $arrData['linkedin'] = $request->linkedin;
      }

      $string = $request->location;

      list($first, $last) = explode("-", $string);

      if($first == 'city'){
        $arrData['city_area'] = $request->area;
      }
      if($first == 'division'){
        $arrData['division_area'] = $request->area;
      }
      $arrData['created_at'] = date('Y-m-d H:i:s');
      $arrData['updated_at'] = null;


      $resume = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

      if(!empty($resume)){

        DB::table('users')->where('id', Auth::user()->id)->update($userArr);
        DB::table('resumes')->where('user_id', Auth::user()->id)->update($arrData);

      }else{

        DB::table('users')->where('id', Auth::user()->id)->update($userArr);
        $arrData['user_id'] = Auth::user()->id;
        DB::table('resumes')->insert($arrData);
      }


      Session::flash('success_message', 'Ad updated successfully');
      return redirect('/create-resume-step-2');

    }


  }


  public function addPersonalStepTwo(){

    $data['resume'] = 'active-menu';
    $info = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

    $data['info'] = $info;
    return view('user-dashboard.add-resume-step-two', $data); 
  }

  public function storeResumeStepTwo(Request $request){
    $validator = Validator::make($request->all(), [
      'highest_edu' => 'required',
      'institute' => 'required',
    ]);

    if ($validator->fails()) {
      return Redirect::back()
      ->withErrors($validator)
      ->withInput();
    } else {

     $arrData = array( 
      'highest_edu' => $request->highest_edu,
      'institute' => $request->institute,
      'status' => 3,
    );
     if(!empty($request->special_edu)){
      $arrData['special_edu'] = $request->special_edu;
    }
    
    $resume = DB::table('resumes')->where('user_id', Auth::user()->id)->first();
    if(!empty($resume)){
     DB::table('resumes')->where('user_id', Auth::user()->id)->update($arrData);

   }else{

    $arrData['user_id'] = Auth::user()->id;
    DB::table('resumes')->insert($arrData);
  }
  Session::flash('success_message', 'Ad updated successfully');
  return redirect('/create-resume-step-3');
}
}



public function addPersonalStepThree(){
  $data['resume'] = 'active-menu';
  return view('user-dashboard.add-resume-step-three', $data); 
}



public function storeResumeStepThrees(Request $request){

  $validator = Validator::make($request->all(), [
    'exprience' => 'required',
    'organization' => 'required',
    'designation' => 'required',
    'notice_period' => 'required',
    'current_salary' => 'required',
    'last_join' => 'required|date',
  ]);

  if ($validator->fails()) {
    return Redirect::back()
    ->withErrors($validator)
    ->withInput();
  } else {

   $arrData = array( 
    'exprience' => $request->exprience,
    'organization' => $request->organization,
    'designation' => $request->designation,
    'notice_period' => $request->notice_period,
    'current_salary' => $request->current_salary,
    'last_join' => $request->last_join,
    'status' => 4,
  );
   if(!empty($request->yourself)){
    $arrData['yourself'] = $request->yourself;
  }
  if(!empty($request->current_role)){
    $arrData['current_role'] = $request->current_role;
  }

  $resume = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

  if(!empty($resume)){
   DB::table('resumes')->where('user_id', Auth::user()->id)->update($arrData);

 }else{

  $arrData['user_id'] = Auth::user()->id;
  DB::table('resumes')->insert($arrData);
}
Session::flash('success_message', 'Ad updated successfully');
return redirect('/create-resume-step-4');
}
}

public function addPersonalStepFour(){
  $data['resume'] = 'active-menu';
   $info = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

    $data['info'] = $info;
  return view('user-dashboard.add-resume-step-four', $data); 
}


public function storeResumeStepFour(Request $request){
  $validator = Validator::make($request->all(), [
    'marital_status' => 'required',
    'expected_salary' => 'required',
    'present_address' => 'required',
    'permanent_address' => 'required',
    'cv' => 'nullable|mimes:doc,pdf,docx',
  ]);

  if ($validator->fails()) {
    return Redirect::back()
    ->withErrors($validator)
    ->withInput();
  } else {

   $arrData = array( 
    'marital_status' => $request->marital_status,
    'expected_salary' => $request->expected_salary,
    'present_address' => $request->present_address,
    'permanent_address' => $request->permanent_address,
    'status' => 5,
  );


   if(!empty($request->file('cv'))){

    $file = $request->file('cv');
    $base_name = preg_replace('/\..+$/', '', $file->getClientOriginalName());
    $base_name = explode(' ', $base_name);
    $base_name = implode('-', $base_name);
    $file_name = $base_name."-".uniqid().".".$file->getClientOriginalExtension();
    $file_path = 'user-cv';
    $file->move($file_path, $file_name);


    $arrData['cv'] = $file_name;
  }


// dd($arrData);

  $resume = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

  if(!empty($resume)){
   DB::table('resumes')->where('user_id', Auth::user()->id)->update($arrData);

 }else{

  $arrData['user_id'] = Auth::user()->id;
  DB::table('resumes')->insert($arrData);
}
Session::flash('success_message', 'Your Resume updated successfully');
return redirect('/resume-complete');

}

}


public function myResume(){

  $data['resume'] = 'active-menu';
  $myResume = Resume::where('user_id', Auth::user()->id)->first();
if($myResume == null){
  // cerate-resume
  // return Redirect::back('/cerate-resume'); 
  return Redirect::to('/cerate-resume');
}
  if(!empty($myResume->division_area)){
   $area = DB::table('division_areas')->where('id',$myResume->division_area)->first();

   $location = DB::table('divisions')->where('id', $area->division_id)->first();
   $data['area'] = $area;
   $data['location'] = $location;

 }
 if(!empty($myResume->city_area)){

  $area = DB::table('city_areas')->where('id',$myResume->city_area)->first();

  $location = DB::table('cities')->where('id', $area->city_id)->first();
  $data['area'] = $area;
  $data['location'] = $location;
}


$data['myResume'] = $myResume;

return view('user-dashboard.my-resume', $data); 
}


public function updateProfilePhoto(Request $request){

  $validator = Validator::make($request->all(), [
    'profile_photo' => 'required | mimes:jpeg,jpg,png',

  ]);

  if ($validator->fails()) {
    Session::flash('info_message', 'Please insert a valid photo');
    return Redirect::back();

  } else {

    if(!empty($request->file('profile_photo'))){

      $image = $request->file('profile_photo');
      $base_name = preg_replace('/\..+$/', '', $image->getClientOriginalName());
      $base_name = explode(' ', $base_name);
      $base_name = implode('-', $base_name);
      $image_name = $base_name."-".uniqid().".".$image->getClientOriginalExtension();
      $file_path = 'profile-photo';
      $image->move($file_path, $image_name);


      DB::table('users')->where('id', '=', Auth::user()->id)->update(['photo'=>$image_name, 'updated_at' => date("Y-m-d h:i:s", time())]);


    }

    Session::flash('success_message', 'updated your profile photo successfully');
    return Redirect::back();
  }

}

// public function editResumeBasic(){

//   $data['resume'] = 'active-menu';

//   $info = Resume::where('user_id', Auth::user()->id)->first();
//   if(!empty($info->division_area)){
//    $area = DB::table('division_areas')->where('id',$info->division_area)->first();

//    $location = DB::table('divisions')->where('id', $area->division_id)->first();
//    $data['area'] = $area;
//    $data['divi_location'] = $location;

//  }
//  if(!empty($info->city_area)){

//   $area = DB::table('city_areas')->where('id',$info->city_area)->first();

//   $location = DB::table('cities')->where('id', $area->city_id)->first();
//   $data['area'] = $area;
//   $data['city_location'] = $location;
// }
// $data['cities'] = City::where('status', 1)->get();
// $data['divisions'] = Division::where('status', 1)->get();
// // dd($data);

// $data['info'] = $info;
// return view('user-dashboard.edit-resume-basic', $data);  
// }


public function updateResumeBasic(Request $request){


 $validator = Validator::make($request->all(), [
        'name' => 'required',
        'phone' => 'required',
        'location' => 'required',
        'dob' => 'required',
      ]);

      if ($validator->fails()) {
        return Redirect::back()
        ->withErrors($validator)
        ->withInput();
      } else {

       $arrData = array( 
        'dob' => $request->dob,
        'status' => 2,
        'created_at' => date('Y-m-d H:i:s'),
      );

       $userArr = array(
        'name' => $request->name,
        'phone' => $request->phone,
      );

       if(!empty($request->gender)){
        $arrData['gender'] = $request->gender;
      }
      if(!empty($request->linkedin)){
        $arrData['linkedin'] = $request->linkedin;
      }

      $string = $request->location;

      list($first, $last) = explode("-", $string);

      if($first == 'city'){
        $arrData['city_area'] = $request->area;
      }
      if($first == 'division'){
        $arrData['division_area'] = $request->area;
      }
      
      $arrData['updated_at'] = date('Y-m-d H:i:s');


      $resume = DB::table('resumes')->where('user_id', Auth::user()->id)->first();

      if(!empty($resume)){

        DB::table('users')->where('id', Auth::user()->id)->update($userArr);
        DB::table('resumes')->where('user_id', Auth::user()->id)->update($arrData);

      }else{

        DB::table('users')->where('id', Auth::user()->id)->update($userArr);
        $arrData['user_id'] = Auth::user()->id;
        DB::table('resumes')->insert($arrData);
      }


      Session::flash('success_message', 'Updated successfully');
      return Redirect::back();

    }

}


}
