<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use App\Http\Controllers\Input;
use App\Category;
use App\City;
use App\CityArea;
use App\Division;
use App\DivisionArea;
use App\Brand;
use Validator;
use Session;
use Auth;
use Redirect;
use DB;

class AdsController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */


	public function index(Request $request, $location=null) {

		$by_paying_member = $request->input('by_paying_member');
		$catId = $request->input('catId');


		if($catId != null){
			$categories = DB::table('categories')->select('categories.id as categoryId')->where('parent', '=', $catId)->get();


			if(count($categories) > 0){
				foreach ($categories as $key => $category) {
					$categoryArr[] = $category->categoryId;
				}
			}else{
				$category = DB::table('categories')->where('id', '=', $catId)->first();

			
				$categoryArr = array();
				array_push($categoryArr, $category->id);
			}


		}
		else{
			$catId = null;

		}
		// dd($catId);

		if($catId != null){


			if($location != null){



				$location = DB::table('locations')->where('title', $location)->first();
				
				
				$locationAreas = DB::table('locations')->select('locations.id as city_areas_id')->where('parent', $location->id)->get();

				if(count($locationAreas) > 0){
					foreach ($locationAreas as $key => $locationArea) {
						$locationArr[] = $locationArea->city_areas_id;
					}
				}else{
					if($location->type = 'area'){
						$locationArr = array();
						array_push($locationArr, $location->id);
						// dd($locationArr);
					}else{
						$locationArr = [];
					}
					
				}



				if(isset($by_paying_member) && ($by_paying_member == 1)){
					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
					->where('users.membership_type', '=', 'member')
					->whereIn('ads.category_id', $categoryArr)
					->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}else{
					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
		// ->where('users.membership_type', '=', 'member')
					->whereIn('ads.category_id', $categoryArr)
					->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}


				$areas = DB::table('locations')->where('id', $location->id)->orderBy('order_id', 'asc')->get();
				$data['areas'] = $areas;



			}else{

				if(isset($by_paying_member) && ($by_paying_member == 1)){

					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
					->where('users.membership_type', '=', 'member')
					->whereIn('ads.category_id', $categoryArr)
		// ->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}else{

					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
		// ->where('users.membership_type', '=', 'member')
					->whereIn('ads.category_id', $categoryArr)
		// ->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}

			}


		}else{


			if($location !=null){


				$location = DB::table('locations')->where('title', $location)->first();

				
				$locationAreas = DB::table('locations')->select('locations.id as city_areas_id')->where('parent', $location->id)->get();

				if(count($locationAreas) > 0){
					foreach ($locationAreas as $key => $locationArea) {
						$locationArr[] = $locationArea->city_areas_id;
					}
				}else{
					if($location->type = 'area'){
						$locationArr = array();
						array_push($locationArr, $location->id);
						// dd($locationArr);
					}else{
						$locationArr = [];
					}
					
					
					
				}

// dd($locationArr);

				if(isset($by_paying_member) && ($by_paying_member == 1)){
					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
					->where('users.membership_type', '=', 'member')
		// ->whereIn('ads.category_id', $categoryArr)
					->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();

				}else{

					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
		// ->where('users.membership_type', '=', 'member')
		// ->whereIn('ads.category_id', $categoryArr)
					->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
					// dd( $allAds);
				}
// dd($location);

				$areas = DB::table('locations')->where('type', 'area')->whereIn('id', $locationArr)->orderBy('order_id', 'asc')->get();
				$data['areas'] = $areas;




			}else{

				if(isset($by_paying_member) && ($by_paying_member == 1)){

					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
					->where('users.membership_type', '=', 'member')
		// ->whereIn('ads.category_id', $categoryArr)
		// ->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}else{

					$allAds  = DB::table('ads')
					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
					->leftJoin('users', 'users.id', '=', 'ads.user_id')
					->leftJoin('locations', 'locations.id', '=', 'ads.location_id')
					->where('ads.status', '=', 1)
		// ->where('users.membership_type', '=', 'member')
		// ->whereIn('ads.category_id', $categoryArr)
		// ->whereIn('ads.location_id', $locationArr)
					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'locations.name as city_area_name')
					->orderBy('users.membership_type', 'desc')
					->get();
				}

			}



		}

		

		$data['ads'] = $allAds;
		$data['by_paying_member'] = $by_paying_member;
		$data['catId'] = $catId;
		$data['categories'] = DB::table('categories')->where('parent', '=', '0')->get();
		

		// dd($location);

		if($location != null){
			$data['location'] = $location;
		}else{
			$data['location'] = null;
		}
		
		$data['cities'] = DB::table('locations')->where('type', 'city')->orderBy('order_id', 'asc')->get();
		$data['divisions'] = DB::table('locations')->where('type', 'division')->orderBy('order_id', 'asc')->get();
// dd($data);
		return view('pages.ads', $data);

	}


// 		if($location !=null){

// 			$by_paying_member = $request->input('by_paying_member');

// 			$catId = $request->input('catId');

// 			if($catId != null){
// 				$categories = DB::table('categories')->select('categories.id as categoryId')->where('parent', '=', $catId)->get();


// 				if(count($categories) > 0){
// 					foreach ($categories as $key => $category) {
// 						$categoryArr[] = $category->categoryId;
// 					}
// 				}else{
// 					$categoryArr = [];
// 				}


// 			}
// 			else{
// 				$catId = null;
// 			}



// 			$location = DB::table('cities')->where('name', $location)->first();
// 			$locationAreas = DB::table('city_areas')->select('city_areas.id as city_areas_id')->where('city_id', $location->id)->get();

// 			if(count($locationAreas) > 0){
// 				foreach ($locationAreas as $key => $locationArea) {
// 					$locationArr[] = $locationArea->city_areas_id;
// 				}
// 			}else{
// 				$locationArr = [];
// 			}


// 			if($by_paying_member == 1){

// 				if($catId != null){
// 					$allAds  = DB::table('ads')
// 					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
// 					->leftJoin('users', 'users.id', '=', 'ads.user_id')
// 					->leftJoin('city_areas', 'city_areas.id', '=', 'ads.location_id')
// 					->where('ads.status', '=', 1)
// 					->where('users.membership_type', '=', 'member')
// 					->whereIn('ads.category_id', $categoryArr)
// 					->whereIn('ads.city_area', $locationArr)
// 					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'city_areas.name as city_area_name')
// 					->orderBy('users.membership_type', 'desc')
// 					->get();
// 				}else{
// 					$allAds  = DB::table('ads')
// 					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
// 					->leftJoin('users', 'users.id', '=', 'ads.user_id')
// 					->leftJoin('city_areas', 'city_areas.id', '=', 'ads.city_area')
// 					->where('ads.status', '=', 1)
// 					->where('users.membership_type', '=', 'member')
// 					->whereIn('ads.city_area', $locationArr)
// 					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'city_areas.name as city_area_name')
// 					->orderBy('users.membership_type', 'desc')
// 					->get();

// 				}



// 			}else{

// 				if($catId != null){


// 					$allAds  = DB::table('ads')
// 					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
// 					->leftJoin('users', 'users.id', '=', 'ads.user_id')
// 					->leftJoin('city_areas', 'city_areas.id', '=', 'ads.city_area')
// 					->where('ads.status', '=', 1)
// 					->whereIn('ads.category_id', $categoryArr)
// 					->whereIn('ads.city_area', $locationArr)
// 					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'city_areas.name as city_area_name')
// 					->orderBy('users.membership_type', 'desc')
// 					->get();
// // dd($allAds);
// 					// $data['by_paying_member'] = 0;
// 				}else{


// 					$allAds  = DB::table('ads')
// 					->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
// 					->leftJoin('users', 'users.id', '=', 'ads.user_id')
// 					->leftJoin('city_areas', 'city_areas.id', '=', 'ads.city_area')
// 					->where('ads.status', '=', 1)
// 					->whereIn('ads.city_area', $locationArr)
// 					->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'city_areas.name as city_area_name')
// 					->orderBy('users.membership_type', 'desc')
// 					->get();

// 					// $data['by_paying_member'] = 0;
// 				}
// 			}


// 			$areas = DB::table('city_areas')->where('city_id', $location->id)->orderBy('order_id', 'asc')->get();

// 			$data['ads'] = $allAds;
// 			$data['location'] = $location;
// 			$data['areas'] = $areas;


// 		}
// 		else{
// 			$allAds  = DB::table('ads')
// 			->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
// 			->leftJoin('users', 'users.id', '=', 'ads.user_id')
// 			->leftJoin('city_areas', 'city_areas.id', '=', 'ads.city_area')
// 			->where('ads.status', '=', 1)
// 			->select('ads.*', 'categories.name as categoryName', 'users.membership_type as membership_type', 'city_areas.name as city_area_name')
// 			->orderBy('users.membership_type', 'desc')
// 			->get();

// 			$data['ads'] = $allAds;

// 			$data['location'] = null;



// 		}

// 		$data['categories'] = DB::table('categories')->where('parent', '=', '0')->get();
// 		$data['cities'] = DB::table('cities')->get();
// 		$data['divisions'] = DB::table('divisions')->get();

// 		if($request->input('by_paying_member')){
// 			$data['by_paying_member'] = $request->input('by_paying_member');
// 		}else{
// 			$data['by_paying_member'] = 0;
// 		}

// 		if($request->input('catId')){
// 			$data['catId'] = $request->input('catId');
// 		}else{
// 			$data['catId'] =null;
// 		}


// 		return view('pages.ads', $data);
// 	}




	public function adsByArea($areaId, $area=null){

		$allAds  = DB::table('ads')
		->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
		->leftJoin('users', 'users.id', '=', 'ads.user_id')
		->leftJoin('city_areas', 'city_areas.id', '=', 'ads.city_area')
		->select('ads.*', 'categories.name as categoryName','users.membership_type as membership_type', 'city_areas.name as city_area_name')
		->where('ads.status', '=', 1)
		->where('city_area', $areaId)
		->get();


		$area = DB::table('city_areas')->where('id', $areaId)->first();
		$data['cities'] = DB::table('cities')->get();
		$data['divisions'] = DB::table('divisions')->get();
		$data['ads'] = $allAds;
		$data['location'] = $area;

		$data['categories'] = DB::table('categories')->where('parent', '=', '0')->get();

		if(isset($by_paying_member)){
			$data['by_paying_member'] = $by_paying_member;
		}else{
			$data['by_paying_member'] = 0;
		}
		
		if(isset($by_paying_member)){
			$data['catId'] = $catId;
		}else{
			$data['catId'] =null;
		}

			// $data['areas'] = $areas;

		return view('pages.ads', $data);
	}

//function for get Thumb photo
	public static function getThumbPhoto($ad_id){
		$photo = DB::table('photos')->where('ads_id', '=', $ad_id)->first();
		return $photo; 
	} 

	public function adsDetail($ad_id){
		

		$adInfo = DB::table('ads')->leftJoin('brands', 'ads.brand_id', '=', 'brands.id')->select('ads.*', 'brands.title as brand_title')->where('ads.id', $ad_id)->first();

		if($adInfo != null){

			$relAds = DB::table('ads')
			->leftJoin('categories', 'categories.id', '=', 'ads.category_id')
			->where('ads.status', '=', 1)
			->where('ads.category_id', '=', $adInfo->category_id)
			->where('ads.id', '!=', $adInfo->id)
			->select('ads.*', 'categories.id as categoryId')
			->get();
		// dd($relAds);

			$photos = DB::table('photos')->where('ads_id', $adInfo->id)->get();



			$category = DB::table('categories')->where('id', $adInfo->category_id)->first();
			if($category->parent != 0){

				$category = DB::table('categories')->where('id', $category->parent)->first();

			}else{
				$category = DB::table('categories')->where('id', $adInfo->category_id)->first();
			}
			$subCategory = DB::table('categories')->where('id', $adInfo->category_id)->first();

		// dd($category);

			$area = DB::table('locations')->where('id', $adInfo->location_id)->first();

			// dd($location);
			$location = DB::table('locations')->where('id', $area->parent)->first();





			$data['category'] = $category;
			$data['subCategory'] = $subCategory;
			$data['adInfo'] = $adInfo;
			$data['photos'] = $photos;
			$data['area'] = $area;
			$data['location'] = $location;
			$data['relAds'] = $relAds;

			return view('pages.ads-detail', $data);
		}else{
			return abort(404);
		}
	}

	public function postAd(){

		return view('pages.post-ad');
	}

	public function subCategoryByTitle($categoryTitle){

		$categoryInfo = Category::where('title', $categoryTitle)->where('status', 1)->first();
		if(!empty($categoryInfo)){

			$subcategories = $this->getSubCategoryByTitle($categoryTitle);

			$data['subcategories'] = $subcategories;
			$data['categoryInfo'] = $categoryInfo;

			return view('pages.subcategory', $data);

		}
	}


	public function categoryItem(){

		$categories =  Category::where('status', 1)->where('parent', 0)->get();
		$data['categories'] = $categories;
		return view('pages.category-item', $data);
	}

	public function uploadAd($subCategoryId){
		// dd(1);

		$subCategoryInfo = Category::where('id', $subCategoryId)->where('status', 1)->first();
		// dd($subCategoryId);
		if(!empty($subCategoryInfo)){

			$categoryInfo = Category::where('id', $subCategoryInfo->parent)->first();
// dd($categoryInfo);
			$data['categoryInfo'] = $categoryInfo;

			$data['subCategoryInfo'] = $subCategoryInfo;

			$data['cities'] = DB::table('locations')->where('status', 1)->where('type', 'city')->get();

			$data['divisions'] = DB::table('locations')->where('status', 1)->where('type', 'division')->get();

			$data['areas'] = DB::table('locations')->where('status', 1)->where('type', 'area')->get();

			$data['brands'] = $this->getBrand($subCategoryId);
			$data['features'] = $this->getFeature($subCategoryId);
			$data['types'] = $this->getType($subCategoryId);

// dd($data);
			return view('pages.upload-ad', $data);

		}
		else{
			return abort(404);
		}

	// dd($subcategoryId);
	}


	#store ad 
	public function storeAd(Request $request, $categoryId){


		$categoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();
		
		if(!empty($categoryInfo)){

			$validator = Validator::make($request->all(), [
				'image' => 'required',
				'location' => 'required',
				'location' => 'required',
				'condition' => 'required',
				'description' => 'required',
				'address' => 'required',
				'authenticity' => 'required',
				'price' => 'required',
				'phone_no' => 'required',

            // 'brand' => 'required',
            // 'model' => 'required',
            // 'model_year' => 'required',
            // 'regi_year' => 'required',
            // 'fuel_type' => 'required',
            // 'km' => 'required',
            // 'bed' => 'required',
            // 'bath' => 'required',
            // 'land-size' => 'required',
            // 'size' => 'required',
            // 'gender' => 'required',

			]);

			if ($validator->fails()) {
				return Redirect::back()
				->withErrors($validator)
				->withInput();
			} else {

				$arrData = array( 
					
					'condition' => $request->condition, 
					'description' => $request->description,
					'address' => $request->address,                      
					'authenticity' => $request->authenticity,                      
					'price' => $request->price,                      
					'phone_no' => $request->phone_no,                      

				);

				
				
				$arrData['location_id'] = $request->location;
				$arrData['category_id'] = $categoryId;
				$arrData['user_id'] = Auth::user()->id;

				

				if(!empty($request->model)){
					$arrData['model'] =$request->model;
				}

				if(!empty($request->model_year)){
					$arrData['model_year'] = 1;
				}
				if(!empty($request->title)){
					$arrData['title'] = $request->title;
				}
				if(!empty($request->brand)){
					$arrData['brand_id'] = $request->brand;
				}
				if(!empty($request->feature)){
					
					$arrFeature = $request->feature;
					$stringFeatures = implode(",", $arrFeature);

					$arrData['features'] = $stringFeatures;
				}
				if(!empty($request->fuel_type)){
					
					$arrFuel = $request->fuel_type;
					$stringFuel = implode(",", $arrFuel);

					$arrData['fuel_type'] = $stringFuel;
				}
				
				if(!empty($request->regi_year)){
					$arrData['regi_year'] = $request->regi_year;
				}
				if(!empty($request->type_id)){
					$arrData['type_id'] = $request->type_id;
				}
				if(!empty($request->price_type)){
					$arrData['price_type'] = $request->price_type;
				}
				if(!empty($request->capacity)){
					$arrData['capacity'] = $request->capacity;
				}
				if(!empty($request->km)){
					$arrData['km'] = $request->km;
				}
				if(!empty($request->bed)){
					$arrData['bed'] = $request->bed;
				}
				if(!empty($request->bath)){
					$arrData['bath'] = $request->bath;
				}
				if(!empty($request->size)){
					$arrData['size'] = $request->size;
				}
				if(!empty($request->size)){
					$arrData['size'] = $request->size;
				}
				if(!empty($request->land_size)){
					if(!empty($request->size_unit)){
						$arrData['land_size'] = $request->land_size.'_'.$request->size_unit;
					}
					else{
						$arrData['land_size'] = $request->land_size;
					}
					
				}

				
				if(!empty($request->phone_no_type)){
					$arrData['phone_no_type'] = $request->phone_no_type;
				}
				if(!empty($request->gender)){
					$arrData['gender'] = $request->gender;
				}


				$arrData['user_location'] = 1;
				$arrData['user_ip'] = 1;
				$arrData['created_at'] = date('Y-m-d H:i:s');
				$arrData['updated_at'] = Null;
				$arrData['status'] = 5;

				// try{
				// 	DB::beginTransaction();

				$adId = DB::table('ads')->insertGetId($arrData);

				if(!empty($request->file('image')[0])){

					$count = count($request->file('image'));
					$photos = array();

					for($i=0; $i<$count; $i++)
					{
						if(!empty($request->file('image')[$i])){
							$image = $request->file('image')[$i];
							$base_name = preg_replace('/\..+$/', '', $image->getClientOriginalName());
							$base_name = explode(' ', $base_name);
							$base_name = implode('-', $base_name);
							$image_name = $base_name."-".uniqid().".".$image->getClientOriginalExtension();
							$file_path = '../uploads';
							$image->move($file_path, $image_name);

							array_push($photos, [
								'photo'=> $image_name, 
								'ads_id'=> $adId, 
								'status'=> 1,
								'created_at'=> date("Y-m-d h:i:s", time()), 
								'updated_at'=> Null, 
							]);
						}
					}

					DB::table('photos')->insert($photos);                
				}


					// DB::commit();


				Session::flash('success_message', 'Ad uploaded successfully and pending for approved..!');
				return redirect('/dashboard');

				// }
				// catch(\Exception $e)
				// {
				// 	DB::rollback();


				// 	Session::flash('info_message', 'The Ad has not been uploaded pleasae try agin.');
				// 	return Redirect::back()->withErrors($validator)->withInput();

				// }

			}

		}
		else{
			return abort(404);
		}
		
	}


	public function adEdit($adId){

		$ad = DB::table('ads')->where('id', $adId)->first();
		$subCategoryInfo = DB::table('categories')->where('id', $ad->category_id)->first();

		$categoryInfo = DB::table('categories')->where('id', $subCategoryInfo->parent)->first();

		$data['categories'] = Category::where('parent', '=', 0)->get();

		$location = DB::table('locations')->where('id', $ad->location_id)->first(); 

		$areas = DB::table('locations')->where('type', 'area')->where('parent', $location->parent)->where('status', '1')->get();
		$data['areas'] = $areas;

		
		if($location->parent != 0){
			$data['location'] = DB::table('locations')->where('id', $location->parent)->first();
		}else{
			$data['location'] = $location;
		}



		$data['cities'] = DB::table('locations')->where('type', 'city')->where('status', 1)->get();
		$data['divisions']  =DB::table('locations')->where('type', 'division')->where('status', 1)->get();


		$data['photos'] = DB::table('photos')->where('ads_id', $ad->id)->get();



		$adsConObj = new AdsController;
		$data['brands'] = $adsConObj->getBrand($ad->category_id);

 // dd($adId);
		$data['features'] = $adsConObj->getFeature($adId);
		$data['types'] = $adsConObj->getType($adId);

		$data['ad'] = $ad;
		$data['subCategoryInfo'] = $subCategoryInfo;
		$data['categoryInfo'] = $categoryInfo;

		return view('user-dashboard.edit-ads', $data);
	}



	public function updateLocation(Request $request, $adId){

		$ad = DB::table('ads')->where('id', $adId)->first();

		if(!empty($ad)){

			if(!empty($request->area)){

				try{
					DB::beginTransaction();
					DB::table('ads')->where('id', $adId)->update(['location_id' => $request->area]);
					DB::commit();
					Session::flash('success_message', 'Ad category updated successfully');
					return Redirect::back();

				}catch(\Exception $e){

					DB::rollback();
					Session::flash('info_message', 'Something wrong, ad category updated does not updated...!');
					return Redirect::back();

				}

			}else{
				Session::flash('info_message', 'Something wrong, ad category updated does not updated...!');
				return Redirect::back();
			}
		}
		else{
			return abort(404);
		}
	}



	public function location($categoryId){


		$subCategoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();
		
		if(!empty($subCategoryInfo)){

			$categoryInfo = Category::where('parent', $subCategoryInfo->parent)->first();

			$data['cities'] = City::where('status', 1)->get();
			$data['divisions'] = Division::where('status', 1)->get();

			$data['categoryInfo'] = $categoryInfo;

			$data['subCategoryInfo'] = $subCategoryInfo;

			return view('pages.location', $data);
		}
		else{
			return abort(404);
		}
	}

	public function locationCity($cityId, $categoryId){
		$subCategoryInfo = Category::where('id', $categoryId)->where('status', 1)->first();
		
		if(!empty($subCategoryInfo)){

			$categoryInfo = Category::where('parent', $subCategoryInfo->parent)->first();

			$data['city'] = City::where('id', $cityId)->first();

			$data['cityAreas'] = CityArea::where('city_id', $cityId)->get();
			$data['categoryInfo'] = $categoryInfo;

			$data['subCategoryInfo'] = $subCategoryInfo;
			return view('pages.location-city-area', $data);
		}
		else{
			return abort(404);
		}


	// dd($cityId);
	}

	public function ajaxSubcategory($id){
		$category = Category::where('id', $id)->first();
		$subcategory = Category::where('parent', $id)->get();
		return response()->json(['subcategory'=>$subcategory, 'category'=>$category]);
	}

	// public function subCategory($id){

	// 	$categoryInfo = Category::where('id', $id)->first();
	// 	if(!empty($categoryInfo)){

	// 		$parentcatInfo = Category::where('id', $categoryInfo->parent)->first();

	// 		$data['categoryInfo'] = $categoryInfo;

	// 		$data['parentcatInfo'] = $parentcatInfo;


	// 		return view('pages.upload-ad', $data);

	// 	}
	// 	else{
	// 		return abort(404);
	// 	}
	// }

	public function selectLocation($categoryId){

		$categoryInfo = Category::where('id', $categoryId)->first();
		if(!empty($categoryInfo)){

			$parentcatInfo = Category::where('id', $categoryInfo->parent)->first();

			$data['categoryInfo'] = $categoryInfo;

			$data['parentcatInfo'] = $parentcatInfo;

			$data['cities'] = City::where('status', 1)->get();
			$data['divisions'] = Division::where('status', 1)->get();
			// dd($data);
			return view('pages.location', $data);

		}
		else{
			return abort(404);
		}

		
	}

	public function ajaxLocationArea($id){
		$cityAreas = DB::table('locations')->where('parent', $id)->get();
		return response()->json(['areas'=>$cityAreas]);

	}





	// public function ajaxDivisionArea($id){
	// 	$areas = DivisionArea::where('division_id', $id)->get();
	// 	$division = Division::where('id', $id)->first();
	
	// 	return response()->json(['areas'=>$areas, 'division'=>$division]);
	// }

	public function getSubCategoryByTitle($categoryTitle){

		$categoryInfo = Category::where('title', $categoryTitle)->where('status', 1)->first();
		if(!empty($categoryInfo)){

			return Category::where('parent', $categoryInfo->id)->where('status', 1)->get();

		}
		else{
			return abort(404);
		}
	}


	public function getBrand($subcategoryId){
		return DB::table('brands')->where('category_id', $subcategoryId)->where('status', 1)->get();
	}

	public function getFeature($subcategoryId){
		return DB::table('features')->where('category_id', $subcategoryId)->where('status', 1)->get();
	}
	public function getType($subcategoryId){
		return DB::table('item_types')->where('category_id', $subcategoryId)->where('status', 1)->get();
	}


}
